#include "menuobjbutton.h"

MenuObjButton::MenuObjButton()
 : MenuButtonTrigger()
{

    obj = 0; // указатель на объект отображаемый на кнопке

}


MenuObjButton::~MenuObjButton()
{
}




/*!
    \fn MenuObjButton::display()
 */
void MenuObjButton::display()
{

    // рисуем "подложку" из родительского класса
    MenuButtonTrigger::display();

    // нету объекта и выводить нечего
    if ( !obj ) return;

    Vector2D l_d, r_u;
    obj->getDimensionsBoxWithChilds(&l_d, &r_u);

    Vector2D dm = r_u - l_d;

    // найдём направление на центр DimBox относительно рисуемого объекта
    Vector2D dm_center = l_d + dm * 0.5;

    // коэффициент масштабирования для того чтоб любой объект в кнопку влез
    Vector2D sz =  size.getPositive();

    Scalar scale_x =  sz.val.x / dm.val.x;
    Scalar scale_y =  sz.val.y / dm.val.y;

    // выберем какой коэффициент использовать в качестве ужимающего
    Scalar scale;
    if ( scale_x < scale_y ) scale = scale_x; else scale = scale_y;

    scale *= 0.8; // чтоб от границы кнопки отодвинуть

    // центр кнопки
    Vector2D butt_center = pos + size * 0.5;

    glPushMatrix();

      glTranslatef( butt_center.val.x, butt_center.val.y, 0.0f );
      glScalef ( scale, scale, scale );

      glTranslatef( -dm_center.val.x, -dm_center.val.y, 0.0f );

    obj->DisplayWithChilds();

    glPopMatrix();

}
