#ifndef TEMPLATE_OBJ_TREE_H
#define TEMPLATE_OBJ_TREE_H

#include <list>
#include "debug.h"

/**
@brief Шаблон дерева произвольных объектов

При добавлении этого шаблона к объекту при создании (конструировании) объекта он автоматически будет добавлен в дерево объектов этого типа.

Для этого при конструировании необходимо передать в конструктор шаблона указатель на родительский объект (или 0 для создания корневого объекта).

При уничтожении объекта он автоматически вызывает удаление всех дочерних объектов и сам удаляется из дерева.

@author Феклушкин Денис <edu2005-60@mail.ru>
*/


template <class T>
class template_obj_tree {

public:
    /// Изменить родителя на нового
    void setParent (T* const parent_ptr);
    
    /// Возвращает указатель на родителя
    T* getParent ();

    template_obj_tree( T* parent_ptr );
    ~template_obj_tree();

protected:
    typedef std::list<T*> ChildsList;
    std::list<T*> child_items; ///< Список указателей на дочерние объекты
    
    /**
    @brief Выполнить метод для всех дочерних объектов и для самого объекта.

    Выполнение начнётся с самых "старых" дочерних объектов.

    Внимание: эта функция непосредственно НЕ реализует выполнение функции-члена для потомков дочерних объектов!
    
    Синтаксис вызова функции-члена по указателю:
    
    childs_exec( &T::some_method );
    
    (т.е. метод записывается без скобок!)

    @param (T::*func_ptr)() Указатель на функцию-член объекта, не имеющую параметров и не возвращающую значений
    */
    void childs_exec ( void (T::*func_ptr)() );

private:
    typename ChildsList::iterator this_iter;
    typename ChildsList::iterator AddItem(T* ptr);
    void RemoveItem(typename ChildsList::iterator iter);
    T* parent; ///< Родительский объект
};


template <class T>
typename std::list<T*>::iterator template_obj_tree<T>::AddItem(T* ptr)
{

    assert ( ptr != this );
    child_items.push_back( ptr );
    return ((child_items.end())--); // вернём последний добавленный элемент

}


template <class T>
void template_obj_tree<T>::RemoveItem(typename ChildsList::iterator iter)
{

    assert ( iter != this_iter );
    child_items.erase( iter );

}


template <class T>
void template_obj_tree<T>::setParent ( T* const parent_ptr) {

      assert ( parent_ptr != this );

      // удаляемся из старого родителя
      if ( parent ) parent->RemoveItem( this_iter );

      parent = parent_ptr;

      // добавляемся в список нового родителя
      if ( parent ) this_iter = parent->AddItem( static_cast<T*> (this) );

}


template <class T>
T* template_obj_tree<T>::getParent () {

      return parent;

}


template <class T>
template_obj_tree<T>::template_obj_tree( T* parent_ptr )
{

    parent = 0;
    setParent( parent_ptr );

}


template <class T>
template_obj_tree<T>::~template_obj_tree() {

     T* curr;

     typename ChildsList::iterator iter;

     // уничтожаем детей, не забывая что они сами удаляют себя из списка родителя
     while( child_items.begin() != child_items.end() ) {

       iter = child_items.end(); iter--; // удаляем с конца списка
       curr = (*iter);
       T* this_obj = static_cast<T*> (curr);
       delete this_obj;

     }

     // удаляем себя из списка родителя
     if ( parent ) parent->RemoveItem( this_iter );

}


template <class T>
void template_obj_tree<T>::childs_exec ( void (T::*func_ptr)() )
{

     T* curr;

     typename ChildsList::iterator iter = child_items.begin();

     while( iter != child_items.end() ) {

       curr = (*iter);
       T* this_obj = static_cast<T*> (curr);
       ( this_obj->*func_ptr )();
       iter++;

     }

}


#endif
