#include "input.h"

extern Options options;

input_c::input_c() {
}

input_c::~input_c() {
}



/*!
    \fn input_c::processEvent()
 */
void input_c::processEvent()
{


    SDL_Event event;
    SDL_PollEvent(&event);
    const Coord move_step = 0.14f;


    if ( event.type == SDL_KEYDOWN ) {

      // Toggle screen view
//       if ( event.key.keysym.sym == SDLK_f ) {
//             options.getBoolValue(FULLSCREEN) ?
//                   options.setValue(FULLSCREEN, "false") :
//                   options.setValue(FULLSCREEN, "true");
//             Screen::Init();
//       };

//      if ( event.key.keysym.sym == SDLK_SPACE )
//            for(int i = 0; i < 7; i++) first_creature[i].randomize();


      if ( event.key.keysym.sym == SDLK_UP )
            Screen::SetScreenBoxOffset(0, move_step);
      if ( event.key.keysym.sym == SDLK_DOWN )
            Screen::SetScreenBoxOffset(0, - move_step);
      if ( event.key.keysym.sym == SDLK_LEFT )
            Screen::SetScreenBoxOffset(- move_step, 0);
      if ( event.key.keysym.sym == SDLK_RIGHT )
            Screen::SetScreenBoxOffset(move_step, 0);
  //     if ( event.key.keysym.sym == SDLK_d )
  //          delete first_creature;

    }
//     if ( event.type == SDL_MOUSEBUTTONDOWN) {
//             if(mouse_pressed_trigger == false) {
//               mouse_pressed_trigger = true;
//               msg(DEBUG, "Mouse DOWN" );
//   //            first_creature->play_sound(SHOTGUN);
//               if(event.button.button == SDL_BUTTON_RIGHT) {
//                   new creature(event.button.x, event.button.y);
//                   play_sound(OW);
//               } else {
//                   play_sound(SHOTGUN);
//               }
//             }
//     }

//
     if ( event.type == SDL_KEYUP ) {
//       if ( event.key.keysym.sym == SDLK_s ) {
//   //          for(int i = 0; i < 7; i++) first_creature[i].play_sound();
//   //          if(options.debug >= 0) cout << "s pressed" << endl;
//           kill_moving_objects();
//       }
     }

}
