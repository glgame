/**
@file
@brief Часто используемые цвета
*/

#ifndef COLORS_H_
#define COLORS_H_

#include "math/coord_t.h"

static const Color ButtonColor          = { 0,   255,   0 };
static const Color ButtonColorPressed   = { 255,   0,   0 };
static const Color ButtonColorChecked   = { 155,   0,   0 };
static const Color ButtonColorUnchecked = { 10,   10,   100 };
static const Color bg_color             = { 120,  70,   70 };  ///< window background
static const Color PolygoneBody         = { 0,   186,   95 };  ///< зеленый
static const Color CurveBody            = { 0,   180,   255 }; ///< бирюза?
static const Color ColorCurve           = { 190,   0,   190 }; ///< розовый
static const Color ColorCurveDots       = { 220,   220, 220 }; ///< почти белый

#endif

