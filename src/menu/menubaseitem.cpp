#include "menubaseitem.h"


MenuBaseItem::MenuBaseItem()
: template_obj_tree<MenuBaseItem> (0)
{

  Active = true;
  Hidden = false;

}


MenuBaseItem::~MenuBaseItem()
{
}


void MenuBaseItem::DisplayWithChilds()
{

    display();

    childs_exec ( &MenuBaseItem::DisplayWithChilds );

}


// возвращаем координаты относительно всего экрана
Vector2D MenuBaseItem::getScreenCoords( const Vector2D point )
{

  Vector2D res = point;
  MenuBaseItem* curr = this;

  while( curr != 0 ) {
        res += curr->pos;
        curr = curr->getParent();
  }

  return res;

}

Vector2D MenuBaseItem::getScreenCoords()
{

  Vector2D tmp;
  return getScreenCoords ( tmp );

}
