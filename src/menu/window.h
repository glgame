#ifndef WINDOW_H
#define WINDOW_H

#include <list>

#include "../math/coord_t.h"
#include "../math/vector2d.h"
#include "../debug.h"

#include "../linked_list_t.h"
#include "menubaseitem.h"
#include "../input/eventbase.h"
#include "../input/eventgroup.h"
#include "../input/eventmousemove.h"
#include "../screen.h"
#include "../colors.h"

/**
класс реализующий простое окно (например, для меню)

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class Window : public MenuBaseItem, public EventGroup {

    void MouseMove();
    bool Filtered();

    bool Mouse_pressed;

protected:

    void Init();

public:
    /**
     * конструктор для пространства GL
     * @param left_up_coord координаты верхней левой точки окна
     * @param size  размер окна (вектор направо + вниз)
     */
    Window(Vector2D* left_up_coord, Vector2D* size);
    Window();

    virtual ~Window();

    virtual void MoveTo(Vector2D* new_coords);

    virtual void display(); // FIXME потом сделать protected
};

#endif
