#ifndef MENUBUTTONTRIGGER_H
#define MENUBUTTONTRIGGER_H

#include "menubutton.h"

#include "../screen.h"
#include "../gl_functions.h"

/**
кнопка-переключатель

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class MenuButtonTrigger : public MenuButton
{

    bool trigger_checked;

protected:
    void display();
    void ButtonPressed();

public:
    inline bool itChecked() const { return trigger_checked; }

    MenuButtonTrigger();
    ~MenuButtonTrigger();

};

#endif
