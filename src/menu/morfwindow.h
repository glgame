#ifndef MORFWINDOW_H
#define MORFWINDOW_H

#include "window.h"
#include "../math/vector2d.h"

/**
Окно, при перемещении делающее морфинг
Просто красиво :)

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class MorfWindow : public Window
{

    Vector2D coord_step;
    Vector2D size_step;
    unsigned curr_step;

    void MouseMove();

public:
    void display(); // FIXME need be protected
    MorfWindow(Vector2D* left_up_coord, Vector2D* size);
    ~MorfWindow();

    unsigned num_steps;

    void MoveTo(Vector2D* new_coords);
    void MoveTo(Vector2D* new_coords, Vector2D* new_size);
};

#endif
