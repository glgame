#include "window.h"

Window::Window(Vector2D* left_up_coord, Vector2D* size) : MenuBaseItem(), EventGroup()
{

  Init();

  pos = *left_up_coord;
  Window::size = *size;

  // обработка события EventMouseMove()
//  EventBase::base_group.AddItem(this);


}


Window::Window() : MenuBaseItem(), EventGroup()
{

  Init();

}


Window::~Window()
{

//  EventBase::base_group.RemoveItem(this);

}



/*!
    \fn Window::display()
 */
void Window::display()
{

    glTranslatef( pos.val.x, pos.val.y, 0.0 );

    glColor ( bg_color );

    gl_glass_enable();

    glBegin(GL_QUADS);
      glVertex2f( 0.0f,               0.0f);
      glVertex2f( 0.0f,         size.val.y);
      glVertex2f( size.val.x,   size.val.y);
      glVertex2f( size.val.x,         0.0f);
    glEnd();

    gl_glass_disable();

}


/*!
    \fn Window::Init()
 */
void Window::Init()
{

}


/*!
    \fn Window::MoveTo(Vector2D* new_coords)
 */
void Window::MoveTo(Vector2D* new_coords)
{
    pos = *new_coords;
}


bool Window::Filtered() {

    if ( EventItem::Filtered() ) return true;

    // координаты клика относительно центра экрана
    Vector2D click_coords = Screen::Pixel2GL ( event->button.x, event->button.y );

    // координаты начала окна относительно центра экрана
    Vector2D display_coords = MenuBaseItem::getScreenCoords() - Screen::getCurrentViewCenterVector();

    // проверим - попали мы мышкой в квадрат окна или нет
    Vector2D diff = click_coords - display_coords;
    if ( diff.length() <= size.length() && diff.val.x >= 0 && diff.val.y <= 0 ) return false ;

    return true;

}


void Window::MouseMove() {
    return; // FIXME moving blocked
    if ( Filtered() ) return;

    pos = Screen::Pixel2GL ( event->motion.x, event->motion.y );

}

