#ifndef MENUBUTTON_H
#define MENUBUTTON_H

#include "menubaseitem.h"
#include "../colors.h"
#include "../input/eventmouseclick.h"
#include "../screen.h"

/**
кнопочка для меню

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class MenuButton : public MenuBaseItem, public EventMouseClick
{


public:
    MenuButton();
    ~MenuButton();
    void display();
    void MouseClick();
    virtual void ButtonPressed() = 0;

//    inline bool itPressed() const { return pressed_flag; }

};

#endif
