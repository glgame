#include "menubutton.h"

MenuButton::MenuButton()
 : MenuBaseItem(), EventMouseClick()
{

// раз это кнопка значит мы ловим события

// ловля событий мышки, добавим себя в базовую группу обработчиков:
//EventBase::base_group.AddItem(this);

}


MenuButton::~MenuButton()
{

//  EventBase::base_group.RemoveItem(this);

}



/*!
    \fn MenuButton::display()
 */
void MenuButton::display()
{
// FIXME нет установки цвета, переделать логику!
    // рисование начинается относительно начала координат меню
    glPushMatrix();

    glTranslatef( pos.val.x, pos.val.y, 0.0f );

    glBegin(GL_QUADS);
      glVertex2f( 0.0f,               0.0f);
      glVertex2f( 0.0f,         size.val.y);
      glVertex2f( size.val.x,   size.val.y);
      glVertex2f( size.val.x,         0.0f);
    glEnd();

    glPopMatrix();

}


void MenuButton::MouseClick() {

//     if ( EventMouseClick::Filtered() ) return;

      SDL_Event event = HumanInterface::getEvent();

     // координаты клика относительно центра экрана
     Vector2D click_coords = Screen::Pixel2GL ( event.button.x, event.button.y ) - Screen::getCurrentViewCenterVector();

     // координаты клика относительно кнопки
     Vector2D click_pos = click_coords - getScreenCoords();

      // проверим - попали мы мышкой в квадрат кнопки или нет
     if ( click_pos.val.x < 0 || click_pos.val.y > 0 ) return; // не попали
     if ( click_pos.val.x > size.val.x || click_pos.val.y < size.val.y ) return; // не попали
 
     // отлично, это наше сообщение! остановим обработку
//      EventItem::StopHandling = true;

      ButtonPressed();

}
