#ifndef MENUBASEITEM_H
#define MENUBASEITEM_H

#include <string>
#include <list>

#include "../math/vector2d.h"
#include "../template_obj_tree.h"


/**
базовый класс для элементов меню

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class MenuBaseItem : public template_obj_tree<MenuBaseItem> {

public:
    bool Hidden;
    typedef unsigned ID; // тип уникального идентификатора элемента меню

    inline ID getID() const { return id; };
    inline std::string getName() const { return name; };

    MenuBaseItem();
    virtual ~MenuBaseItem();

    Vector2D getScreenCoords();
    Vector2D getScreenCoords( const Vector2D point );

    inline void setPos (Vector2D pos_offset) { pos = pos_offset; }
    inline void setSize (Vector2D new_size) {
           assert ( new_size.val.x >= 0 );
           assert ( new_size.val.y <= 0 );
           size = new_size;
    }

    void DisplayWithChilds();
protected:
    bool Active;

    Vector2D pos;
    Vector2D size;

    virtual void display() = 0;

    virtual inline void setName (std::string new_name) { name = new_name; }

private:

    std::string name; // название
    ID id;

};

#endif
