#include "menubuttontrigger.h"

MenuButtonTrigger::MenuButtonTrigger()
 : MenuButton()
{

  trigger_checked = false;

}


MenuButtonTrigger::~MenuButtonTrigger()
{

}


void MenuButtonTrigger::display()
{

    if ( trigger_checked ) glColor ( ButtonColorChecked );
    else glColor ( ButtonColorUnchecked );

    MenuButton::display();

}


void MenuButtonTrigger::ButtonPressed()
{

    trigger_checked = !trigger_checked;

}

