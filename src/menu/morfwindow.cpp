#include "morfwindow.h"

MorfWindow::MorfWindow(Vector2D* left_up_coord, Vector2D* size)
 : Window(left_up_coord, size)
{

 num_steps = 100;
 curr_step = num_steps;

}


MorfWindow::~MorfWindow()
{
}

void MorfWindow::MoveTo(Vector2D* new_coords, Vector2D* new_size)
{

    Vector2D path = *new_size - size;
    size_step = path / num_steps;

    MoveTo (new_coords);

}

void MorfWindow::MoveTo(Vector2D* new_coords)
{

    Vector2D path = *new_coords - pos;
    coord_step = path / num_steps;

    curr_step = 0;
}


void MorfWindow::display() {

    if ( curr_step != num_steps ) {

        pos += coord_step;
        size += size_step;
        curr_step++;

    }

    Window::display();

}


void MorfWindow::MouseMove() {


}

