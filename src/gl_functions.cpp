#include "gl_functions.h"


void gl_glass_enable() {

    // включаем всякую прозрачность
    glEnable (GL_ALPHA_TEST);
    glEnable (GL_BLEND);
    glBlendFunc (GL_DST_COLOR, GL_ZERO);

}


void gl_glass_disable() {

    glDisable (GL_ALPHA_TEST);
    glDisable (GL_BLEND);

}


void gl_white_glass_enable() {

    glEnable (GL_ALPHA_TEST);
    glEnable (GL_BLEND);
    glBlendFunc (GL_DST_COLOR, GL_ZERO);

}


void gl_white_glass_disable() {

    glDisable (GL_ALPHA_TEST);
    glDisable (GL_BLEND);

}


void glColor ( Color c ) {

    glColor3ub ( c.red, c.green, c.blue );

}


void glVertex( Vector2D& v )
{
    glVertex2d( v.val.x, v.val.y );
}


void DisplayCross( Scalar size ) {

    glBegin( GL_LINES );
      glVertex2f( - size,  0);
      glVertex2f(   size,  0);

      glVertex2f(   0,   size);
      glVertex2f(   0, - size);
    glEnd();

}


void DisplayCross( Vector2D coord, Scalar size ) {
    glPushMatrix();
    glTranslate( coord );
    DisplayCross( size );
    glPopMatrix();
}


void glTranslate( Vector2D& coord ) {
    glTranslatef( coord.val.x, coord.val.y, 0.0 );
}
