#ifndef OPTIONS_H
#define OPTIONS_H

#include <string>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include "debug.h"

/**
  @brief Опции и настройки

  Этот класс нужен для того чтобы прочитать из файла опции и иногда записать их.
  Класс хранит опции в статическом массиве. Для удобства доступа к опциям
  из разных частей программы допускается создавать несколько экземпляров класса.

  "Внутреннее имя" - обозначение опции в виде перечисляемого значения OPT_NAME
  "Внешнее имя" - имя опции в виде строки, которой опция обозначена в файле.

  @class	Options
  @author	Феклушкин Денис <edu2005-60@mail.ru>
*/
class Options{

public:

/// Тип хранимого опцией значения
enum OPT_TYPE {
      BOOLEAN_,
      INT_,
      FLOAT_,
      STRING_
};

/// Внутренние имена опций
enum OPT_NAME {
      NOT_DEFINED,    ///< Не определено (используется для работы алгоритма чтения опций)
      DEBUG,          ///< Включен вывод debug-информации
      WINDOW_SIZE_X,  ///< Размер окна по X
      WINDOW_SIZE_Y,  ///< Размер окна по Y
      SHOW_GRID,      ///< Показывать зелёную сетку фона
      GRID_STEP,      ///< Шаг сетки
      MAX_FPS,        ///< Максимальный fps
      PAUSED,         ///< Игра на паузе
      FULLSCREEN      ///< Игра в полноэкранном режиме
};

/// Опция
struct OptionUnit {
  std::string nameString;	///< Внешнее имя опции
  OPT_NAME    nameEnum;		///< Внутреннее имя опции
  OPT_TYPE    type;			///< Тип значения
  std::string description;	///< Дополнительное описание
  std::string value;		///< Значение
 };


	/** Инициализация класса */
	void	Init();

	Options& operator=(Options&); ///< Закрываем возможность копирования
    Options(const Options&); ///< Закрываем возможность создания копирующего конструктора

	/** Массив доступных опций. Один для всех обьектов класса */
	static OptionUnit* availOptions;

	/** Количество доступных опций */
	static int optCount;

    Options();
	~Options() {}

	/** Загрузка настроек из файла */
	void	loadFromFile(std::string fileName);

	/** Чтение опции по внутреннему имени в строку */
    std::string getValue(OPT_NAME nameEnum);
    bool		getBoolValue(OPT_NAME nameEnum);
    int		getIntValue(OPT_NAME nameEnum);
    float		getFloatValue(OPT_NAME nameEnum);
    std::string getStringValue(OPT_NAME nameEnum);

	/** Чтение опции по внешнему имени в строку */
	std::string getValue(std::string name);

	/** Установка опции по внутреннему имени */
	void		setValue(OPT_NAME nameEnum, std::string value);

	/** Сохранение настроек в файл */
	void saveOptions(std::string fileName);
};

#endif
