#ifndef RIGHTMENU_H
#define RIGHTMENU_H

#include "menu/window.h"
#include "menu/menubuttontrigger.h"
#include "menuobjbutton.h"

#include "phys/force_point_obj.h"
#include "phys/obj_disk.h"

/**
рисует меню инструментов справа

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class RightMenu : public Window
{
public:
    RightMenu();

    ~RightMenu();

};

#endif
