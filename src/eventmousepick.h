#ifndef EVENTMOUSEPICK_H
#define EVENTMOUSEPICK_H

#include "input/eventmouse.h"
#include "math/vector2d.h"
#include "screen.h"

/**
бля, какие же эвенты выходят кривые...

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class EventMousePick : public EventMouse
{

    bool pressed;

public:
    EventMousePick();

    ~EventMousePick();
    bool Filtered();
    void RecvEvent();

    virtual bool MousePickUp(Vector2D pick_world_coords) = 0;
    virtual void MousePickDown(Vector2D pick_world_coords) = 0;

};

#endif
