#ifndef EVENTSOURCE_H
#define EVENTSOURCE_H

#include "input/eventbase.h"

/**
	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class EventSource : public EventBase
{
public:
    EventSource();

    ~EventSource();
    void ProcessEvents();

};

#endif
