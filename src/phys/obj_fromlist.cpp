#include "obj_fromlist.h"
obj_FromList::obj_FromList(GLuint object, Coord x, Coord y, Angle rotate, Coord size)
{
    obj_FromList::object = object;
    obj_FromList::s_x = x;
    obj_FromList::s_y = y;
    obj_FromList::rotation = rotate;



}


obj_FromList::~obj_FromList()
{
}

void obj_FromList::display()
{

    glPushMatrix();

    glTranslatef(s_x, s_y, 0);

    glRotated(rotation, 0.8f, 0.5f, 1.0f);

    glCallList(object);

    glPopMatrix();

}

void obj_FromList::calculate()
{
    rotation+=1.0f;
}

