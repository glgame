#ifndef CGEOM_H
#define CGEOM_H

#include "../math/vector2d.h"
#include <list>
#include "../gl_functions.h"
//#include "geometry.h"
#include "../tmy_list.h"


/**
  Базовый класс для геометрических фигур (2D), имеющих площадь
  (не для геометрических примитивов!)

  @brief Геометрическая фигура
  @class CGeom
	@author Феклушкин Денис <edu2005-60@mail.ru>
*/

/**
Список векторов.

Обычно (но не обязательно) применяется как список точек, составляющих углы многоугольника.
*/
typedef den::list<Vector2D> VectorsList;

/**
Список итераторов на векторы списка VectorsList

Удобны к использованию в качестве списка углов многоугольника тем, что (например)
при незначительном изменени многоугольника (увеличение, уменьшение, растягивание)
не приходится искать заново триангуляцию, составленную из этих итераторов.

Ещё итераторы а не чистые указатели используются потому что зная итератор можно мгновенно
удалить из списка VectorsList соответствующий ему вектор.
*/
typedef den::list<VectorsList::iterator> ItersList;

class CGeom{
public:

    /// Треугольник
    typedef struct {
      VectorsList::iterator c1;
      VectorsList::iterator c2;
      VectorsList::iterator c3;
    } Triangle;

    typedef std::list<Triangle> TrianglesList; ///< Список треугольников

    CGeom();
    virtual ~CGeom();

    virtual void display() const = 0; ///< Отобразить фигуру средствами OpenGL
    static void DisplayTriangle( Triangle& t ); ///< Отобразить треугольник средствами OpenGL

    /// Разделение списка точек на два списка, для использования в "кривоугольнике" Безье
    static void SplitInto2Lists( VectorsList* from, VectorsList& res1, VectorsList& res2 );
    
    /// Найти повёрнутый вокруг общего центра список точек
    static void RotatedToAngle( VectorsList& from, VectorsList& rotated, Vector2D& center, Scalar angle );

    /// Найти точку, повёрнутую относительно заданной точки на заданный угол
    static Vector2D RotatedToAngle( Vector2D& src, Vector2D& center, const Scalar angle );

    /// Создаёт список итераторов ItersList, указывающих на список VectorsList
    static ItersList* GetItersList( VectorsList* vectors );
    
    /// Создаёт список VectorsList из списка итераторов ItersList
    static VectorsList* GetVectorsList( ItersList* iterators );

};

#endif
