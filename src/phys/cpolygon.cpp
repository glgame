#include "cpolygon.h"

CPolygon::CPolygon()
{
	trianglesList = 0;
}

CPolygon::CPolygon( ItersList* corners )
{
	trianglesList = 0;
    NewCorners( corners );
}

CPolygon::~CPolygon()
{
	delete trianglesList;
}


void CPolygon::NewCorners( ItersList* corners )
{
  CBasePolygon::NewCorners( corners );
  trianglesList = triangulate();
}


/**
  @param axis - Необязательный параметр - ось( точка ) вращения
  @author Александр Оплеснин <gasparfx@gmail.com>
 */
Scalar CPolygon::CalcMomentOfInertiaCapacity( Vector2D* axis )
{
	// Еслу пусто, то и момента инерции нет
	if ( trianglesList->empty() ){
		return 0;
	}
	// Сюда запишем результат
	Scalar momInerCap = 0;
	// Здесь промежуточные результаты
	Scalar momInerCapTemp = 0;

	// Точка вращения( ось вращения)
	Vector2D axisOfRotation;
	// Центр масс всего полигона
	Vector2D centrMass = CDumbPolygon::GetCenterMass();
    // Центр масс одного треугольника
	Vector2D triangleCentrMass;

	// Если ось не задана, - используем центр масс
	if ( !axis ){
		axisOfRotation = centrMass;
	} else {
		axisOfRotation = *axis;
	}


	TrianglesList::iterator i = trianglesList->begin();
	TrianglesList::iterator end = trianglesList->end();
    // Перебираем треугольники
	while ( i != end ) {

		// Образующие векторы текущего треугольника
		Vector2D e1 = ( *(i->c2) ) - ( *(i->c1) );
		Vector2D e2 = ( *(i->c3) ) - ( *(i->c1) );

		// d - Якобиан преобразования
		Scalar d = Vector2D::ProductVector( e1, e2 );

		// Переобозначения - для простоты
		Scalar ex1 = e1.val.x, ey1 = e1.val.y;
		Scalar ex2 = e2.val.x, ey2 = e2.val.y;

		// Крутая формула :)
		Scalar integralX = (1.0 / 3.0) * (0.25f * (ex1*ex1 + ex2*ex1 + ex2*ex2));
		Scalar integralY = (1.0 / 3.0) * (0.25f * (ey1*ey1 + ey2*ey1 + ey2*ey2));

		// Емкость момента инерции относительно c1
		momInerCapTemp = d * ( integralX + integralY );

	    // Центр масс текущего треугольника
		triangleCentrMass = ( e1 + (e2 - e1) * 0.5 ) * (2.0/3.0);

		// Емкость момента инерции относительно центра масс
		momInerCapTemp = momInerCapTemp - ( fabs ( Vector2D::ProductVector(e1, e2) * 0.5 )
											* triangleCentrMass.length()
											* triangleCentrMass.length() );

		// Абсолютные координаты центра масс треугольника
		triangleCentrMass = triangleCentrMass + *(i->c1);

		// Емкость момента инерции относительно заданной оси
		momInerCapTemp = momInerCapTemp + ( fabs ( Vector2D::ProductVector(e1, e2) * 0.5 )
											* ( triangleCentrMass - axisOfRotation ).length()
											* ( triangleCentrMass - axisOfRotation ).length() );
		// Прибавляем получивщуюся емкость и идем к следующему треугольнику
		momInerCap += momInerCapTemp;
		i++;
	}

	return momInerCap;
}
