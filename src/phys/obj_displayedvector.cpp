#include "obj_displayedvector.h"

obj_DisplayedVector::obj_DisplayedVector( Vector2D* start_point, Vector2D* value )
 : displayed_obj()
{
  show_value = true;
  show_axis = true;
  show_debug_to_console = false;

  obj_DisplayedVector::start_point = start_point;
  obj_DisplayedVector::value  = value;
//  obj_DisplayedVector::magnify  = 1 / physics_obj::magnify_forces;
}


obj_DisplayedVector::~obj_DisplayedVector()
{
}




/*!
    \fn obj_DisplayedVector::display()
 */
void obj_DisplayedVector::display()
{
  Vector2D value = (*obj_DisplayedVector::value);

  glPushMatrix();

  glTranslatef( start_point->val.x - Screen::getCurrentViewCenter().x,
                start_point->val.y - Screen::getCurrentViewCenter().y,
                - 8.5f );


  // show x and y axis
  if ( show_axis ) {

      // x axis:
      glBegin( GL_LINES );

        glColor3f(0.9f, 0.2f, 0.2f);
        glVertex2f( 0.0f, 0.0f );

        glColor3f(0.3f, 0.0f, 0.0f);
        glVertex2f( value.val.x, 0.0f );

      glEnd();

      // y axis:
      glBegin( GL_LINES );

        glColor3f(0.2f, 0.9f, 0.2f);
        glVertex2f( 0.0f, 0.0f );

        glColor3f(0.0f, 0.3f, 0.0f);
        glVertex2f( 0.0f, value.val.y );

      glEnd();

  }

  // show value
  if ( show_value ) {

      glBegin( GL_LINES );

        glColor3f(0.7f, 0.7f, 0.7f);
        glVertex2f( 0.0f, 0.0f );

        glColor3f(0.3f, 0.3f, 0.3f);
        glVertex2f( value.val.x, value.val.y );

      glEnd();

  }

  glPopMatrix();

  if( show_debug_to_console ) {
        std::cout << "Vector start_point: " << start_point
            << " value: " << obj_DisplayedVector::value->length() << std::endl;
  }

}
