#include "obj_segmentrope.h"

obj_SegmentRope::obj_SegmentRope(force_point_obj* first_hinge, force_point_obj* last_hinge)
 : segment_obj(first_hinge, last_hinge)
{
}


obj_SegmentRope::~obj_SegmentRope()
{
}



void obj_SegmentRope::physics_forces_calculate(){

    // Коэффициент упругости
    static const Scalar k = 1.0f;

    // Растягиваемость нитей
    static const Scalar n = 1.0;

    this_segment = last_hinge->getCoord() - first_hinge->getCoord();
    Length l = this_segment.length();

    Length len_dx = l - length;

    // сила в сегменте подчиняется закону Гука + учитывает длину пружины
    if(len_dx > 0 )
        force = this_segment.ort() * k * len_dx;

    // Передадим силы к обоим шарнирам
    // (на самом деле - импульсы, чтобы сразу вернуть уехавший шарнир на место)
    last_hinge->ApplyImpulse( -n * force );
    first_hinge->ApplyImpulse( n * force );

}

