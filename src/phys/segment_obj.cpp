#include "segment_obj.h"

segment_obj::segment_obj(force_point_obj* first_hinge, force_point_obj* last_hinge)
 : physics_obj()
{

  // сохраняем указатели на концы сегмента
  segment_obj::first_hinge = first_hinge;
  segment_obj::last_hinge = last_hinge;
  length = 0;

}


segment_obj::~segment_obj()
{
}


void segment_obj::Init(){

  assert ( getParent() != 0 );

  // подсчёт вектора сегмента
  calculate_segment_vector();

  // запоминм его изначальную длину если она не задана явно
  if ( !length ) length = this_segment.length();

}


void segment_obj::display(){

    Vector2D end = this_segment;

    glColor3f(0.6,0.6,1.0);
    glLineWidth ( 2.0f);
    glLineStipple(2,0xAAAA);
    glEnable(GL_LINE_STIPPLE);

    glBegin( GL_LINES );

      glVertex2f( 0.0f, 0.0f );
      glVertex2f( end.val.x, end.val.y);

    glEnd();

    glDisable(GL_LINE_STIPPLE);
    glLineWidth ( 1.0f);

}


void segment_obj::physics_forces_calculate(){

    // Коэффициент упругости
    static const Scalar k = 1.0f;

    // Растягиваемость сегмента
    static const Scalar n = 1.0;

    calculate_segment_vector();

    Length length_curr = this_segment.length();

    // сила в сегменте подчиняется закону Гука + учитывает длину пружины
    force = this_segment.ort() * k * (length_curr - length);

    // Передадим силы к обоим шарнирам
    // (на самом деле - импульсы, чтобы сразу вернуть уехавший шарнир на место)
    last_hinge->ApplyImpulse( -n * force );
    first_hinge->ApplyImpulse( n * force );

    // Трение внутри сегмента
    // Внимание! с этой штукой верёвки тянутся под силой тяжести до бесконечности
//    static const double viscosity = 0.0f;
//    friction = (first_hinge->velocity - last_hinge->velocity) * viscosity;

    // Замедлим движение шарниров трением
//    first_hinge->apply_force( -1 * friction);
//    last_hinge->apply_force( friction );


}

void segment_obj::physics_forces_reset(){
    force.Reset();
    friction.Reset();
}

void segment_obj::physics_forces_apply(){

    calculate_segment_vector();

}

void segment_obj::getDimensionsBox(Vector2D *const v1, Vector2D *const v2)
{

    *v2 = this_segment;

}


/*!
    \fn segment_obj::calculate_length()
 */
void segment_obj::calculate_segment_vector()
{

  this_segment = last_hinge->getCoord() - first_hinge->getCoord();

  // установим центр сегмента относительно родителя
  setCoords( first_hinge->getCoord() );

}
