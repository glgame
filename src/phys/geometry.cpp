#include "geometry.h"

Scalar getTriangleArea( Vector2D* c1, Vector2D* c2, Vector2D* c3 ) {

  Vector2D a = *c2 - *c1;
  Vector2D b = *c3 - *c1;

  return getTriangleArea( &a, &b );

}


Scalar getTriangleArea( Vector2D* a, Vector2D* b ) {

  return fabs ( Vector2D::ProductVector(*a, *b) * 0.5 );

}

Vector2D getTriangleCenterMass( Vector2D* c1, Vector2D* c2, Vector2D* c3 ) {

  Vector2D a = *c2 - *c1;
  Vector2D b = *c3 - *c1;
  return getTriangleCenterMass( &a, &b );

}

Vector2D getTriangleMediana( Vector2D* a, Vector2D* b ) {

  Vector2D c = *b - *a;
  Vector2D mediana = *a + c * 0.5;
  return mediana;

}

Vector2D getTriangleCenterMass( Vector2D* a, Vector2D* b ) {

  Vector2D mediana = getTriangleMediana(a, b);
  Vector2D to_center = mediana * (2.0/3.0);

  return to_center;

}


// вычисляет площадь многоугольника
Scalar getPolygoneArea( CornersList* corners ) {
    return fabs( getPolygoneAreaOriented( corners ) );
}


/**
 * Возвращает ориентированную площадь многоугольника
 * если многоугольник закручен по часовой стрелке - положительна,
 * иначе отрицательна.
 * @param corners список углов
 * @return ориентированная площадь
 */
Scalar getPolygoneAreaOriented( CornersList* corners ) {

  Scalar res = 0.0;

  // проверка того, что многоугольник состоит как минимум из 3х углов
  assert ( corners->size() >= 3 );

  CornersList::iterator first = corners->begin();
  CornersList::iterator end = corners->end();
  CornersList::iterator i = first;

  while ( i != end ) {

    CornersList::iterator next = i; next++; // FIXME ужос!
    if ( next == end ) next = first;
    res += ( i->val.x * next->val.y ) - ( i->val.y * next->val.x );
    i++;

  }

  return 0.5 * res;
}


bool VectorsIsCrossing( Vector2D* v1, Vector2D* v2, Vector2D* coord_v2) {

  assert( false );
  return true;

}


// проверим, не попали ли в треугольник "левые" вершины?
bool IsCornersInTriange( Vector2D& a, Vector2D& b, Vector2D& coord, CornersList* corners )
{

  unsigned allowed_corners = 4; // FIXME тут будет 3

  CornersList::iterator i = corners->begin();
  CornersList::iterator end = corners->end();

  while ( i != end ) {

      // координаты вершины относительно треугольника
      Vector2D versh = *i - coord;

      // если вершина попала
      if( IsPointInTriangle( versh, a, b ) ) {
          allowed_corners--;
          // исключены 3 вершины треугольника, значит попала левая
          if( !allowed_corners ) return true;
      }

      i++;
  }

  return false;

}


// алгоритм триангуляции поверхности методом "обрезания ушек" :)
TrianglesList* getPolygoneTriangulation( CornersList* corners_list ) {
/* TODO глючило на таком:
std::list<Vector2D> crns;
crns.push_back ( Vector2D (  1.0, -1.0) );
crns.push_back ( Vector2D (  1.0, 1.0) );
crns.push_back ( Vector2D (  -1.0, 1.0) );
crns.push_back ( Vector2D (  -1.0, -1.0) );
crns.push_back ( Vector2D ( -2.0, -5.0) );
*/
  assert( corners_list );
//  assert( corners_list->size() >= 3 );

  // сюда сложим результат
  TrianglesList* triangle_list = new TrianglesList;

  // скопируем лист себе и будем его кромсать
  CornersList corners_copy = *corners_list;

  // берём первые три точки на многоугольнике
  CornersList::iterator temp = corners_copy.begin();
  CornersList::iterator p3 = temp;
  temp++;
  CornersList::iterator p2 = temp;
  temp++;
  CornersList::iterator p1 = temp;
  CornersList::iterator end = corners_copy.end();

  Triangle tri;

  while ( corners_copy.size() > 2 ) {

    // берём три стороны треугольника
    Vector2D dir1 = *p2 - *p3;
    Vector2D dir2 = *p1 - *p2;
    Vector2D dir3 = *p1 - *p3;

    // выпуклое ли это "ушко" многоугольника?
    // проверим с помощью векторного произведения
    if( Vector2D::ProductVector( dir1, dir3 ) >= 0 &&

      // проверим, не попали ли внутрь ушка другие углы
      // p3 = координаты этого треугольника
      !IsCornersInTriange( dir3, dir1, *p3, &corners_copy ) ) {

        // добавляем треугольник в триангуляцию
        tri.c1 = *p3;
        tri.c2 = *p2;
        tri.c3 = *p1;

        triangle_list->push_back( tri );

        // удаляем обработанную вершину
        corners_copy.erase( p2 );

    } else {

      // треугольник неподходящий - передвигаемся к следующему
      p3 = p2;
    }

    p2 = p1;
    p1++; if( p1 == end ) p1++;
  }

  return triangle_list;

}


Vector2D getPolygoneCenterMass( CornersList* corners_list )
{
assert( false ); // ненужна функция уже
    // проверка того, что многоугольник состоит как минимум из 3х углов
    assert ( corners_list->size() >= 3 );

    Vector2D res;

    CornersList::iterator i = corners_list->begin();
    CornersList::iterator end = corners_list->end();

    // формула вычисления центра масс:
    //Xc = (X1 + ... + XN)/N Yc = (Y1 + ... + YN)/N
    while ( i != end ) {
        res.val.x += i->val.x;
        res.val.y += i->val.y;
        i++;
    }

    res /= ( corners_list->size() * 1.0 );

    return res;

}


/*

Hi, I have been doing the same, and then I found this website…
 I think you people may like to know my solution as it is rather elegant (I think):
 I calculate the moment of inertia of a triangle through its center of mass using
  the medians (the lines from halfway each side to the opposite corners). The center
   of mass is the crossing point of the three medians; it is at 2/3 of each median.
    I call the lenght of the medians p,q, and s.

 Now the moment of inertia is: I=M*(p^2+q^2+s^2)/27

 You can find this by deviding the triangle in 4 equal triangles by connecting the mids
  of the sides and then calculating the moment of inertia in a recursive manner.

About concave polygons: They are indeed no problem. I call the angles of the polygon A1,A2,A3,… and the center O. Now if the angle A1_O_A2 is positive, you add this triangle, if it is negative you substract it. And so on for each corner. I hope this explanation is understandable… Draw it out and you will see it works perfect!

((c)Manuel, индус наверно)
*/

Scalar getTriangleMomentOfInertia( Vector2D* p1, Vector2D* p2, Vector2D* p3, Scalar density )
{

    Scalar mass = getTriangleArea( p1, p2, p3 ) * density;

    Vector2D center = getTriangleCenterMass( p1, p2, p3);

    // находим стороны
    Vector2D a = *p2 - *p1;
    Vector2D a_minus = -1 * a; // для удобства поиска медиан
    Vector2D b = *p3 - *p1;
    Vector2D c = *p3 - *p2;

    // медианы полученных треугольников
    Scalar m1 = getTriangleMediana( &a, &b ).length();
    Scalar m2 = getTriangleMediana( &b, &c ).length();
    Scalar m3 = getTriangleMediana( &a_minus, &c ).length();

    return mass * (m1*m1 + m2*m2 + m3*m3) / 27.0;

}

// TODO проверить верно ли предположение, заюзанное в этой функции (про квадрат расстояния)
Scalar getTriangleMomentOfInertiaFromCorner( Vector2D* a, Vector2D* b, Scalar density ) {

    Scalar area = getTriangleArea(a, b);
    Scalar mass = area * density;
    Scalar len = getTriangleMediana(a, b).length() * (2.0/3.0);
    return  mass * pow(len, 2.0) / 50.0;

}


Scalar getPolygoneMomentOfInertia( CornersList* corners_list, Scalar density )
{

    Scalar res = 0.0;

    Vector2D center = getPolygoneCenterMass( corners_list );
    TrianglesList* triangles = getPolygoneTriangulation( corners_list );

    TrianglesList::iterator i = triangles->begin();
    TrianglesList::iterator end = triangles->end();

    while ( i != end ) {

      Scalar moment = getTriangleMomentOfInertia( &i->c1, &i->c2, &i->c3, density );
      Scalar mass = density * getTriangleArea( &i->c1, &i->c2, &i->c3 );
      Vector2D triangle_center = getTriangleCenterMass( &i->c1, &i->c2, &i->c3 );

      res += moment + mass * pow( triangle_center.length(), 2.0);
      i++;

    }

    return 2.0/3.0;

}


// Vector2D ShortestDirectionToPoint( Vector2D* vect, Vector2D* point, Vector2D* vector_nearest_point )
// {
// 
//   assert( vector_nearest_point );
// 
//   // проекция из точки на вектор
//   Vector2D proj = Vector2D::Projection(*point, *vect);
// 
//   // падает ли проекция не ближе чем на начало вектора?
//   if( !vect->IsCollinear( &proj ) ) {
//       // точкой начала направления является начало вектора
//       *vector_nearest_point *= 0;
//       // тогда возвращаем расстояние от начала вектора до точки
//       return *point;
//   }
// 
//   // ушла ли проекция дальше вектора?
//   if( proj.length() > vect->length() ) {
//       // точкой отсчёта начала направления является конец вектора
//       *vector_nearest_point = *vect;
//       // возвращаем расстояние от конца вектора до точки
//       return *point - *vect;
//   }
// 
//   // значит точка начала отсчёта направления это проекция на вектор
//   *vector_nearest_point = proj;
//   // остаётся вариант вернуть нормаль к точке
//   return *point - proj;
// 
// }


// кратчайшее расстояние до одного из концов вектора (вспомогательная функция)
/**
 * 
 * @param v1 вектор, от которого меряется направление
 * @param v2 вектор, до концов которого меряется расстояние
 * @param v2_coords координаты второго вектора
 * @param direction результат: направление на ближайший к v1 конец вектора v2
 * @param v1_direction_coords - результат: начало направления
 */
void ShortestDirectionToVectorEnds(Vector2D* v1, Vector2D* v2, Vector2D* v2_coords, Vector2D* direction, Vector2D* v1_direction_coords)
{

  assert( direction );
  assert( v1_direction_coords );

  // расстояние до начала вектора
  Vector2D res1_from; // точка откуда "растёт" расстояние
  Vector2D res1 = v1->ShortestDirectionToPoint( v2_coords, &res1_from );

  // и до его конца
  Vector2D v2_end = *v2_coords + *v2;
  Vector2D res2_from; // точка откуда "растёт" расстояние
  Vector2D res2 = v1->ShortestDirectionToPoint( &v2_end, &res2_from );

  // возвращаем меньшее из расстояний
  if ( res1.length() < res2.length() ) {
         *v1_direction_coords = res1_from;
         *direction = res1;
  } else {
         *v1_direction_coords = res2_from;
         *direction = res2;
  }

}

// кратчайшее расстояние от вектора до вектора
/**
 * 
 * @param v1 вектор
 * @param v2 второй вектор
 * @param v2_coord координаты второго вектора относительно первого
 * @param direction результат: направление
 * @param direction_coord результат: координаты направления
 * @return true если зафиксировано пересечение векторов
 */
bool ShortestDirectionToVector( Vector2D* v1, Vector2D* v2, Vector2D* v2_coord, Vector2D* direction, Vector2D* direction_coord )
{

  assert( direction );
  assert( direction_coord );

  /*
  алгоритм поиска кратчайшего расстояния:

  1. векторы пересекаются и расстояние = 0
  2. наименьшими будут расстояния от начала или конца другого вектора до нашего вектора
  3. наименьшими будут расстояния от начала или конца текущего вектора до другого вектора
  4. векторы параллельны и достаточно взять нормаль до любой точки вектора
  (не рассматриваем этот случай в алгоритме, т.к. точка отсчёта
  направления одна, а в этом случае их будет бесконечное множество)
  */

  // проверим, вдруг векторы пересекаются
  if ( v1->VectorsIsCrossing( v2, v2_coord, direction_coord ) ) {
      // координата точки пересечения уже сохранена
      // осталось обнулить направление
      direction->Reset();
      return true;
  }

  Vector2D rev_coord = -*v2_coord;

  // до концов вектора v2 от v1
  Vector2D point1;
  Vector2D dir1;
  ShortestDirectionToVectorEnds( v1, v2, v2_coord, &dir1, &point1 );

  // до концов вектора v1 от v2
  Vector2D point2;
  Vector2D dir2;
  ShortestDirectionToVectorEnds( v2, v1, &rev_coord, &dir2, &point2 );

  // сравниваем и отдаём ту длину и координату к ней, что размером меньше
  if ( dir1.length() <= dir2.length() ) {
            *direction_coord = point1;
            *direction = dir1;
  } else {
            *direction_coord = *v2_coord + point2 + dir2;
            *direction = -dir2; // направление переворачиваем, потому что измеряли относительно другого вектора
  }

  return false;
}


// удваивает количество углов полигона
void DoubleCorners( CornersList& list )
{
  CornersList::iterator i = list.begin();
  CornersList::iterator end = list.end();
  CornersList::iterator prev = end; prev--;

  while ( i != end ) {

    // добавляем точку в середину каждого ребра
    Vector2D new_point = (*i - *prev) * 0.5 + *prev;
    list.insert( i, new_point );

    prev = i;
    i++;
  }
}


VectorsList* GetStarPolygone( unsigned num_axis, const Vector2D& main_axis, const Vector2D& middle_axis )
{
  Vector2D main_axis_ = main_axis;
  Vector2D middle_axis_ = middle_axis;

  VectorsList* result = new VectorsList();

  Scalar angle_step = fast_math::pi2 / num_axis;

  Vector2D simmetry(-1.0, tan( angle_step/2.0 ) );
  Vector2D v_mid2 = middle_axis_.Rotated( -2.0 * ( middle_axis_ ^ simmetry ) );
  Vector2D to_crossing = middle_axis_ - main_axis_; 
  Vector2D v_min = simmetry.CrossingPoint( &to_crossing, &main_axis_ );

  for( unsigned num = 0; num < num_axis; num ++ ) {
      Scalar angle = num * angle_step;
      result->push_back( v_mid2.Rotated( -angle ) );
      result->push_back( v_min.Rotated( -angle ) );
      result->push_back( middle_axis_.Rotated( -angle ) );
      result->push_back( main_axis_.Rotated( -angle ) );
  }

  return result;
}


bool IsPointOnRight( Vector2D& vector, Vector2D& point )
{
  if( vector.ClassifySimple( point ) == Vector2D::RIGHT ) return true;
    else return false;
}

bool IsPointOnLeft( Vector2D& vector, Vector2D& point )
{
  // точка слева или лежит на прямой
  if( vector.ClassifySimple( point ) != Vector2D::RIGHT ) return true;
    else return false;
}

bool IsPointInTriangle( Vector2D& point, Vector2D& a, Vector2D& b )
{

  Vector2D c = a - b;
/*
PR( IsPointOnRight( a, point) );
PR( IsPointOnRight( b, point) );
PR( IsPointOnRight( c, point) );
*/
  Vector2D coord4c = point - b;
  bool b_ = IsPointOnLeft( b, point);

  // если точка лежит с одной стороны каждого вектора то она внутри
  if( IsPointOnLeft( a, point)    != b_ &&
      IsPointOnLeft( c, coord4c ) == b_ ) return true; else return false;

}


bool IsPointInTriangle( Vector2D& point, Vector2D& p1, Vector2D& p2, Vector2D& p3 )
{
    Vector2D a = p2 - p1;
    Vector2D b = p3 - p1;
    Vector2D point_ = point - p1;

    return IsPointInTriangle( point_, a, b );
}
