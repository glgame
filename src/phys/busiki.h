#ifndef BUSIKI_H_
#define BUSIKI_H_

#include "obj_disk.h"
#include "obj_rope.h"

// делает бусы из дисков и верёвок :)

class obj_Busiki : public physics_obj
{

//   typedef std::list<physics_obj*> AllObjects;
//   AllObjects all_objects;

public:
  obj_Busiki (
            force_point_obj* start, force_point_obj* end,
            unsigned num, // количество бусинок
            unsigned rope_num_segments, // количество сегментов в веревке
            Scalar rope_len_segments, // длина сегментов в веревке
            Scalar radius,
            Scalar hinges_radius
            );


  ~obj_Busiki();

  Vector2D getCoords();
  void getDimensionsBox(Vector2D *const v1, Vector2D *const v2);
    void display();

    void physics_forces_reset();
    void physics_forces_calculate();
    void physics_forces_apply();

};

#endif
