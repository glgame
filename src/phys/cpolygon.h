#ifndef CPOLYGON_H
#define CPOLYGON_H

#include "cgeom.h"
#include "cbasepolygon.h"
#include "cconvexpolygon.h"


/**
  @brief Простой (не самопересекающийся!) многоугольник

  @author Феклушкин Денис <edu2005-60@mail.ru>
*/
class CPolygon : public CBasePolygon
{
    /// список выпуклых многоугольников, составляющих данный многоугольник
    typedef std::list<CConvexPolygon> ConvexList;
    ConvexList convex_list;

	/// Список треугольников из которых состоит полигон
	TrianglesList* trianglesList;

public:
    CPolygon();
    CPolygon( ItersList* corners );
    ~CPolygon();

    /// (копирование не реализовано)
    CPolygon( const CConvexPolygon& src ){ assert( false); };

	/// Подсчитать емкость момента инерции ( чтобы получить момент нужно домножить на плотность )
	Scalar CalcMomentOfInertiaCapacity( Vector2D* axis = 0 );

    void NewCorners( ItersList* corners );
};

#endif
