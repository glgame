#ifndef OBJ_DISK_H
#define OBJ_DISK_H

#include <list>

#include "../math/vector2d.h"
#include "../hinge_obj.h"
#include "obj_body.h"

#include "../input/eventbase.h"
#include "../input/eventmouseclick.h"


/**
Класс реализующий диск (круглую такую хуёвину)

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class obj_Disk : public obj_Body, private EventMouseClick
{

    Scalar   radius;

    void MouseClick();
    bool Filtered();
    void set_defaults();

    Scalar CalculateMass() const;
    Scalar CalculateMomentOfInertia() const;

public:
    obj_Disk(Vector2D* center, Scalar radius);
    ~obj_Disk();

    void Init();
    void display();
    void physics_forces_calculate();
    void getDimensionsBox(Vector2D *const v1, Vector2D *const v2);
    Scalar getMomentOfInertia() const { return moment_inertia; }
    Vector2D NearestPointOfNutshell( Vector2D* point, bool* is_inside );

    Scalar getRadius() const { return radius; }
    Scalar getMass() const { return mass; }
    Vector2D getCenterMass() const { return Vector2D(); }

    // ближайшая к вектору точка границы круга
    Vector2D NearestPointFromVector( Vector2D* vector, Vector2D* body_coord, Vector2D* res_from_point );

};

#endif
