#include "obj_rope.h"

obj_Rope::obj_Rope( force_point_obj* start, force_point_obj* end, int num_segments )
: physics_obj()
{

  obj_Rope::start = start;
  obj_Rope::end = end;
  obj_Rope::num_segments = num_segments;
  obj_Rope::seg_length = 0;

}


obj_Rope::obj_Rope( force_point_obj* start, force_point_obj* end, int num_segments, Length seg_length )
: physics_obj()
{

  obj_Rope::start = start;
  obj_Rope::end = end;
  obj_Rope::num_segments = num_segments;
  obj_Rope::seg_length = seg_length;

}


obj_Rope::~obj_Rope()
{

}


void obj_Rope::Init()
{

  assert ( start );
  assert ( end );
  assert ( num_segments > 0 );

  // найдем координаты относительно родителя
  setCoords( getParent()->getCoord() );

  // найдём вектор шага сегментов создаваемой верёвки
  Vector2D v_start = start->getCoord();
  Vector2D v_end = end->getCoord();
  Vector2D step = ( v_end - v_start ) / num_segments;


  // создаём верёвку
  Vector2D curr = v_start;

  force_point_obj *from, *to;
  obj_SegmentRope* segment;

  from = start;

  for(unsigned i = 1; i < num_segments; i++) {

    curr += step;

    // создаём точечное тело - шарнир
    hinge_obj* hinge_new = new hinge_obj(curr.val.x, curr.val.y);
    hinge_new->setParent( this );
    hinge_new->SetMass( 0.00001 );
    hinge_new->Init();

    // берем от шарнира точку крепления
    to = hinge_new->GetHinge();

    segment = new obj_SegmentRope(from, to);
    segment->setParent( this );

    if (seg_length > 0) segment->setLength ( seg_length );
    from = to;

  }

  segment = new obj_SegmentRope(from, end);
  segment->setParent ( this );
  if(seg_length > 0) segment->setLength ( seg_length );

}


void obj_Rope::getDimensionsBox(Vector2D *const v1, Vector2D *const v2){

  assert ( v1->length() == 0 );
  assert ( v2->length() == 0 );

}


void obj_Rope::display(){

}


void obj_Rope::physics_forces_reset(){}
void obj_Rope::physics_forces_apply(){}
void obj_Rope::physics_forces_calculate(){}
