#ifndef OBJ_POLYGONEBODY_H
#define OBJ_POLYGONEBODY_H

#include "obj_body.h"
#include "../math/fast_math.h"
#include "../input/eventmouseclick.h"
#include "geometry.h"
#include "cpolygon.h"


/**
класс тела из треугольников

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
/*
class obj_PolygoneBody : public obj_Body, public EventMouseClick, public CPolygon
{
protected:
    VectorsList rotated_corners;
    CDumbPolygon* rotated_polygon;

    // поворачивает углы повёрнутого многоугольника на заданный угол
    void CalculateRotatedCorners();

//    TrianglesList* triangles_list; // триангуляция фигуры

public:
    obj_PolygoneBody();
    ~obj_PolygoneBody();

    void AddCornersList( VectorsList* corners_list );
    virtual void Init();

    void display();
    virtual void getDimensionsBox(Vector2D* v1, Vector2D* v2);
    void physics_forces_calculate();

    // подсчёт момента инерции
    virtual Scalar CalculateMomentOfInertia();
    Scalar CalculateMass();

    static void SplitInto2Lists( VectorsList* from, VectorsList& res1, VectorsList& res2 );
    Vector2D CalculateCenterMass();

protected:
    void MouseClick();
};
*/
#endif
