#include "ccurvepolygon.h"


CCurvePolygon::CCurvePolygon()
{
  controls_list = 0;
}


CCurvePolygon::CCurvePolygon( const CCurvePolygon& src )
  : CPolygon( src )
{
  if( this == &src ) return;

  controls_list = new ItersList;
  *controls_list = *src.controls_list;
}


CCurvePolygon::~CCurvePolygon()
{
  delete controls_list;
}


CCurve CCurvePolygon::EdgeGet( const iter_edge& i ) const
{
    assert( corners_list->size() >= 2 );
    assert( corners_list->size() >= 2 );

    iter_edge j = i;

    CCurve res;

    Vector2D coord = **j;
    res.curvature = **j.control - coord;

    ++j; 
	if( j == corners_list->end() ) 
		j = corners_list->begin();

    res.direction = **j - coord;

    return res;
}


/*!
    \fn CCurvePolygon::IsPointInside( Vector2D& point )
 */
bool CCurvePolygon::IsPointInside( Vector2D& point )
{
    // временный список выпуклых сторон:
    typedef den::list<iter_edge> i_e_list;
    i_e_list convex_edges;

    // пробежим по вогнутым
    iter_edge i(corners_list, controls_list);
    ItersList::iterator end = corners_list->end();

    while( i != end ) {
      // кривая
      CCurve c = EdgeGet( i );

      // проверим на вогнутость
      if( c.IsConcave() ) {
        // точка относительно кривой
        Vector2D point_ = point - **i;
        if( c.IsPointInsideCurve( point_ ) ) {
          // точка попала в вогнутую часть - значит в фигуру не попала
          MSG("inside CONCAVE curve");
          return false;
        }
      } else {
        convex_edges.push_back( i ); // выпуклые сохраним для будущего использования
      }
      ++i;
    }

    // пробежимся по выпуклым
    i_e_list::iterator j = convex_edges.begin();
    i_e_list::iterator jend = convex_edges.end();

    while( j != jend ) {
      // точка относительно кривой
      Vector2D point_ = point - ***j;
      if( EdgeGet( *j ).IsPointInsideCurve( point_ ) ) {
        MSG("inside convex");
        return true; // точно принадлежит фигуре
      }
      j++;
    }

    // попали только внутрь многоугольника
    if( CPolygon::IsPointInside( point ) ) {
        MSG("inside polygon");
        return true;
    } else return false;
}


Scalar CCurvePolygon::GetArea() const
{
    Scalar area = CPolygon::GetArea();

    iter_edge i(corners_list, controls_list);
    ItersList::iterator end = corners_list->end();

    while( i != end ) {
      CCurve c = EdgeGet( i );
      area += c.CalculateArea();
      ++i;
    }

    return area;
}
/*
  Считаем емкость момента инерции полигона и кривых безье.
  Складываем или вычитаем в зависимости от ориентации кривой.

  @param axis - Необязательный параметр - ось( точка ) вращения
  @author Александр Оплеснин <gasparfx@gmail.com>
 */
Scalar CCurvePolygon::CalcMomentOfInertiaCapacity( Vector2D* axis )
{
	/// Ось вращения
	Vector2D axisOfRotation;

	/// Если ось не задана - берем центр масс
	if ( !axis ){
		axisOfRotation = GetCenterMass();
	} else {
		axisOfRotation = *axis;
	}

	/// Считаем емкость момента инерции полигона
	Scalar momentInertia = CPolygon::CalcMomentOfInertiaCapacity( &axisOfRotation );
	
	iter_edge i(corners_list, controls_list);
    ItersList::iterator end = corners_list->end();
	/// Пробегаем все стороны
    while( i != end ) {
      CCurve c = EdgeGet( i );
 	  /// Корректируем емкость момента инерции в соответствии с теоремой Гюйгенса-Штейнера
	  Scalar momentCorrection = c.CalculateArea() 
		                * ( ( c.CalculateCenterMass() + (**i) - axisOfRotation ).length() )
						* ( ( c.CalculateCenterMass() + (**i) - axisOfRotation ).length() );
	  /// Если не выпуклая - вычитаем
	  if ( c.IsConcave() ){
		  momentInertia -= fabs( c.CalcMomentOfInertiaCapacity() ) + fabs( momentCorrection );
	  } else {
	  /// Если выпуклая - плюсуем
	  momentInertia += fabs( c.CalcMomentOfInertiaCapacity() ) + fabs( momentCorrection );
	  }
	  /// Идем к следующей грани
      ++i;
    }
	
	return momentInertia;
}


Vector2D CCurvePolygon::GetCenterMass() const
{
    assert( corners_list->size() >= 2 );
    assert( controls_list->size() >= 2 );

    Vector2D center;

    iter_edge i( corners_list, controls_list );
    ItersList::iterator end = corners_list->end();

    while ( i != end ) {
        CCurve curve = EdgeGet( i );
        center += curve.CalculateCenterMass() + **i;
        ++i;
    }

    return center / corners_list->size();
}


void CCurvePolygon::EdgeRemove( iter_edge& i )
{
    controls_list->erase( i.control );
    corners_list->erase( i );

    assert( corners_list->size() >= 2 );
}


void CCurvePolygon::EdgeSplit( iter_edge& i, Scalar& t )
{
    CCurve edge = EdgeGet( i );
    CCurve c1, c2;
    edge.Split( t, c1, c2 );

    EdgeInsert( i, c2 );
    EdgeInsert( i, c1 );
    EdgeRemove( i );
}


void CCurvePolygon::EdgeInsert( iter_edge& i, CCurve& curve )
{
  assert( false ); // непонятно пока как реализовывать
//    controls_list->insert( i.control, curve.curvature );
//    corners_list->insert( i, curve.direction );
}


/*!
    \fn CCurvePolygon::NewControls( ItersList* controls )
 */
void CCurvePolygon::NewControls( ItersList* controls )
{
    assert( controls );
    assert( controls->size() == corners_list->size() );

    delete controls_list;
    controls_list = controls;
}
