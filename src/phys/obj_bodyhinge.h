#ifndef OBJ_BODYHINGE_H
#define OBJ_BODYHINGE_H

#include <list>
#include "force_point_obj.h"
#include "../linked_list_t.h"

/**
  @brief Шарнир закреплённый на теле

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/

class obj_BodyHinge : public force_point_obj

{
public:
    
    obj_BodyHinge();
    ~obj_BodyHinge();

    /// Список шарниров
    typedef std::list<obj_BodyHinge*> HingesList;

    /// Координаты шарнира относительно тела
    Vector2D relative_coords;

    /// Сохранить итератор на этот шарнир в списке родительского объекта
    inline void setIter ( HingesList::iterator iter ) { this_iter = iter; };

    /// Взять итератор на этот шарнир в списке родительского объекта
    inline HingesList::iterator getIter() const { return this_iter; };

private:
    /// Указывает на текущий шарнир в списке шарниров тела
    HingesList::iterator this_iter;
};




#endif
