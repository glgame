#ifndef PHYSICS_OBJ_H
#define PHYSICS_OBJ_H

#include <list>
#include "../math/vector2d.h"
#include "../debug.h"
#include "../screen.h"
#include "../template_obj_tree.h"


/**
@brief Физический объект

Физика основана на передаче импульсов между телами (так называемый "импульсный движок"). При этом по порядку циклически вызываются следующие функции (каждая - для всех объектов):

virtual void physics_forces_reset() = 0;      ///< Сброс сил
virtual void physics_forces_calculate() = 0;  ///< Вычисление сил
virtual void physics_forces_apply() = 0;      ///< Применение сил


	@author Феклушкин Денис <edu2005-60@mail.ru>
*/


class physics_obj :
 public template_obj_tree<physics_obj>,
 public linked_list_t<physics_obj>
{

public:
    /// Верхний элемент дерева игрового мира. С него начинается рисование всего мира.
    static physics_obj* first_object_in_world;

    virtual void Init(); ///< Инициализация объекта после настройки всех параметров

    virtual void physics_forces_reset() = 0;      ///< Сброс сил
    virtual void physics_forces_calculate() = 0;  ///< Вычисление сил
    virtual void physics_forces_apply() = 0;      ///< Применение сил

    physics_obj();
    virtual ~physics_obj();

    static void all_forces_calculate(); ///< Подсчёт сил все объектов данного мира
    static void all_forces_reset();     ///< Сброс сил всех объектов данного мира
    static void all_forces_apply();     ///< Применение сил всех объектов данного мира

    /// "Сопротивление воздуха" (сопротивление прямолинейному движению)
    /// Применяется в основном не для реализма а для нейтрализации погрешностей вычислений
    static double air_resistance;

    /// Сопротивление вращению (не путать с моментом инерции!)
    /// Применяется в основном не для реализма а для нейтрализации погрешностей вычислений
    static double moment_resistance;

    static double dt; ///< Приращение времени за одну итерацию движка
    static Vector2D g; ///< Сила гравитации

    static void display_all(); ///< Показать все объекты данного мира

    bool visible; ///< Отображать или нет объект на экране?

    /// Найти направление к данному объекту от указанного объекта (FIXME:ненужная функция?)
    Vector2D getCoordsFrom ( physics_obj* ptr );

    /// Найти направление от данного объекта к указанному объекту (FIXME:ненужная функция?)
    Vector2D getCoordsTo ( physics_obj* ptr );

    /// Найти направление от родительского объекта на данный объект (FIXME:ненужная функция?)
    inline Vector2D getCoordsFromParent() { return getCoordsFrom ( getParent() ); }

    /// Найти координаты объекта (не путать с центром тяжести!)
    inline Vector2D getCoord() const { return coord; }

    /// Установить координаты объекта
    inline void setCoords (const Vector2D coords) { coord = coords; }
    
    /// Установить координаты объекта
    inline void setCoords (const Vector2D* coords) { coord = *coords; }

    /// Изменить координаты объекта сдвинув его
    inline void Move( const Vector2D incr ) { coord += incr; }

    /// Заполнить 2 вектора габаритов объекта с детьми
    void getDimensionsBoxWithChilds(Vector2D* l_d, Vector2D* r_u);

    /// Отобразить объект с детьми в OpenGL
    virtual void DisplayWithChilds();

protected:
    /// Возвращает 2 вектора из координат объекта, указывающие углы текущего габаритного ящика
    virtual void getDimensionsBox(Vector2D *const v1, Vector2D *const v2) = 0;

private:
    Vector2D coord; ///< Координаты объекта

    /// Возвращает 2 вектора из координат объекта, указывающие углы текущего габаритного ящика
    /// Удобна для внутреннего применения (рекурсия из getDimensionsBoxWithChilds())
    void getDimensionsBoxWithChilds2(Vector2D shift, Vector2D* l_d, Vector2D* r_u);

    /// Отобразить объект (без детей) в OpenGL
    virtual void display() = 0;

};

#endif
