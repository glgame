#include "obj_curvebody.h"
obj_CurveBody::obj_CurveBody()
{
  original_corners = 0;
  original_controls = 0;
  DisplayCorners = true;
}


obj_CurveBody::~obj_CurveBody()
{
  delete controls_list;
//  delete rotated_polygon;
}


// внутренний инит, не для вызова снаружи
void obj_CurveBody::Init()
{
    assert( original_corners );
    assert( original_controls );
    assert( original_corners->size() == original_controls->size() );

    // заполняем список повёрнутых углов и контролек
    CalculateRotatedCorners();

    // заполним списки итераторов
    NewCorners( CGeom::GetItersList( &corners_vectors ) );
    NewControls( CGeom::GetItersList( &controls_vectors ) );

    // заполним список острых углов
    FillSharpCorners();

    mass = CalculateMass();
    center_mass = CalculateCenterMass();
    moment_inertia = CalculateMomentOfInertia();

    obj_Body::Init();
}


void obj_CurveBody::NewPoints( VectorsList* corners, VectorsList* controls )
{
    assert( corners );
    assert( controls );
    assert( corners->size() == controls->size() );

    delete original_corners; original_corners = corners;
    delete original_controls; original_controls = controls;

    Init();
}


void obj_CurveBody::display()
{

    CPolygon::display();
    glColor( CurveBody );
    glLineWidth ( 2.0f);

   // display center mass
    const float size = 0.1;
    DisplayCross( GetCenterMass(), size );

    // display body
    iter_edge i( corners_list, controls_list );
    ItersList::iterator end = corners_list->end();

    while ( i != end ) {

        CCurve curve = EdgeGet( i );
        if( curve.IsConcave() ) curve.setColor( ButtonColorChecked );
          else curve.setColor( CurveBody );
        if( !DisplayCorners ) curve.DisplayDots = false;
        glPushMatrix();
        glTranslate ( **i );
        curve.display();
        glPopMatrix();

        ++i;
    }

    glLineWidth ( 1.0f);

}


void obj_CurveBody::getDimensionsBox(Vector2D* v1, Vector2D* v2)
{

    assert ( v1->length() == 0 );
    assert ( v2->length() == 0 );

    iter_edge i( corners_list, controls_list );
    ItersList::iterator end = corners_list->end();

    while ( i != end ) {

        CCurve curve = EdgeGet( i );
        Vector2D dim1, dim2;
        curve.GetDimensionsBox( dim1, dim2 );
        dim1 += **i;
        dim2 += **i;

        Vector2D::MergeVector ( dim1, v1, v2 );
        Vector2D::MergeVector ( dim2, v1, v2 );

        ++i;
    }

}


void obj_CurveBody::physics_forces_calculate()
{
    CalculateRotatedCorners();
	ApplyForce( g * mass ); // вес тела это сила тянущая его вниз;
//    RotatedToAngle( *controls_list, rotated_controls, center_mass, angle ); // поворот всех контролек

}


void obj_CurveBody::MouseClick()
{

    // координаты клика в GL относительно центра экрана
    SDL_Event event = HumanInterface::getEvent();
    Vector2D click_coords = Screen::Pixel2GL ( event.button.x, event.button.y );

    Scalar len = (click_coords - getCoord()).length();

    Vector2D v1, v2;
    getDimensionsBox( &v1, &v2 );

    // проверим, рядом ли был клик?
    if ( len < v1.length() || len < v2.length() ) {

      // координаты клика относительно тела
      Vector2D body_click = click_coords - getCoord();

      // о, рядом. значит надо точно проверить
      if( IsPointInside( body_click )) {
        ApplyForce( Vector2D ( 0.0f, 2.9f) );
        apply_moment( -15.5 * dt );
      }
    }

}

/**
 @param axis Необязательный параметр - ось вращения
 @author Александр Оплеснин <gasparfx@gmail.com>
*/
Scalar obj_CurveBody::CalculateMomentOfInertia(Vector2D* axis) // FIXME Очень медленный алгоритм
{
    // Считаем емкость момента инерции
    Scalar momentInertiaCapacity = CCurvePolygon::CalcMomentOfInertiaCapacity(axis);
    // Считаем момент инерции
    moment_inertia = momentInertiaCapacity * getDensity();
    return moment_inertia;

    // Это старый алгоритм - надо удалить будет
    //int accuracy = 30;	///< Чем выше это число тем точнее расчет

	///*
	// * minX, minY, maxX, maxY - координаты прямоугольника
	// * полностью содержащего нашу фигуру.
	// * Находятся как крайние значения из списка углов и
	// * контролов.
	// */
	//double minX = ( *( corners_list->begin() ) )->val.x;
	//double maxX = ( *( corners_list->begin() ) )->val.x;

	//double minY = ( *( corners_list->begin() ) )->val.y;
	//double maxY = ( *( corners_list->begin() ) )->val.y;

	//ItersList::iterator i = corners_list->begin();
	//ItersList::iterator end = corners_list->end();

	//while( i != end ) {
	//	if ( (*i)->val.x < minX ) minX = (*i)->val.x ;
	//	if ( (*i)->val.x > maxX ) maxX = (*i)->val.x ;
	//	if ( (*i)->val.y < minY ) minY = (*i)->val.y ;
	//	if ( (*i)->val.y > maxY ) maxY = (*i)->val.y ;
	//	++i;
	//}
	//i = controls_list->begin();
	//end = controls_list->end();

	//while( i != end ) {
	//	if ( (*i)->val.x < minX ) minX = (*i)->val.x ;
	//	if ( (*i)->val.x > maxX ) maxX = (*i)->val.x ;
	//	if ( (*i)->val.y < minY ) minY = (*i)->val.y ;
	//	if ( (*i)->val.y > maxY ) maxY = (*i)->val.y ;
	//	++i;
	//}

	///** Шаг перебора по оси x */
	//double xStep = (maxX - minX) / accuracy;
	//if (xStep < 0.000001) xStep = 0.000001;

	///** Шаг перебора по оси y */
	//double yStep = (maxY - minY) / accuracy;
	//if (yStep < 0.000001) yStep = 0.000001;

	//double momentInertia = 0;
	//Vector2D point;
	//Vector2D centrMass = CalculateCenterMass();
	///*
	// * Перебираем все точки прямоугольника, содержащего нашу фигуру
	// * и если они ей принадлежат, учтем их в вычислении момента инерции
	// */

	//double x;
	//double y;

	//for (x = minX; x < maxX; x += xStep){

	//	for (y = minY; y < maxY; y += yStep){
	//		point.val.x = x + xStep / 2;
	//		point.val.y = y + yStep / 2;

	//		if ( IsPointInside(point) ) {
	//			/*
	//			 * Формула для момента инерции:
	//			 * ∫r²*dm, где
	//			 *  r  - расстояние от центра, до данного куска фигуры
	//			 *  dm - масса данного куска фигуры
	//			 */
	//			momentInertia += (point - centrMass).length() * (point - centrMass).length()
	//				* getDensity() * xStep * yStep;
	//		}
	//	}
	//}

	//return momentInertia;


}


// заполняет список острых углов
void obj_CurveBody::FillSharpCorners()
{
    assert( corners_list );

    ItersList::iterator end = corners_list->end();
    iter_edge i( corners_list, controls_list );
    iter_edge prev = i; --prev; --prev;

    while ( i != end ) {

        if( IsSharpCorner( i ) ) {
            sharp_corners.push_back( *i );
        }

        prev = i;
        ++i;
    }
}


void obj_CurveBody::HandlePenetrations( obj_CurveBody& remote )
{
/*
Vector2D crd = getCoords() - Screen::getCurrentViewCenter();
Vector2D d1, d2;
getDimensionsBox( &d1, &d2 );
d1.DisplayDebug( crd, true );
d2.DisplayDebug( crd, true );

    CCurveList l_curves_list;//( &rotated_corners, &rotated_controls );
    CCurveList r_curves_list;//( &remote.rotated_corners, &remote.rotated_controls );

    CCurveList::iterator l = l_curves_list.GetFirst();
    CCurveList::iterator lend = l_curves_list.GetEnd();
    CCurveList::iterator rend = r_curves_list.GetEnd();

    Vector2D l_direction, l_curvature, l_coord;
    Vector2D r_direction, r_curvature, r_coord;

    while ( l != lend ) {

      l_curves_list.GetCurve( l_direction, l_curvature, l_coord, l );
      CCurve l_curve( l_direction, l_curvature );

      CCurveList::iterator r = r_curves_list.GetFirst();

      while ( r != rend ) {

          r_curves_list.GetCurve( r_direction, r_curvature, r_coord, r );
          CCurve r_curve( r_direction, r_curvature );

          // координата удалённой кривой относителньо локальной
          Vector2D coord = remote.getCoords() - l_coord + r_coord - getCoords();

          // найдём ближайшие точки t двух парабол
          Scalar t_loc, t_rem;
          l_curve.GetNearestPointsOfParabolas( r_curve, coord, t_loc, t_rem );

          // попадает ли направление на параболу в её отображаемую часть?
          if( !( t_loc < 0 || t_loc > 1 ||
                 t_rem < 0 || t_rem > 1 ) ) {

             // находим точку начала направления
             Vector2D cur_point = l_curve.GetParabolaPoint( t_loc );

             // найдём направление на удалённую параболу
             Vector2D cur_dir = coord + r_curve.GetParabolaPoint( t_rem ) - cur_point;

             // лежит ли конец направления внутри фигуры?
             Vector2D end_dir = cur_point + cur_dir;
             if( l_curve.IsPointInsideCurve( end_dir ) ) {

#ifndef NDEBUG
Vector2D crds = getCoords() + cur_point + l_coord - Screen::getCurrentViewCenter();
cur_dir.DisplayDebug( crds, false );
#endif

             }

          }

          r = r_curves_list.GetNext( r );
        }

        l = l_curves_list.GetNext( l );
      }
*/
}


Vector2D obj_CurveBody::CalculateCenterMass()
{
    return CCurvePolygon::GetCenterMass();
}


Scalar obj_CurveBody::CalculateMass() const
{
    return GetArea() * getDensity();
}


// проверка, является ли угол между 2 кривыми острым
bool obj_CurveBody::IsSharpCorner( iter_edge i )
{
  // предыдущий угол
  iter_edge prev = i; --prev;
  if( prev == corners_list->end() ) --prev;

  // недостающая сторона предыдущей кривой
  Vector2D tang2 = **i - **prev.control;

  Vector2D tang1 = **i.control - **i;

  if( ( tang1 ^ tang2 ) > 0.0 ) return true;
    else return false;
}


void obj_CurveBody::CalculateRotatedCorners()
{
    // поворот на угол тела изначальных углов и контрольных точек
    CGeom::RotatedToAngle( *original_corners, corners_vectors, center_mass, angle );
    CGeom::RotatedToAngle( *original_controls, controls_vectors, center_mass, angle );
}
