#include "force_point_obj.h"

GLfloat force_point_obj::size = 0.07f;

force_point_obj::force_point_obj()
{

    init_defaults();
    setCoords( Vector2D ( 0, 0) );

}


force_point_obj::force_point_obj(Vector2D coords)
{

    init_defaults();
    setCoords( &coords );

}

force_point_obj::force_point_obj(Vector2D* coords)
{

    init_defaults();
    setCoords( *coords );

}


void force_point_obj::Init() {

    assert ( getParent() != 0 );

}


force_point_obj::~force_point_obj()
{
}


void force_point_obj::physics_forces_reset()
{
    impulse *= 0;
}

void force_point_obj::physics_forces_calculate()
{
}

void force_point_obj::physics_forces_apply()
{
}

void force_point_obj::ShowDebugVector() {

  Vector2D value = impulse;

  // show value
  if ( show_debug_vector ) {

      glBegin( GL_LINES );

        glColor3f(0.7f, 0.7f, 0.7f);
        glVertex2f( 0.0f, 0.0f );

        glColor3f(0.3f, 0.3f, 0.3f);
        glVertex2f( value.val.x, value.val.y );

      glEnd();

  }


  // show x and y axis
  if ( show_debug_vector_axis && show_debug_vector ) {

      // x axis:
      glBegin( GL_LINES );

        glColor3f(0.9f, 0.2f, 0.2f);
        glVertex2f( 0.0f, 0.0f );

        glColor3f(0.3f, 0.0f, 0.0f);
        glVertex2f( value.val.x, 0.0f );

      glEnd();

      // y axis:
      glBegin( GL_LINES );

        glColor3f(0.2f, 0.9f, 0.2f);
        glVertex2f( 0.0f, 0.0f );

        glColor3f(0.0f, 0.3f, 0.0f);
        glVertex2f( 0.0f, value.val.y );

      glEnd();

  }


    if( show_debug_to_stdout ) {
#ifndef NDEBUG
          std::cout << "Force + Point: " << this
              << " value: " << impulse.length() << std::endl;
#endif
    }
}

void force_point_obj::display()
{

    glColor3f(0.1,0.9,0.9);

    glBegin( GL_LINES );
      glVertex2f( - size,  0);
      glVertex2f(   size,  0);

      glVertex2f(   0,   size);
      glVertex2f(   0, - size);
    glEnd();

    ShowDebugVector();

}


void force_point_obj::getDimensionsBox(Vector2D *const v1, Vector2D *const v2){

    v1->val.x = -size;
    v1->val.y = -size;

    v2->val.x = size;
    v2->val.y = size;

}



/*!
    \fn force_point_obj::init_defaults()
 */
void force_point_obj::init_defaults()
{

    show_debug_vector = false;
    show_debug_vector_axis = true;
    show_debug_to_stdout = false;

}
