#ifndef OBJ_ROPE_H
#define OBJ_ROPE_H

#include <list>

#include "../displayed_obj.h"

#include "../hinge_obj.h"
#include "obj_segmentrope.h"
#include "../debug.h"

/**
попробуем сделать веревку с физикой

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class obj_Rope : public physics_obj
{
//    typedef std::list<physics_obj*> AllObjects;
//    AllObjects all_objects;


    force_point_obj *start, *end;
    unsigned num_segments;
    Length length;
    Length seg_length;


public:
    obj_Rope( force_point_obj* start, force_point_obj* end, int num_segments );
    obj_Rope( force_point_obj* start, force_point_obj* end, int num_segments, Length length );

    ~obj_Rope();

    void Init();

    void display();

    void getDimensionsBox(Vector2D *const v1, Vector2D *const v2);

    void physics_forces_reset();
    void physics_forces_calculate();
    void physics_forces_apply();


};

#endif
