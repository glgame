#include "obj_body.h"

obj_Body::obj_Body()
 : physics_obj()
{
    inited = false;
    MousePicked = false;
    density = 1.0f;
	angle_speed = 0;
	angle = 0;
	moment = 0;
	moment_inertia = 0;
}


obj_Body::~obj_Body()
{
}


void obj_Body::hinges_calculate_all()
{

   // Подсчитываем влияние каждого шарнира
   obj_BodyHinge::HingesList::iterator i = hinges_list.begin();
   while( i != hinges_list.end() ) {

      hinge_calculate(*i);
      i++;

   }

}


void obj_Body::hinge_calculate(obj_BodyHinge* hinge)
{
    Vector2D force = hinge->getForce();

    // Координаты шарнира относительно тела с учётом поворота тела:
    Vector2D coords_rotated = hinge->relative_coords.Rotated( angle );

    ApplyForce( &force, &coords_rotated );
}


void obj_Body::hinge_move(obj_BodyHinge* hinge)
{
    // Поворачиваем и двигаем:
    Vector2D new_coords = hinge->relative_coords.Rotated( angle ) + center_velocity;

    // Сохраняем новые координаты:
    hinge->setCoords( new_coords + getCenter() );
}


void obj_Body::hinges_move_all()
{

  // Двигаем шарниры, прикреплённые к телу
  obj_BodyHinge::HingesList::iterator i = hinges_list.begin();
  obj_BodyHinge::HingesList::iterator end = hinges_list.end();

  while( i != end ) {

     hinge_move(*i);
     i++;

  }

}


void obj_Body::physics_forces_reset()
{

    assert( inited );

    moment = 0;
    center_impulse *= 0;

}


void obj_Body::physics_forces_apply()
{

    assert( inited );

    /* вычисляем действие шарниров ( если они есть )
       потому что тут гарантированно
       уже сделано вычисление всех сил в них */
    hinges_calculate_all();

    // учитываем сопротивление вращению
    moment -= angle_speed * moment_resistance * dt;

    // крутим тело
    assert( moment_inertia );
    angle_speed += moment / moment_inertia;
    angle += angle_speed;

    // учитываем сопротивление воздуха
    ApplyForce( -1 *  center_velocity * air_resistance );

    // перемещаем тело
    assert( mass );
    center_velocity += center_impulse / mass;

    if(!MousePicked) Move( center_velocity );

    // двигаем все шарниры на теле
    hinges_move_all();

}

force_point_obj* obj_Body::addBodyHingeFromCenter(const Vector2D& coords)
{
  Vector2D world_coords = getCoord() + coords; // Абсолютные координаты шарнира

  obj_BodyHinge* new_hinge = new obj_BodyHinge();
  new_hinge->setParent( this ); // Родителем шарнира будет данное тело
  new_hinge->setCoords( world_coords ); // Установка абсолютных координат шарниру
  new_hinge->Init();

  // Сохраняем относительные координаты шарнира
  new_hinge->relative_coords = coords;

  // Сохраняем итератор для быстрого удаления шарнира из списка шарниров тела
  new_hinge->setIter( hinges_list.end() );

  // Добавим шарнир в список шарниров данного тела
  hinges_list.push_back( new_hinge );

  // Внешним функциям незачем знать что эта точка является шарниром:
  return dynamic_cast<force_point_obj*> (new_hinge);
}

void obj_Body::removeBodyHinge(force_point_obj* hinge)
{
// FIXME неправильно это, шарнир должен сам уметь удалять себя из этого списка!
// иначе при удалении шарнира снаружи у нас в списке останется указатель на мусор
  obj_BodyHinge* removing_hinge = dynamic_cast<obj_BodyHinge*> (hinge);
  hinges_list.remove( removing_hinge ); // FIXME fast deleting using iterator need
  delete removing_hinge;

}


/*!
    \fn obj_Body::MousePickUp()
 */
bool obj_Body::MousePickUp(Vector2D pick_world_coords)
{
    MousePicked = true;
    setCoords( pick_world_coords );

    return true;
}


/*!
    \fn obj_Body::MousePickDown()
 */
void obj_Body::MousePickDown(Vector2D pick_world_coords)
{
    MousePicked = false;
    setCoords( pick_world_coords );
}


/*!
    \fn obj_Body::Init()
 */
void obj_Body::Init()
{
    // инициализация происходит однажды
    assert( inited != true );

    // на момент инициализации должен быть установлен родитель тела
    assert( getParent() );

    inited = true;
}


void obj_Body::ApplyForce( Vector2D force )
{

    Vector2D unused_coord;
    ApplyForce( &force, &unused_coord );

}


void obj_Body::ApplyForce( Vector2D* force )
{
    ApplyForce( *force );
}


void obj_Body::ApplyForce( Vector2D* force, Vector2D* coord )
{
    Vector2D impulse = *force * dt;
    ApplyImpulse( &impulse, coord );
}

void obj_Body::ApplyImpulse( Vector2D* impulse, Vector2D* coord )
{

    // перпендикуляр (плечо) к силе от центра масс тела
    Vector2D perp = impulse->NormalToPoint( coord );

    // берём от плеча момент сил
    Scalar berem_moment = Vector2D::ProductVector( *impulse, perp );
    moment += berem_moment;

    // подсчитываем влияние сил шарнира на центр тяжести
    // (хитро - просто берём остаток от того, что взял момент)
//    center_impulse += (*impulse - (impulse->ort() * berem_moment ) );
    center_impulse += *impulse;

}


Scalar getBiggestLengthOfVectors ( Vector2D* v1, Vector2D* v2 )
{

    Scalar v1_len = v1->length();
    Scalar v2_len = v2->length();

    if ( v1_len > v2_len ) return v1_len; else return v2_len;

}


bool obj_Body::BodyNear( obj_Body* other_body )
{

    Vector2D v1, v2;
    getDimensionsBox( &v1, &v2);

    Vector2D g1, g2;
    other_body->getDimensionsBox( &g1, &g2);

    Scalar min_len = getBiggestLengthOfVectors( &v1, &v2 ) +
                     getBiggestLengthOfVectors( &g1, &g2 );

    Scalar to_other_len = ( other_body->getCoord() - getCoord() ).length();

    if ( to_other_len > min_len ) return false; else return true;

}



bool obj_Body::CollisionDetect( obj_Body* other )
{
assert( false );
return false;
//     // если тело далеко то выходим
//     if ( !BodyNear( other ) ) return false;
//
// //    obj_PolygoneBody* remote = dynamic_cast<obj_PolygoneBody*>( body );
//
//     // найдем ближайшее расстояние до тела
//     Vector2D from_point;
//     Vector2D direct = other->NearestDirectToBody( other, &from_point );

}
