#ifndef SEGMENT_OBJ_H
#define SEGMENT_OBJ_H

#include "physics_obj.h"
#include "force_point_obj.h"
#include "../math/fast_math.h"
#include "../math/vector2d.h"
#include "../debug.h"

/**
элементарный сегмент (линия) с 2 шарнирами на концах

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class segment_obj : public physics_obj
{
protected:
    force_point_obj* first_hinge;
    force_point_obj* last_hinge;
    Vector2D   this_segment;
    Vector2D   friction;
    Vector2D   force;
    Length     length;

public:

    void Init();

    segment_obj(force_point_obj* first_hinge, force_point_obj* last_hinge);
    ~segment_obj();
    void display();

    void physics_forces_reset();
    void physics_forces_calculate();
    void physics_forces_apply();

    void getDimensionsBox(Vector2D *const v1, Vector2D *const v2);

    inline void setLength (const Scalar len) { length = len; }
    inline Vector2D getForce() const { return force; };

private:
    void calculate_segment_vector();
};

#endif
