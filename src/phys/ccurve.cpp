#include "ccurve.h"


CCurve::CCurve()
{
    Init();
}


CCurve::CCurve( const CCurve& src )
{
    direction = src.direction;
    curvature = src.curvature;
    Init();
}


CCurve::CCurve( const Vector2D& direction )
{
    CCurve::direction = direction;
    Init();
}


CCurve::CCurve( const Vector2D& direction, const Vector2D& curvature )
{
    CCurve::direction = direction;
    CCurve::curvature = curvature;
    Init();
}


CCurve::~CCurve()
{
}


/*!
    \fn CCurve::Init()
 */
void CCurve::Init()
{
    DisplayDots = true;
    color = ColorCurve;
}


/**
Подсчёт площади фигуры, ограниченной данной кривой и прямой, соединяющей начало и конец кривой.
Площадь кривой положительна если контрольный вектор лежит справа по направлению кривой.
(Аналогично вычислению ориентированной площади многоугольника.)
*/
Scalar CCurve::CalculateArea()
{
    /*
    Площадь сегмента параболы равна 2/3 от площади треугольника.
    Площадь треугольника равна половине векторного произведения
    двух сторон.
    */
    return (2.0/3.0) * Vector2D::ProductVector(curvature, direction) * 0.5;
}


void CCurve::display() const
{

    // получим "векторизацию" кривой
    unsigned num_steps = static_cast<unsigned>( (direction.length() + curvature.length()) * 3 ) + 2; // FIXME округление может сделать?
    VectorsList* line = ConvertToVectors( num_steps );

    VectorsList::iterator i = line->begin();
    VectorsList::iterator end2 = line->end();

    glColor(color);
    glLineWidth( 2.0f );
    glBegin( GL_LINE_STRIP ); // ломанная линия

    glVertex2f( 0.0, 0.0 ); // первая линия

    while ( i != end2 ) {
      glVertex2f( i->val.x, i->val.y );
      i++;
    }

    glEnd();
    glLineWidth ( 1.0 );

    // проставим ограничительные точки в начале и конце ломанной
    if( DisplayDots ) {

        glPointSize( 4.0 );
        glColor(ColorCurveDots);

        glBegin(GL_POINTS);
          glVertex2f( 0.0, 0.0 );
          glVertex2f( direction.val.x, direction.val.y );
        glEnd();

        glPointSize( 1.0 );

    }

    delete line;

}


/**
   @brief Поиск векторов, приблизительно составляющих кривую.

   Результирующий список может быть меньше количества заказанных шагов!

   @param num_steps максимальное количество шагов минус 2 (начало и конец)
   @return список точек кривой, включая начало и конец
 */
VectorsList* CCurve::ConvertToVectors( unsigned num_steps ) const
{

  // имеет смысл вызывать, чтобы поделить кривую хотя бы на 2 вектора
  assert( num_steps >= 2 );

  // создадим лист
  VectorsList* result_list = new VectorsList;

  result_list->push_back( Vector2D() ); // первый сегмент, точно смотрящий в начало

  // проверим, вдруг наша кривая совсем даже не кривая - сэкономим время
  if ( !curvature.IsNull() ) {

    Scalar t_step = 1.0 / num_steps;
    Scalar t = t_step; // первая точка на кривой, отстоит на один шаг от начала

    while( t < 1.0 ) {

        Vector2D new_point = GetParabolaPoint( t );
        result_list->push_back( new_point );

        t += t_step;
    }

  }

  result_list->push_back( direction ); // последний сегмент, точно смотрящий в конец кривой
  return result_list;

}


/**
 * @param t Коэффициент, "проезжающий" направление от начала до конца на участке [0...1]
 * @return Точка на кривой
 */
Vector2D CCurve::GetParabolaPoint( Scalar t ) const
{
    Vector2D res;

    // формула точки на квадратной кривой Безье: 2 * Control * (1-t)*t + End * t^2
    // она же - параметрически заданная наклонная парабола
    res.val.x = 2.0 * curvature.val.x * (1.0 - t) * t + direction.val.x * pow(t, 2.0);
    res.val.y = 2.0 * curvature.val.y * (1.0 - t) * t + direction.val.y * pow(t, 2.0);

    return res;
}


VectorsList* CCurve::GetDirectionToCurveNearestPoint( CCurve* remote, Vector2D& coord )
{

    const unsigned steps = 2; // FIXME hardcode :(

    // создадим лист
    VectorsList* result_list = new VectorsList;

    // получим набор векторов, составляющих обе кривые
    VectorsList* local_vectors = ConvertToVectors( steps );
    VectorsList* remote_vectors = remote->ConvertToVectors( steps );

    // l - local, r - remote:
    VectorsList::iterator l_prev = local_vectors->begin();
    VectorsList::iterator l_end = local_vectors->end();
    VectorsList::iterator l = l_prev; l++;

    // начальное значение заведомо больше чем результат
    Vector2D nearest_dir = coord;
    Vector2D nearest_dir_coord;

    /*
    Может быть 2 варианта ближайших расстояний:
    1. кривые пересекаются, возможно несколько раз и ближайших расстояний
       будет несколько но все расстояния будут равны нулю
    2. кривые не пересекаются и ближайшее расстояние одно
    */

    bool crossing_flag = false;

    while ( l != l_end ) {

      Vector2D l_rebro = *l - *l_prev;
      Vector2D l_coord = *l_prev;

      // l - local, r - remote:
      VectorsList::iterator r_prev = remote_vectors->begin();
      VectorsList::iterator r_end = remote_vectors->end();
      VectorsList::iterator r = r_prev; r++;

      while ( r != r_end ) {

        // длина дальнего ребра
        Vector2D r_rebro = *r - *r_prev;

        // координата дальнего ребра относительно ближнего
        Vector2D r_coord = *r_prev + coord - l_coord;

        // получим расстояние между 2 ребрами кривых
        Vector2D cur_dir, cur_dir_coord;
        bool crossing = ShortestDirectionToVector( &l_rebro, &r_rebro, &r_coord, &cur_dir, &cur_dir_coord );

        // абсолютная точка, откуда растёт направление
        Vector2D from_point = l_coord + cur_dir_coord;

        // рёбра пересекаются?
        if( crossing ) {
                // кривые пересекаются - меняем стратегию
                crossing_flag = true;
                MSG( "crossing!");
                result_list->push_back( cur_dir ); // направления у пересечения нет
                result_list->push_back( from_point ); // координаты пересечения
        }

        // пересечений (пока) не обнаруживалось
        // ищем кратчайшее из расстояний
        if( !crossing_flag && cur_dir.length() < nearest_dir.length() ) {

              // сохраняем новое ближайшее направление
              nearest_dir = cur_dir;
              // и точку его начала,
              nearest_dir_coord = from_point;
        }

        r++;
      }

      l++;
    }

    // у нас небыло точек пересечения,
    // значит результирующая точка одна, её единственную и возвратит функция
    if( !crossing_flag ) {
        MSG("one result");
        result_list->push_back( nearest_dir );
        result_list->push_back( nearest_dir_coord );
    }

    delete local_vectors;
    delete remote_vectors;

    return result_list;

}


Vector2D CCurve::NearestTangentBetweenParabolas( CCurve& other_parabola, Vector2D& his_coord )
{
// FIXME неверный алгоритм, кажется
// надо попробовать использовать середины расстояний между фокусами

    // начало нашей параболы относительно начала нашей кривой Безье
    Vector2D s1 = GetParabolaExtremum();

    // удалённой, тоже относительно начала нашей кривой Безье
    Vector2D s2 = his_coord + other_parabola.GetParabolaExtremum();

    // половинка направления от нашего начала параболы к удалённому
    Vector2D o_half = (s2 - s1) * 0.5;

    // любые две точки на параболах
    Vector2D p1 = direction;
    Vector2D p2 = his_coord;

    // половинка расстояния между точками
    Vector2D p_dir_half = (p2 - p1) * 0.5;

    // конец вектора-касательной
    Vector2D res_end = p_dir_half + direction;
    // начало вектора-касательной
    Vector2D res_start = o_half + s1;

    return res_end - res_start;

}


/*!
    \fn CCurve::GetParabolaExtremum()
 */
Vector2D CCurve::GetParabolaExtremum()
{
    return GetParabolaPoint( 0.5 );
}


/*!
    \fn CCurve::GetDirectionToParabola( CCurve& parabola )
 */
Vector2D CCurve::GetDirectionToParabola( CCurve& parabola, Vector2D& coord, Vector2D& from_point )
{
    // ближайшие точки нашей и удалённой параболы
    Scalar t_loc, t_rem;
    GetNearestPointsOfParabolas( parabola, coord, t_loc, t_rem );

    // начало направления на удалённую параболу
    from_point = GetParabolaPoint( t_loc );

    // точка на удалённой параболе
    Vector2D remote_point = parabola.GetParabolaPoint( t_rem ) + coord;

    // направление на удалённую параболу
    return remote_point - from_point;

}


void CCurve::GetNearestPointsOfParabolas( CCurve& parabola, Vector2D& coord, Scalar& t_local, Scalar& t_remote )
{
    // получим ближайшую общую касательную
    Vector2D tangent = NearestTangentBetweenParabolas( parabola, coord );

    // какой точке нашей параболы соответствует эта касательная?
    t_local = GetPointOfOsculation( tangent );

    // удалённая точка
    t_remote = parabola.GetPointOfOsculation( tangent );
}


/**
@param tangent Касательная к параболе
@return Точка t, соответствующая касательной
*/
Scalar CCurve::GetPointOfOsculation( Vector2D& tangent )
{
  // при нулевой длине кривой и нулевой кривизне точка касания равна нулю
  if( direction.IsNull() && curvature.IsNull() ) return 0.0;

  Scalar k;
  if( tangent.val.x ) k = tangent.val.y / tangent.val.x; else k = 0.0; // x==0, поэтому k=0

  // k = y'(t) / x'(t), отсюда t:
  Scalar t = (2.0 * curvature.val.y - 2.0 * curvature.val.x * k) /
              (4.0 * curvature.val.y - 2.0 * direction.val.y - 4.0 * k * curvature.val.x +
               2.0 * direction.val.x * k );
  return t;
}


/*!
    \fn CCurve::GetTangent( Scalar t )
 */
Vector2D CCurve::GetTangent( Scalar t )
{
  Vector2D res;

  // если кривая совсем не кривая, то, избегая возвращения ноля при t=0, вернём просто
  // параллельный вектор
  if( curvature.IsNull() ) {
      res = direction;
  } else {
      // это просто производная формул кривой Безье из CCurve::GetParabolaPoint()
      res.val.x = 2.0 * curvature.val.x - 4.0 * curvature.val.x * t + 2.0 * direction.val.x * t;
      res.val.y = 2.0 * curvature.val.y - 4.0 * curvature.val.y * t + 2.0 * direction.val.y * t;
  }

  assert( !res.IsNull() );
  return res;
}


Vector2D CCurve::CalculateCenterMass()
{
    // середина направления
    Vector2D dir_half = 0.5 * direction;

    // TODO считаем на глазок, без формул, похоже на правду но не доказано :(
    // вернём треть от высоты параболы
    return dir_half + GetParabolaAxis() * (1.0/3.0);
}


/*!
    \fn CCurve::GetParabolaAxis()
 */
Vector2D CCurve::GetParabolaAxis()
{
    return GetParabolaExtremum() - direction * 0.5;
}


/*!
    \fn CCurve::GetDimensionsBox(Vector2D& v1, Vector2D& v2)
 */
void CCurve::GetDimensionsBox(Vector2D& v1, Vector2D& v2)
{
  // для начала закинем начало
  Vector2D start;
  Vector2D::MergeVector ( start, &v1, &v2 );

  // конец
  Vector2D::MergeVector ( direction, &v1, &v2 );

  // половину кривизны
  Vector2D c( curvature * 0.5 );
  Vector2D::MergeVector ( c, &v1, &v2 );

  // и половину другой стороны кривизны
  Vector2D b( (curvature - direction) * 0.5 + direction );
  Vector2D::MergeVector ( b, &v1, &v2 );
}


bool CCurve::IsPointInsideCurve( Vector2D& point )
{
    // направление на точку из вершины curvature
    Vector2D to_point = point - curvature;

    // точка пересечения с direction;
    Vector2D cross = direction.CrossingPoint( &to_point, &curvature );

    // найдём соответствующую этой точке t
    Scalar t = cross / direction;

    // если t вышла за пределы то точка уже точно не попадает
    if( t < 0 || t > 1 ) return false;

    // найдём соответствующую t точку на кривой относительно cross
    Vector2D etalon = GetParabolaPoint( t ) - cross;

    // найдём вектор от cross к изучаемой точке
    Vector2D dir2point = point - cross;

    // если точка лежит на direction то она принадлежит площади кривой
    if( dir2point.IsNull() ) return true;

    // проверим, в одну ли сторону смотрят векторы
    if( !dir2point.SectorCompare( &etalon ) ) return false;

    // проверяем длины векторов, делаем выводы :)
    if( dir2point.length() > etalon.length() ) return false; else return true;
}


/*!
    \fn CCurve::GetCurveLength()
 */
Scalar CCurve::GetCurveLength()
{
    Vector2D control2end = direction - curvature;
    Scalar vec_len = direction.length() + curvature.length() + control2end.length();
    return 0.5 * vec_len;
}



/**
Оригинальная кривая остаётся нетронутой.

@param t Точка, в которой будет разделена кривая
@param cur1 Сюда будет помещена первая по направлению полученная кривая
@param cur2 Сюда будет помещена вторая по направлению полученная кривая
*/
void CCurve::Split( Scalar t, CCurve& cur1, CCurve& cur2 )
{
    assert( t >= 0 );
    assert( t <= 1 );

    // координаты точки разделения
    cur1.direction = GetParabolaPoint( t );

    // касательная в точке разделения
    Vector2D tangent = GetTangent( t );

    // контролька первой кривой
    cur1.curvature = curvature.CrossingPoint( &tangent, &cur1.direction );

    // направление второй кривой
    cur2.direction = direction - cur1.direction;

    // недостающая сторона треугольника
    Vector2D c2 = direction - curvature;

    // вторая контролька
    cur2.curvature = tangent.CrossingPoint( &c2, &cur2.direction );
}


/*!
    \fn CCurve::IsConcave()
 */
bool CCurve::IsConcave()
{
    return direction.Classify( curvature ) == Vector2D::LEFT;
}

/**
 @param axis Необязательный параметр - ось вращения
 @author Александр Оплеснин <gasparfx@gmail.com>
*/
Scalar CCurve::CalcMomentOfInertiaCapacity( Vector2D* axis )
{
	// Если объект слишком мал - не считаем
	Scalar area = fabs( CalculateArea() );
	if ( area < 0.0000001 ){
		return 0;
	}
	// Сюда будем результат запоминать
	Scalar momInerCap = 0;

	// Ось вращения
	Vector2D axisOfRotation;
	// Центр масс фигуры
	Vector2D centrMass = CCurve::CalculateCenterMass();

	// Если ось не была задана - берем центр масс
	if ( !axis ){
		axisOfRotation = centrMass;
	} else {
		axisOfRotation = *axis;
	}
        Scalar dirAngle = atan2(direction.val.y, direction.val.x); ///< Угол между direction и Ox(против часовой)
        Scalar curAngle = atan2(curvature.val.y, curvature.val.x); ///< Угол между curvature и Ox(против часовой)
        Scalar newCurAngle = curAngle - dirAngle;
        Vector2D d(direction.length(), 0);
        Vector2D c(cos( newCurAngle ) * curvature.length(), sin( newCurAngle ) * curvature.length() );

        Scalar Cx = c.val.x;
        Scalar Cy = c.val.y;
        Scalar Dx = d.val.x;

		// Емкость момента инерции относительно начала
		momInerCap = Cy*Dx*(4*Cx*Cx+10*Dx*Cx+4*Cy*Cy+15*Dx*Dx)/210.0; /// TODO Потестить формулу

		// Емкость момента инерции относительно центра масс
		momInerCap = fabs( momInerCap ) - ( area * centrMass.length() * centrMass.length() );


		momInerCap = momInerCap + ( area * ( centrMass - axisOfRotation ).length()
										 * ( centrMass - axisOfRotation ).length() );

		return momInerCap;
}
