#ifndef CCURVEPOLYGON_H
#define CCURVEPOLYGON_H

#include "cpolygon.h"
#include "ccurve.h"


/**
  @brief "Кривоугольник"

  Замкнутая геометрическая фигура, составленная из квадратных кривых Безье
	
  @author Феклушкин Денис <edu2005-60@mail.ru>
*/
class CCurvePolygon : public CPolygon
{
protected:
    ItersList* controls_list;
    void NewControls( ItersList* controls );

public:
    struct iter_edge;

    CCurvePolygon();
    CCurvePolygon( const CCurvePolygon& src );
    ~CCurvePolygon();

    bool IsPointInside( Vector2D& point );
    Scalar GetArea() const; // площадь
	Vector2D GetCenterMass() const; // направление на центр масс
    CCurve EdgeGet( const iter_edge& i ) const; // взять сторону
    void EdgeRemove( iter_edge& i ); // удалить сторону
    void EdgeInsert( iter_edge& i, CCurve& curve ); // добавить сторону
    void EdgeSplit( iter_edge& i, Scalar& t ); // разделить сторону на две

	/// Емкость момента инерции относительно заданной оси
	Scalar CalcMomentOfInertiaCapacity( Vector2D* axis = 0 );

};


// итератор для прыжков по сторонам кривоугольника
struct CCurvePolygon::iter_edge : ItersList::iterator
{
  ItersList* corners_list;
  ItersList* controls_list;
  ItersList::iterator control;

  iter_edge( ItersList* corners, ItersList* controls )
    : ItersList::iterator( corners->begin() )
  {
    assert( corners );
    assert( controls );

    corners_list = corners;
	controls_list = controls;
    control = controls->begin();
  }

  iter_edge( const iter_edge& src )
    : ItersList::iterator( src )
  {
    corners_list = src.corners_list;
	controls_list = src.controls_list;
    control = src.control;
  }

  iter_edge& operator=( const iter_edge& src )
  {
    if( this == &src ) return *this; // присваивание самому себе
    ItersList::iterator* this_it = this;
    *this_it = src;
    corners_list = src.corners_list;
	controls_list = src.controls_list;
    return *this;
  }

  iter_edge& operator=( const ItersList::iterator& src )
  {
    ItersList::iterator* this_it = this;

    if( this_it == &src ) return *this; // присваивание самому себе
    *this_it = src;
    return *this;
  }

  iter_edge& operator++()
  {
    ItersList::iterator tmp = *this;
	if ( tmp == corners_list->end() ) 
	{
		tmp = corners_list->begin();
		control = controls_list->begin();
	}
    ++tmp;
    *this = tmp;
	++control;
    return *this;
  }

  iter_edge operator++(int)
  {
    iter_edge tmp = *this;
    ++(*this);
    return tmp;
  }

  iter_edge& operator--()
  {
    ItersList::iterator tmp = *this;
	if ( tmp == corners_list->begin() ) 
	{
		tmp = corners_list->end();
		control = controls_list->end();
	}
    --tmp;
    *this = tmp;
	--control;
    return *this;
  }

  iter_edge operator--(int) { assert( false ); return *this; }
};


#endif
