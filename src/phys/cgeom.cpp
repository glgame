#include "cgeom.h"

CGeom::CGeom()
{
}


CGeom::~CGeom()
{
}


void CGeom::DisplayTriangle( Triangle& t )
{
    glBegin( GL_LINE_LOOP ); // замкнутая ломанная линия
        glVertex( *t.c1 );
        glVertex( *t.c2 );
        glVertex( *t.c3 );
    glEnd();
}


/**
Разделение списка точек на два списка, для использования в "кривоугольнике" Безье.

На входе функция получает список с чётным количеством точек, который будет разделён так:
- нечётные элементы попадут в список res1, пригодный для создания многоугольника
- чётные элементы попадут в список res2, пригодный для задания контрольных точек
  "кривоугольника" Безье.

Внимание: Результирующие списки дополняются, не перезаписываются!

@param from Указатель на исходный список точек
@param res1 Результирующий список нечётных точек
@param res2 Результирующий список чётных точек
*/
void CGeom::SplitInto2Lists( VectorsList* from, VectorsList& res1, VectorsList& res2 )
{
  assert( from );
  assert( from->size() >= 2 );
  assert( from->size() % 2 == 0 ); // проверка на чётность
 
  res1.clear();
  res2.clear();

  VectorsList::iterator i = from->begin();
  VectorsList::iterator end = from->end();

  while ( i != end ) {
    res1.push_back( *i );
    ++i;
    res2.push_back( *i );
    ++i;
  }

}


/**
Быстрый поиск повёрнутых вокруг общего центра списка точек.
Быстрота достигается изменением существующих векторов, переданных в
результирующем списке, а не созданием новых.

@param from Исходный список векторов
@param rotated Ссылка на результирующий список точек (после выполнения
функции он будет изменён и дополнен)
@param center Центр вращения
@param angle Угол поворота
*/
void CGeom::RotatedToAngle( VectorsList& from, VectorsList& rotated, Vector2D& center, Scalar angle )
{
    VectorsList::iterator i = from.begin();
    VectorsList::iterator end = from.end();
    VectorsList::iterator r = rotated.begin();
    VectorsList::iterator r_end = rotated.end();

    while ( i != end && r != r_end ) {
        *r = RotatedToAngle( *i, center, angle );
        ++i;
        ++r;
    }

    // если кончился наш список то обрезаем результирующий и всё
    if( i == end ) {
        rotated.erase( r, r_end );
    } else {

      // кончился удалённый список - добиваем его нашим остатком
      while( i != end ) {
          rotated.push_back( RotatedToAngle( *i, center, angle ) );
          ++i;
      }
    }
}


/**
  Поворот точки на заданный угол относительно заданной точки

  @param src Исходная точка
  @param center Центр вращения
  @param angle Угол поворота
  @return Созданная повёрнутая точка
*/
Vector2D CGeom::RotatedToAngle( Vector2D& src, Vector2D& center, const Scalar angle )
{
  // точка относительно центра
  Vector2D from_center = src - center;

  // повернём её на угол
  from_center.Rotate( angle );

  // возвращаем её относительно начала
  return center + from_center;
}



/**
Создаёт новый список итераторов ItersList, указывающих соответственно на
каждый итератор из списка VectorsList

@param vectors Исходный список векторов
@return Указатель на созданный список итераторов 
*/
ItersList* CGeom::GetItersList( VectorsList* vectors )
{
    assert( vectors );

    ItersList* iters = new ItersList;
    VectorsList::iterator i = vectors->begin();
    VectorsList::iterator end = vectors->end();

    while ( i != end ) {
      iters->push_back( i );
      i++;
    }

    return iters;
}


/**
Создаёт новый список векторов VectorsList, идентичный списку, на который
соответственно указывают итераторы исходного списка ItersList

@param iterators Исходный список итераторов
@return Указатель на созданный список векторов
*/
VectorsList* CGeom::GetVectorsList( ItersList* iterators )
{
    assert( iterators );

    VectorsList* vecs = new VectorsList;
    ItersList::iterator i = iterators->begin();
    ItersList::iterator end = iterators->end();

    while ( i != end ) {
      vecs->push_back( **i );
      i++;
    }

    return vecs;
}
