#ifndef OBJ_CURVEBODY_H
#define OBJ_CURVEBODY_H

#include "ccurvepolygon.h"
#include "obj_body.h"
#include "../input/eventmouseclick.h"


/**
  @brief Тело в виде "кривоугольника" Безье

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class obj_CurveBody : public CCurvePolygon, public obj_Body, public EventMouseClick
{
    VectorsList* original_corners;
    VectorsList* original_controls;

    VectorsList corners_vectors;
    VectorsList controls_vectors;

    ItersList sharp_corners;

public:
    obj_CurveBody();
    ~obj_CurveBody();

    bool DisplayCorners;
    bool DisplayCurvesDots;

    void NewPoints( VectorsList* corners, VectorsList* controls );

    void display();
    void getDimensionsBox(Vector2D* v1, Vector2D* v2);
    void physics_forces_calculate();

    // ближайшая к точке точка на оболочке тела
    Vector2D NearestPointOfNutshell( Vector2D* point, bool* is_inside );

    // ближайшая к вектору точка границы тела
    Vector2D NearestPointFromVector( Vector2D* vector, Vector2D* body_coord, Vector2D* res_from_point );

    // ближайшая к телу точка на удалённом теле
    Vector2D NearestDirectToBody( obj_Body* body, Vector2D* from_point );
    void HandlePenetrations( obj_CurveBody& remote );

    // возвращает направление к кривой, только для НЕ острых фигур!
    Vector2D NearestDirectToParabola( CCurve& parabola, Vector2D& parabola_coord, Vector2D& nearest_coord );

    // удалить кривую вместе с углом
    void RemoveCurve( iter_edge i );

    bool IsSharpCorner( iter_edge i );
    Scalar getMass() const { return mass; }
    Vector2D getCenterMass() const { return center_mass; }
    Scalar getMomentOfInertia() const { return moment_inertia; }

private:
    void FillSharpCorners();

protected:
    void Init();
    void MouseClick();
    Scalar CalculateMass() const; /// Подсчитать текущую массу тела
    Vector2D CalculateCenterMass();
    Scalar CalculateMomentOfInertia(Vector2D* axis = 0);
    void CalculateRotatedCorners();
};

#endif
