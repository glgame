#ifndef OBJ_FROMLIST_H
#define OBJ_FROMLIST_H

#include "../displayed_obj.h"
#include "physics_obj.h"
#include <cmath>
#include "../math/fast_math.h"


/**
Класс показывает объект из GL List

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class obj_FromList : public displayed_obj , physics_obj{
    GLuint object;
    void display();
    void calculate();

    GLfloat s_x, s_y;

public:
    obj_FromList(GLuint object, Coord x, Coord y, Angle rotate, Coord size);
    ~obj_FromList();

    Scalar rotation;
};

#endif
