#include "busiki.h"

obj_Busiki::obj_Busiki (
            force_point_obj* start, force_point_obj* end,
            unsigned num, // количество бусинок
            unsigned rope_num_segments, // количество сегментов в веревке
            Scalar rope_len_segments, // длина сегментов в веревке
            Scalar radius,
            Scalar hinges_radius
            )
{

Vector2D half_step = (end->getCoord() - start->getCoord()) / (num * 2);
Vector2D hinges_otstup = half_step.ort() * hinges_radius;

force_point_obj* hinge_left;
force_point_obj* hinge_right = 0;

force_point_obj* curr = start;

physics_obj* save;

for (unsigned i = 0; i < num; i++) {

    Vector2D center = curr->getCoord() + half_step;

    obj_Disk* disk = new obj_Disk ( &center, radius );
    disk->setParent( this );
    disk->Init();

    hinge_left  = disk->addBodyHingeFromCenter( -hinges_otstup );
    hinge_right = disk->addBodyHingeFromCenter(  hinges_otstup );

    save = new obj_Rope(curr, hinge_left, rope_num_segments, rope_len_segments);
    save->setParent(this);
    save->Init();

    curr = hinge_right;

}

save = new obj_Rope(hinge_right, end, rope_num_segments, rope_len_segments);
save->setParent(this);
save->Init();

}


obj_Busiki::~obj_Busiki ()
{
}



void obj_Busiki::getDimensionsBox(Vector2D *const v1, Vector2D *const v2){

  assert ( v1->length() == 0 );
  assert ( v2->length() == 0 );

}


Vector2D obj_Busiki::getCoords(){

    return Vector2D(0,0);

}


/*!
    \fn obj_Busiki::display()
 */
void obj_Busiki::display()
{
}


void obj_Busiki::physics_forces_apply(){};
void obj_Busiki::physics_forces_reset(){};
void obj_Busiki::physics_forces_calculate(){};
