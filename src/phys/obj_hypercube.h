#ifndef OBJ_HYPERCUBE_H
#define OBJ_HYPERCUBE_H

#include "../displayed_obj.h"

/**
	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class obj_hypercube : public displayed_obj {

    static GLuint hypercube_id;

public:

    obj_hypercube();

    ~obj_hypercube();
    void display();

};

#endif
