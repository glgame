#ifndef CCURVE_H
#define CCURVE_H

#include "../math/vector2d.h"
#include "../gl_functions.h"
#include <list>
#include "geometry.h"
#include "cgeom.h"


/**
@brief Квадратная кривая Безье
@author Феклушкин Денис <edu2005-60@mail.ru>

Квадратная кривая Безье является отрезком обыкновенной квадратной параболы (наклонной).
Задаётся квадратная кривая Безье двумя параметрами: направлением и контрольной точкой.
Внутри класса парабола задана параметрически: с изменением t изменяются x и y.
Кривой Безье при этом принадлежит отрезок этой параболы t [0,1]

Ненужные подробности:
  Любая точка Pt на кривой Безье второго порядка вычисляется по формуле (1):
  Pt = S*(1-t)^2 + 2*C*(1-t)*t + E*t^2
  где:
  t (time) — time-итератор точки;
  S (start) — начальная опорная (узловая) точка (t=0) (anchor point);
  С (control) — управляющая точка (direction point);
  E (end) — конечная опорная (узловая) точка (t=1) (anchor point).

*/

class CCurve : public CGeom
{

    Color color; ///< Цвет кривой ;)

public:
    Vector2D direction; ///< Направление кривой (direction vector)
    Vector2D curvature; ///< Кривизна кривой (control vector)

    CCurve();
    CCurve( const CCurve& src );
    CCurve( const Vector2D& direction );
    CCurve( const Vector2D& direction, const Vector2D& curvature );

    ~CCurve();

    /// Подсчёт площади фигуры, ограниченной данной кривой
    Scalar CalculateArea();

	/// Подсчитать емкость момента инерции ( чтобы получить момент нужно домножить на плотность )
	Scalar CalcMomentOfInertiaCapacity( Vector2D* axis = 0 );

    /// Отобразить кривую средствами OpenGL
    void display() const;

    /// Аппорксимирует кривую в набор точек
    VectorsList* ConvertToVectors( unsigned num_steps ) const;

    /// Фраг включения отображения начальной и конечной точек кривой
    bool DisplayDots;

    /// Устанавливает цвет отображения данной кривой
    inline void setColor (const Color color) { CCurve::color = color; }

    /// Найти точку, принадлежащую кривой, при заданном t
    Vector2D GetParabolaPoint( Scalar t ) const;

    /// FIXME Удалить нахрен глюкало
    VectorsList* GetDirectionToCurveNearestPoint( CCurve* remote, Vector2D& coord );

    /// Поиск ближайшей касательная к двум параболам (кривоват и не нужен)
    Vector2D NearestTangentBetweenParabolas( CCurve& other_parabola, Vector2D& his_coord );

    /// Направление на точку экстремума параболы
    Vector2D GetParabolaExtremum();

    /// Поиск ближайших точек двух парабол. TODO:Сомневаюсь что это нужно.
    void GetNearestPointsOfParabolas( CCurve& parabola, Vector2D& coord, Scalar& t_local, Scalar& t_remote );

    /// Найти ближайшее направление от одной параболы до другой. TODO:Сомневаюсь что это нужно.
    Vector2D GetDirectionToParabola( CCurve& parabola, Vector2D& coord, Vector2D& from_point );

    /// Найти точку касания, соответствующую заданной касательной к параболе
    Scalar GetPointOfOsculation( Vector2D& tangent );

    /// Найти касательную к параболе в заданной точке t
    Vector2D GetTangent( Scalar t );

    /// Найти центр масс фигуры, образованной кривой
    Vector2D CalculateCenterMass();

    /// Найти ось параболы
    Vector2D GetParabolaAxis();

    /// Найти "габаритный ящик" кривой
    void GetDimensionsBox(Vector2D& v1, Vector2D& v2);

    /// Вернуть контрольную точку
    inline Vector2D GetCurvature() const { return curvature; }

    /// Вернуть конец кривой (направление)
    inline Vector2D GetDirection() const { return direction; }

    /// Попадает ли точка в кривую?
    bool IsPointInsideCurve( Vector2D& point );

    /// Найти длину кривой
    Scalar GetCurveLength();

    /// Деление кривой на две в заданной точке
    void Split( Scalar t, CCurve& cur1, CCurve& cur2 );

    /// Вогнутая ли кривая?
    bool IsConcave();

protected:
    void Init(); ///< Для внутреннего использования
};

#endif
