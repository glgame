#include "cconvexpolygon.h"


CConvexPolygon::CConvexPolygon()
{
}

CConvexPolygon::CConvexPolygon( ItersList* corners )
{
  assert( corners );
  NewCorners( corners );
}


CConvexPolygon::~CConvexPolygon()
{
  delete triangulation;
}


CConvexPolygon::CConvexPolygon( const CConvexPolygon& src )
  : CBasePolygon( src )
{
    triangulation = new TrianglesList;
    *triangulation = *src.triangulation;
}


/**
Данная функция триангуляции перегружает имеющуюся функцию для увеличения
производительности, т.к. триангуляция выпуклого прямоугольника задача
простая, выполняющаяся быстро с линейной скоростью

@return Список треугольников
*/
CGeom::TrianglesList* CConvexPolygon::GetTriangulation()
{
  assert( corners_list );

  TrianglesList* res = new TrianglesList;

  ItersList::iterator first = corners_list->begin();
  ItersList::iterator end = corners_list->end();
  ItersList::iterator curr = first; ++curr;
  ItersList::iterator prev = curr++;

  Triangle t;

  while ( curr != end ) {

    // заполяем список углов треугольника
    t.c1 = *first;
    t.c2 = *prev;
    t.c3 = *curr;

    // добавляем этот треугольник в результирующий список
    res->push_back( t );

    prev = curr;
    ++curr;
  }

  return res;
}


/**
Показать треугольники, составляющие многоугольник тонкими линиями в OpenGL
*/
void CConvexPolygon::display_triangles()
{
    assert( corners_list );

    // покажем триангуляцию тонкими линиями
    glLineWidth ( 1.0 );
    TrianglesList::iterator j = triangulation->begin();
    TrianglesList::iterator jend = triangulation->end();

    while ( j != jend ) {
      DisplayTriangle( *j );
      j++;
    }
}


/*!
    перегружаем NewCorners, потому что после добавления углов требуется триангуляция
*/
void CConvexPolygon::NewCorners( ItersList* corners )
{
    assert( corners );

    CBasePolygon::NewCorners( corners );
    assert( IsConvex() );
    triangulation = GetTriangulation();
}
