#ifndef OBJ_SEGMENTROPE_H
#define OBJ_SEGMENTROPE_H

#include "force_point_obj.h"
#include "segment_obj.h"

/**
попробуем сделать настоящую мягкую верёвку

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class obj_SegmentRope : public segment_obj
{
public:
    obj_SegmentRope(force_point_obj* first_hinge, force_point_obj* last_hinge);

    ~obj_SegmentRope();
    void physics_forces_calculate();

};


#endif
