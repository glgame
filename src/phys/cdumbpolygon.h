#ifndef CDUMBPOLYGON_H
#define CDUMBPOLYGON_H

#include "cgeom.h"
#include "../gl_functions.h"
#include "../tmy_list.h"


/**
  @brief "Тупой многоугольник"
  
  Основная особенность: не требует инициализации, сразу после добавления
  или изменения списка точек можно вызывать функции данного класса.

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class CDumbPolygon : public CGeom
{
protected:
    /// Классификация положения точки относительно многоугольника
    enum PointPlaceEnum {
        INSIDE,   ///< Внутри
        OUTSIDE,  ///< Снаружи
        BORDER    ///< На границе
    };

public:
    ItersList* corners_list; ///< Список углов многоугольника
    virtual void NewCorners( ItersList* corners ); ///< Задать новый список углов

public:
    typedef ItersList::iterator Edge; ///< Итератор ребра многоугольника
    void Rotate( Scalar angle ); ///< Повернуть углы многоугольника относительно его центра

    Vector2D EdgeGet( Edge i ) const; ///< Вернуть ребро многоугольника
    virtual void display() const; ///< Отобразить средствами OpenGL
    virtual Vector2D GetCenterMass() const; ///< Найти направление на центр масс многоугольника
    virtual Scalar GetArea() const; ///< Подсчёт ориентированной площади многоугольника
	
    bool IsCW() const; ///< Полигон закручен по часовой стрелке?
    bool IsConvex() const; ///< Полигон выпуклый?
    bool IsIntersected() const; ///< Полигон самопересекается?
    bool IsCornerConvex( ItersList::iterator i ) const; ///< Выпуклый угол?
    bool WithoutZeroEdges() const; ///< Прямоугольник не содержит сторон нулевой длины?

	TrianglesList* triangulate(); ///< Разбить полигон на треугольники

    CDumbPolygon();
    CDumbPolygon( ItersList* corners );
    CDumbPolygon( const CDumbPolygon& src );
    ~CDumbPolygon();

    /// Параллельное смещение всех углов многоугольника
    static void ShiftCorners( VectorsList* list, Vector2D& direction );

    /// Попадает ли точка внутрь многоугольника?
    /// Точка на границе считается внутренней
    bool IsPointInside( Vector2D& point );

    /// Классифицирует положение точки относительно многоугольника
    PointPlaceEnum PointPlace( Vector2D& point );

private:
    /// Классификация положения ребра относительно прямой
    enum EdgePlace {
      TOUCHING,   ///< Касательное
      CROSSING,   ///< Пересекающее
      INESSENTIAL ///< Несущественное
    };
    
    /// Классифицирует положения ребра относительно прямой
    EdgePlace EdgeType( Vector2D& point, Edge i );
};

#endif
