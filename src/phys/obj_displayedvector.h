#ifndef OBJ_DISPLAYEDVECTOR_H
#define OBJ_DISPLAYEDVECTOR_H

#include <iostream>
#include "../displayed_obj.h"
#include "../math/vector2d.h"
#include "physics_obj.h"

/**
Показываемый на экране вектор. Для целей дебага.

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class obj_DisplayedVector : public displayed_obj
{
    Vector2D* start_point;
    Vector2D* value;
public:
    obj_DisplayedVector( Vector2D* start_point, Vector2D* value );
    ~obj_DisplayedVector();
    void display();

    bool show_debug_to_console;
    bool show_value;
    bool show_axis;
//    Scalar magnify;

};

#endif
