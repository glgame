#include "cdumbpolygon.h"


CDumbPolygon::CDumbPolygon()
{
  corners_list = 0;
}


CDumbPolygon::CDumbPolygon( const CDumbPolygon& src )
{
  if( this == &src ) return;

  corners_list = new ItersList;
  *corners_list = *src.corners_list;
}


/**
Этот конструктор не копирует список углов!
Он единолично использует переданный через указатель список.

@param src Список углов
*/
CDumbPolygon::CDumbPolygon( ItersList* corners )
{
  assert( corners );
  assert( corners->size() >= 2 ); // минимально возможно 1 ребро, 2 вершины

  corners_list = 0;
  NewCorners( corners );
}


CDumbPolygon::~CDumbPolygon()
{
  delete corners_list;
}


bool CDumbPolygon::IsCornerConvex( ItersList::iterator i ) const
{
    assert( corners_list );
    assert( i != corners_list->end() );
    assert( corners_list->size() >= 2 );

    ItersList::loop_iterator prev( *corners_list, i );
    ItersList::loop_iterator next = prev;
    ++next;
    --prev;

    Vector2D a = **next - **i;
    Vector2D b = **i - **prev;

    if( a.IsCollinear( &b ) ) return true;
    if( Vector2D::ProductVector( a, b ) >= 0 ) return false;
       else return true;
}

/**
Находит площадь несамопересекающегося многоугольника, причём знак результата зависит от того, в какую сторону закручен многоугольник: если влево то знак положительный, вправо - отрицательный.
*/
Scalar CDumbPolygon::GetArea() const
{
  assert( corners_list );
  assert( corners_list->size() >= 2 );

  Scalar res = 0.0;


  ItersList::iterator end = corners_list->end();
  ItersList::iterator i = corners_list->begin();
  ItersList::iterator prev = i++;

  while ( i != end ) {
	res += ( (*i)->val.y * (*prev)->val.x ) - ( (*i)->val.x * (*prev)->val.y );
    prev = i;
    ++i;
  }

  return 0.5 * res;
}


void CDumbPolygon::Rotate( Scalar angle )
{
    assert( corners_list );

    ItersList::iterator i = corners_list->begin();
    ItersList::iterator end = corners_list->end();

    while ( i != end ) {
        (*i)->Rotate( angle );
        i++;
    }
}


void CDumbPolygon::display() const
{
    assert( corners_list );

    // display body
    ItersList::iterator i = corners_list->begin();
    ItersList::iterator end = corners_list->end();

    glBegin( GL_LINE_LOOP ); // замкнутая ломанная линия

    while ( i != end ) {
        glVertex( **i ); // iter->iter->vector :)
        i++;
    }

    glEnd();
}


/**
Задаёт новый список углов. Переданный список начинает использоваться единолично
данным многоугольником.

Старый список углов уничтожается.

@param corners Указатель на список углов
*/
void CDumbPolygon::NewCorners( ItersList* corners )
{
    assert( corners );
    assert( corners->size() >= 2 ); // минимально возможно 1 ребро, 2 вершины

    delete corners_list;
    corners_list = corners;
}


/**
Проверка на закрученность полигона по часовой стрелке

Алгоритм:
Найдем нижнюю вершину (если их несколько, то самую правую), а затем возьмем векторное произведение сторон после и перед ней. Причина, по которой мы берем именно такую точку - в том, что внутренний угол при такой вершине обязательно будет выпуклым и строго меньше пи.
*/
bool CDumbPolygon::IsCW() const
{
    assert( corners_list );
    assert( corners_list->size() >= 2 ); // минимально возможно 1 ребро, 2 вершины
    assert( WithoutZeroEdges() ); // без рёбер нулевой длины

    // будем считать полигон из 1 ребра закрученным против часовой стрелки
    if( corners_list->size() == 2 ) return false;

    ItersList::iterator begin = corners_list->begin();
    ItersList::iterator end = corners_list->end();
    ItersList::iterator i = begin;

    ItersList::iterator b1 = i;

    // найдём самую нижнюю точку правую
    while ( i != end ) {
        if( ( (*i)->val.y < (*b1)->val.y ) ||
            // если обе точки одинаково внизу - берём правую точку
            ( ( (*i)->val.y == (*b1)->val.y ) && ( (*i)->val.x > (*b1)->val.x ) ) )
                b1 = i;

        i++;
    }

    // проверим векторное произведение рёбер этой точки
    // следующая точка:
    ItersList::iterator b2 = b1; b2++; if( b2 == end ) b2++;
    // предыдущая точка:
    ItersList::iterator b0 = b1; if( b0 == begin ) b0 = end; b0--;

    Vector2D a = **b1 - **b0;
    Vector2D b = **b2 - **b1;

    // векторное произведение даст направление закрутки всего многоугольника
    if( Vector2D::ProductVector( b, a ) > 0 ) return true; else return false;
}


bool CDumbPolygon::IsConvex() const
{
    assert( corners_list );
    assert( corners_list->size() >= 2 ); // минимально возможно 1 ребро, 2 вершины
    assert( WithoutZeroEdges() ); // без рёбер нулевой длины
//    assert( !IsCW() ); // алгоритм сработает только против часовой стрелки

    // будем считать полигон из 1 ребра тоже выпуклым
    if( corners_list->size() == 2 ) return true;

    ItersList::iterator end = corners_list->end();
    ItersList::iterator i = corners_list->begin();

    while ( i != end ) {

        if( !IsCornerConvex( i ) ) return false;

        i++;
    }

    return true;
}


/**
Тупо проверяет пересечение рёбер каждого с каждым.
Медленный алгоритм.
*/
bool CDumbPolygon::IsIntersected() const
{
    assert( corners_list );
    assert( corners_list->size() >= 2 ); // минимально возможно 1 ребро, 2 вершины
    assert( WithoutZeroEdges() ); // без рёбер нулевой длины

    if( corners_list->size() == 2 ) return false; // такой случай считаем нормальным

    ItersList::iterator begin = corners_list->begin();
    ItersList::iterator end = corners_list->end();
    ItersList::iterator i, j;

    Vector2D d1, d2, coord, cross;

    // тупо проверяем на пересечение каждое с каждым рёбра
    for( j = begin; j != end; j++ ){
      for( i = begin; i != end; i++ ){

        d1 = EdgeGet( j ); // первое ребро
        d2 = EdgeGet( i ); // второе ребро

        coord = **i - **j; // координаты второго вектора относительно первого
        Vector2D cross_point = d1.CrossingPoint( &d2, &coord );
        Vector2D cross_point2 = cross_point - coord;

        switch( d1.Classify( cross_point ) ) {
          case Vector2D::BETWEEN:
          case Vector2D::END:

            switch( d2.Classify( cross_point2 ) ) {
              case Vector2D::BETWEEN:
              case Vector2D::END:
                return true;
              default:;
            }

          default:;
        }
      }
    }

    return false;
}


Vector2D CDumbPolygon::EdgeGet( ItersList::iterator i ) const
{
    assert( corners_list );
    assert( corners_list->size() >= 2 );
    assert( i != corners_list->end() ); // ненормальная ситуация

    ItersList::loop_iterator next( *corners_list, i );
    ++next;

    return **next - **i;
}


bool CDumbPolygon::WithoutZeroEdges() const
{
    assert( corners_list );

    ItersList::iterator i = corners_list->begin();
    ItersList::iterator end = corners_list->end();

    while ( i != end ) {
        if( EdgeGet( i ).IsNull() ) return false;
        i++;
    }

    return true;
}


Vector2D CDumbPolygon::GetCenterMass() const
{
    assert( corners_list );

    Vector2D res;

    ItersList::iterator i = corners_list->begin();
    ItersList::iterator end = corners_list->end();

    // формула вычисления центра масс:
    //Xc = (X1 + ... + XN)/N Yc = (Y1 + ... + YN)/N
    while ( i != end ) {

        res.val.x += (*i)->val.x;
        res.val.y += (*i)->val.y;
        i++;
    }

    res /= ( corners_list->size() * 1.0 );

    return res;
}


void CDumbPolygon::ShiftCorners( VectorsList* list, Vector2D& direction )
{
    assert( list );

    VectorsList::iterator i = list->begin();
    VectorsList::iterator end = list->end();
    while ( i != end ) {
        *i += direction;
        i++;
    }
}


bool CDumbPolygon::IsPointInside( Vector2D& point )
{
  assert( corners_list );

  switch( PointPlace( point ) ) {
    case BORDER:
    case INSIDE:
      return true;
    default:
      return false;
  }
}


/*
функция классифицирует ребро как TOUCHING, CROSSING или INESSENTIAL
(пересекающее, касательное или безразличное) относительно точки и
направленного из неё направо бесконечного горизонтального вектора
*/
CDumbPolygon::EdgePlace CDumbPolygon::EdgeType( Vector2D& point, Edge i )
{
/*
Обратите внимание, как функция edgeType обнаруживает пересекающие ребра:
Если точка а лежит слева от ребра е, то ребро является пересекающим, только если вершина начала ребра v(=e.org) лежит ниже луча ra, а вершина конца w(=e.dest) расположена на луче или лежит выше его. Тогда ребро не может быть горизонтальным и луч га должен пересекать ребро в некоторой точке, отличающейся от его нижнего конца. Если точка а находится справа от ребра е, то роли конечных точек v и w взаимно меняются.
*/
  Vector2D point_crd = point - **i; // координаты точки относительно начала ребра
  Vector2D edge = EdgeGet( i ); // ребро
  switch ( edge.Classify( point_crd ) ) {
    case Vector2D::LEFT:
      return ( ( point_crd.val.y > 0 ) && ( edge.val.y >= point_crd.val.y ) ) ? CROSSING : INESSENTIAL;
    case Vector2D::RIGHT:
      return ( ( point_crd.val.y < 0 ) && ( edge.val.y <= point_crd.val.y ) ) ? CROSSING : INESSENTIAL;
    case Vector2D::BETWEEN:
    case Vector2D::START:
    case Vector2D::END:
      return TOUCHING;
    default:
      return INESSENTIAL;
  }
}


CDumbPolygon::PointPlaceEnum CDumbPolygon::PointPlace( Vector2D& point )
{
  assert( corners_list );

  bool parity = false;

  ItersList::iterator i = corners_list->begin();
  ItersList::iterator end = corners_list->end();

  while ( i != end ) {
    switch ( EdgeType( point, i ) ) {
      case TOUCHING:
        return BORDER;
      case CROSSING:
        parity = !parity;
      default:;
    }
    i++;
  }
  return (parity ? INSIDE : OUTSIDE);
}

/**
  @brief Триангуляция многоугольника
  @author Александр Оплеснин <gasparfx@gmail.com>
  @return Указатель на список треугольников
 */
CGeom::TrianglesList* CDumbPolygon::triangulate()
{
	/*
	 *Используется алгоритм Разделяй-и-Властвуй
	 */
	TrianglesList* triangles = new TrianglesList;
	if (corners_list->size() < 3) {
		return triangles;
	}
	// Если это уже треугольник...
	if (corners_list->size() == 3) {
		Triangle triangle;

		ItersList::iterator i = corners_list->begin();

		triangle.c1 = *(i++);
		triangle.c2 = *(i++);
		triangle.c3 = *i;
        // ... то список будет из всего одного элемента
		triangles->push_back( triangle );
	} else {
		// Если полигон выпуклый - алгоритм проще
		if ( IsConvex() ) {
			Triangle triangle;

			ItersList::iterator i1 = corners_list->begin();
			ItersList::iterator i2 = i1;
			triangle.c1 = *i1;
			triangle.c2 = *(++i2);
			triangle.c3 = *(++i2);

			while ( 1 ) {
			triangles->push_back( triangle );

			i2++;
			if ( i2 == corners_list->end() ) break;

			triangle.c2 = triangle.c3;
			triangle.c3 = *i2;
			}
		} else {

		// Если вершин много ( > 3), то найдем среди них любую выпуклую
		ItersList::iterator i2 = corners_list->begin();
		ItersList::iterator end = corners_list->end();
		while ( i2 != end && !IsCornerConvex( i2 )){
			i2++;
		}
		assert( i2 != end ); //Выпуклых вершин не было

		// i2 - итератор на эту вершину. i1, i3 - его соседи
		ItersList::iterator i1, i3;
		if ( i2 == corners_list->begin() ) {
			i1 = corners_list->end();
		} else {
			i1 = i2;
		}
		i1--;

		i3 = i2;
		i3++;
		if ( i3 == corners_list->end() ) {
			i3 = corners_list->begin();
		}

		// Составляем из i1,i2,i3 - треугольник(полиугольник на самом деле)
		ItersList* triangleCorners = new ItersList;
		triangleCorners->push_back( *i1 );
		triangleCorners->push_back( *i2 );
		triangleCorners->push_back( *i3 );
		CDumbPolygon polygonTriangle( triangleCorners );

		// Будем искать вторгающиеся точки

		ItersList::iterator j = corners_list->begin();
		ItersList::iterator d = corners_list->end(); // Лучший кандидат на текущий момент (наиближайший к i2)

		double dist = -1.0;	// Расстояние до лучшего кандидата

		// Перебирая все остальные точки найдем вторгающиюся в треугольник
		while ( j != end ){

			if (    ( polygonTriangle.IsPointInside(**j) )
				 && ( j != i1 ) && ( j != i2 ) && ( j != i3 ) ) {
					 if ( ( (**i2) - (**j) ).length() > dist ) {
						 dist = ( (**i2) - (**j) ).length();
						 d = j;
					 }
			}
			j++;
		}

		if ( d == corners_list->end() ){ // Не было вторгающихся вершин
			ItersList* newCornersList = new ItersList;
			newCornersList->assign(corners_list->begin(), corners_list->end());

			// Удаляем из списка i2 - он сейчас отделится в свой треугольник
			newCornersList->remove( *i2 );
			// Отделяем треугольник
			CDumbPolygon newPolygon1( newCornersList );

			ItersList* newCornersList2 = new ItersList;
			newCornersList2->push_back( *i1 );
			newCornersList2->push_back( *i2 );
			newCornersList2->push_back( *i3 );
			CDumbPolygon newPolygon2( newCornersList2 );

			// Запускаем триангуляцию для треугольника и для оставшейся части полигона
			TrianglesList* trianglesPoly1 = newPolygon1.triangulate();
			TrianglesList* trianglesPoly2 = newPolygon2.triangulate();

			// Объединяем результаты триангуляций
			triangles->splice( triangles->end(), *(trianglesPoly1) );
			triangles->splice( triangles->end(), *(trianglesPoly1) );
			delete trianglesPoly1;
			delete trianglesPoly2;
		} else { // d - вторгающаяся вершина
			// Разделим полигон на 2
			ItersList* newCornersList = new ItersList;
			ItersList::iterator c = i2;
			do{
				newCornersList->push_back( *c );
				c++;
				if ( c == corners_list->end() ) {
					c = corners_list->begin();
				}
			} while ( c != d );
			newCornersList->push_back( *c );
			// Это первый
			CDumbPolygon newPolygon1( newCornersList );

			ItersList* newCornersList2 = new ItersList;
			c = d;
			do{
				newCornersList2->push_back( *c );
				c++;
				if ( c == corners_list->end() ) {
					c = corners_list->begin();
				}
			} while ( c != i2 );
			newCornersList2->push_back( *c );
			// Это второй
			CDumbPolygon newPolygon2( newCornersList2 );

			// Запускаем триангуляцию для полигонов
			TrianglesList* trianglesPoly1 = newPolygon1.triangulate();
			TrianglesList* trianglesPoly2 = newPolygon2.triangulate();

			// Объединяем результаты триангуляций
			triangles->splice( triangles->end(), *(trianglesPoly1) );
			triangles->splice( triangles->end(), *(trianglesPoly1) );
			delete trianglesPoly1;
			delete trianglesPoly2;
		}
	}
	}
	return triangles;
}
