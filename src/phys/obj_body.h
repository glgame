#ifndef OBJ_BODY_H
#define OBJ_BODY_H

#include "physics_obj.h"
#include "obj_bodyhinge.h"
#include "../eventmousepick.h"

/**
  @brief Базовый класс для всех объектов, имеющих объем (площадь)

  Тело заполненно однородным материалом.

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class obj_Body : public physics_obj, public EventMousePick
{

    bool MousePicked; ///< TODO:Объект взят мышкой

    Scalar density; ///< Плотность вещества тела
    Scalar moment; ///< Момент сил

protected:
    bool inited; ///< Флаг инициализированности тела

    Scalar moment_inertia; ///< Момент инерции

    /// Направление на центр масс от нулевой точки тела
    Vector2D center_mass;

    /// Импульс тела (относительно центра масс)
    Vector2D center_impulse;

    /// Скорость перемещения центра масс тела
    Vector2D center_velocity;

    Scalar   mass; ///< Масса
    Angle    angle_speed; ///< Угловая скорость
    Angle    angle; ///< Текущий угол поворота тела

    /// Переместить шарнир согласно текущему положению тела
    void hinge_move(obj_BodyHinge* hinge);

    /// Подсчёт влияния на тело данного шарнира
    void hinge_calculate(obj_BodyHinge* hinge);

    /// Переместить шарниры тела согласно текущему положению тела
    void hinges_move_all();

    /// Подсчёт влияния на тело сил, приложенных к его шарнирам
    void hinges_calculate_all();

    obj_BodyHinge::HingesList hinges_list; ///< Список шарниров данного тела

public:
    obj_Body();
    ~obj_Body();

    void physics_forces_reset(); ///< Сброс сил
    void physics_forces_apply(); ///< Применение сил

    /// Добавить шарнир на тело (координаты шарнира относительно тела)
    force_point_obj* addBodyHingeFromCenter(const Vector2D& coords);

    /// Удалить шарнир
    void removeBodyHinge(force_point_obj* hinge);

    /// Применить момент
    inline void apply_moment (const Scalar apply_moment) { assert(inited); moment += apply_moment; };

    /// Объект хотят схватить мышкой?
    bool MousePickUp(Vector2D pick_world_coords);

    /// Объект бросили мышкой
    void MousePickDown(Vector2D pick_world_coords);

    /// Центр масс тела в абсолютных координатах
    inline Vector2D getCenter() { assert(inited); return getCoord() + getCenterMass(); }

    /// Плотность тела
    inline Scalar getDensity() const { return density; }

    /// Установить плотность данного тела
    inline void setDensity (const Scalar new_density) { density = new_density; }

    virtual Scalar getMass() const = 0; ///< Вернуть массу тела
    virtual Vector2D getCenterMass() const = 0; ///< Вернуть центр масс
    virtual Scalar getMomentOfInertia() const = 0; /// Центральный момент инерции

    void Init();

    /// Применяет импульс к телу в произвольной его точке (относительно центра)
    void ApplyImpulse( Vector2D* impulse, Vector2D* coord );

    /// Применяет силу к телу в произвольной его точке (относительно центра)
    void ApplyForce( Vector2D* force, Vector2D* coord );

    /// Применяет силу в точке центра масс
    void ApplyForce( Vector2D* force );

    /// Применяет силу в точке центра масс
    void ApplyForce( Vector2D force );

    /// Указанное тело близко? Быстрая проверка
    bool BodyNear( obj_Body* other_body );

private:
    /// TODO: Определение столкновения с указанным объектом
    bool CollisionDetect( obj_Body* other );
};

#endif
