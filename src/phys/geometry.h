#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "../math/vector2d.h"
#include <cmath>
#include <list>
#include "cgeom.h"

/**
    @brief Старая геометрическая библиотека

    Не используйте эту библиотеку, она будет удалена!
    Используйте CGeom.

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/


// return square by triangle's coords
Scalar getTriangleArea( Vector2D* c1, Vector2D* c2, Vector2D* c3 );

// return square by 2 vectors
Scalar getTriangleArea( Vector2D* a, Vector2D* b );

// return vector to triangle center mass
Vector2D getTriangleCenterMass( Vector2D* c1, Vector2D* c2, Vector2D* c3 );

// mediana
Vector2D getTriangleMediana( Vector2D* a, Vector2D* b );

// return vector to triangle center mass
Vector2D getTriangleCenterMass( Vector2D* a, Vector2D* b );

// return main moment of inertia
Scalar getTriangleMomentOfInertia( Vector2D* p1, Vector2D* p2, Vector2D* p3, Scalar density );

// находит момент инерции, вычесленный относительно угла
Scalar getTriangleMomentOfInertiaFromCorner( Vector2D* a, Vector2D* b, Scalar mass );

typedef std::list<Vector2D> CornersList;

// return main moment of inertia
Scalar getPolygoneMomentOfInertia( CornersList* corners_list, Scalar density );

//return square of poligone
Scalar getPolygoneArea( CornersList* corners );

//return oriented square of poligone
Scalar getPolygoneAreaOriented( CornersList* corners );

// return vector to polygone center mass
Vector2D getPolygoneCenterMass( CornersList* corners_list );

typedef struct {
  Vector2D c1;
  Vector2D c2;
  Vector2D c3;

  void display() {
    glBegin( GL_LINE_LOOP ); // замкнутая ломанная линия
    glVertex2f( c1.val.x, c1.val.y );
    glVertex2f( c2.val.x, c2.val.y );
    glVertex2f( c3.val.x, c3.val.y );
    glEnd();
  }
} Triangle;

typedef std::list<Triangle> TrianglesList;

TrianglesList* getPolygoneTriangulation( CornersList* corners_list );

// возвращает истину если точка лежит справа от вектора
bool IsPointOnRight( Vector2D& vector, Vector2D& point );

// возвращает истину если точка лежит слева от вектора или на векторе
bool IsPointOnLeft( Vector2D& vector, Vector2D& point );

// кратчайшее расстояние от вектора до вектора
bool ShortestDirectionToVector( Vector2D* v1, Vector2D* v2, Vector2D* v2_coord, Vector2D* direction, Vector2D* direction_coord );

// кратчайшее расстояние до одного из концов вектора (вспомогательная функция)
void ShortestDirectionToVectorEnds(Vector2D* v1, Vector2D* v2, Vector2D* v2_coords, Vector2D* direction, Vector2D* v1_direction_coords);

// удвоить количество точек в полигоне (добавить их по центру каждого ребра)
void DoubleCorners( CornersList& list );

// делает звёзду из кривых
VectorsList* GetStarPolygone( unsigned num_axis, const Vector2D& main_axis, const Vector2D& middle_axis );

// если точка совпадает с вершиной то она не внутри! <- FIXME this!
bool IsPointInTriangle( Vector2D& point, Vector2D& a, Vector2D& b );

bool IsPointInTriangle( Vector2D& point, Vector2D& p1, Vector2D& p2, Vector2D& p3 );

#endif
