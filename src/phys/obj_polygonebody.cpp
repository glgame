#include "obj_polygonebody.h"
/*
obj_PolygoneBody::obj_PolygoneBody()
{

    rotated_polygon = 0;
    mass = 0.0;
    setDensity( 0.1f ); // FIXME вынести отседа это!
    visible = true;

}


obj_PolygoneBody::~obj_PolygoneBody()
{
    delete rotated_polygon;
}


void obj_PolygoneBody::Init()
{
    CPolygon::Init();

    // скопируем углы
    Vector2D zero;
    RotatedToAngle( *corners_vectors, rotated_corners, zero, angle );

    // скармливаем углы повёрнутому многоугольнику
    rotated_polygon = new CDumbPolygon( &rotated_corners );

    obj_Body::Init();
}


void obj_PolygoneBody::AddCornersList( VectorsList* corners_list )
{
    assert( corners_list );

    // сохраняем список
    NewCorners( corners_list );

    // создаём список повернутых углов
    CalculateRotatedCorners();

    // триангулируем фигуру
//    CalculateTriangulation();

}


void obj_PolygoneBody::display()
{

    glColor( ColorCurve );
    glLineWidth ( 2.0 );

    // display center of body
    const float size = 0.08;
    DisplayCross( size );

    // display center mass
    const float size2 = 0.15;
    DisplayCross( center_mass, size2 );

    // display body
    glColor( PolygoneBody );
    rotated_polygon->display();

}


void obj_PolygoneBody::getDimensionsBox(Vector2D* v1, Vector2D* v2)
{

    assert ( v1 );
    assert ( v2 );

    CornersList::iterator i = rotated_corners.begin();
    CornersList::iterator end = rotated_corners.end();

    while ( i != end ) {

        Vector2D::MergeVector ( *i, v1, v2 );

        i++;
    }

}


void obj_PolygoneBody::physics_forces_calculate()
{

    CalculateRotatedCorners(); // поворот всех углов тела
//    CalculateTriangulation(); // новая триангуляция

    ApplyForce( g * mass ); // вес тела это сила тянущая его вниз

}


void obj_PolygoneBody::CalculateRotatedCorners()
{
    CGeom::RotatedToAngle( *corners_vectors, rotated_corners, center_mass, angle );
}


void obj_PolygoneBody::MouseClick()
{

    // координаты клика в GL относительно центра экрана
    SDL_Event event = HumanInterface::getEvent();
    Vector2D click_coords = Screen::Pixel2GL ( event.button.x, event.button.y );

    // расстояние от клика до объекта в физическом мире
//    Vector2D click_coords( event.button.x, event.button.y );

    Scalar len = (click_coords - center_mass).length();

    Vector2D v1, v2;
    getDimensionsBox( &v1, &v2 );

    if ( len < v1.length() || len < v2.length() ) {
        ApplyForce( Vector2D ( 0.0f, 2.9f) );
        apply_moment( -2.5 * dt );

    }

}


Scalar obj_PolygoneBody::CalculateMomentOfInertia() // FIXME
{
//    return getPolygoneMomentOfInertia( corners_vectors, getDensity() );
    return 4.0/3.0; // TODO need algo for moment of inertia
}

Scalar obj_PolygoneBody::CalculateMass() // FIXME
{
    return rotated_polygon->CalculateArea() * getDensity();
}


Vector2D obj_PolygoneBody::CalculateCenterMass()
{
    return rotated_polygon->CalculateCenterMass();
}
*/
