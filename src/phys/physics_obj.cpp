#include "physics_obj.h"

physics_obj* physics_obj::first_object_in_world = 0;


/// Обычно направлена вниз :)
Vector2D physics_obj::g = Vector2D (0.0f, - 9.8125f);
//Vector2D physics_obj::g = Vector2D (0.0f, 0.0f);

double physics_obj::moment_resistance = 0.05; ///< Сопротивление вращению
double physics_obj::air_resistance = 0.5; ///< Сопротивление воздуха
double physics_obj::dt = 0.000001; ///< Приращение времени


physics_obj::physics_obj() : template_obj_tree<physics_obj> (0)
{

  visible = true;

}


physics_obj::~physics_obj()
{
}


void physics_obj::all_forces_calculate()
{

    exec4all( &physics_obj::physics_forces_calculate );

}


void physics_obj::all_forces_apply()
{

    exec4all( &physics_obj::physics_forces_apply );

}


void physics_obj::all_forces_reset()
{

    exec4all( &physics_obj::physics_forces_reset );

}


Vector2D physics_obj::getCoordsFrom( physics_obj* ptr ){

  assert ( ptr != 0 );

  return coord - ptr->getCoord();

}


Vector2D physics_obj::getCoordsTo( physics_obj* ptr ){

  assert ( ptr != 0 );

  return ptr->getCoord() - coord;

}


void physics_obj::DisplayWithChilds(){

     ChildsList::iterator iter = child_items.begin();

     // узнаем, будет ли объект виден на экране
 /*    if ( ( Screen::getCurrentViewCenterVector() - getCoords() ).length() < (Screen::getGlScreenSize()/2).length() ) display();
 */    // TODO (это надо бы делать в районе вывода на gl, когда появятся для этого отдельтные функции

     if( visible ) display();

     // рисуем его детей
     while( iter != child_items.end() ) {

       physics_obj* child_obj = static_cast<physics_obj*> (*iter);

       Vector2D child_shift = child_obj->getCoord() - getCoord();

       glPushMatrix();
       glTranslatef (child_shift.val.x, child_shift.val.y, 0.0f);

       child_obj->DisplayWithChilds();

       glPopMatrix();

       iter++;

     }

}


void physics_obj::Init(){

// TODO make this function pure virtual

}


void physics_obj::getDimensionsBoxWithChilds(Vector2D* l_d, Vector2D* r_u){


     assert ( l_d != r_u );
     assert ( l_d ); assert ( r_u );

     getDimensionsBoxWithChilds2( Vector2D(0.0, 0.0), l_d, r_u);

}

void physics_obj::getDimensionsBoxWithChilds2(Vector2D shift, Vector2D* l_d, Vector2D* r_u){

     assert ( l_d != r_u );
     assert ( l_d ); assert ( r_u );

     // закидываем координаты текущего объекта
     Vector2D v1, v2;
     getDimensionsBox( &v1, &v2 );

     v1 += shift;
     v2 += shift;

     Vector2D::MergeVector( v1, l_d, r_u );
     Vector2D::MergeVector( v2, l_d, r_u );


     // пробегаем по детям
     ChildsList::iterator iter = child_items.begin();

     while( iter != child_items.end() ) {
       physics_obj* child_obj = static_cast<physics_obj*> (*iter);


child_obj->getDimensionsBoxWithChilds2( shift + child_obj->getCoordsFromParent(), l_d, r_u);

       iter++;
     }

}

