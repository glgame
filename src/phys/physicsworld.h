#ifndef PHYSICSWORLD_H
#define PHYSICSWORLD_H

#include "physics_obj.h"
#include "../input/humaninterface.h"

/**
корень для физических объектов

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class PhysicsWorld : public physics_obj
{
public:

    PhysicsWorld();

    ~PhysicsWorld();

    void display();
    void physics_forces_reset();
    void physics_forces_calculate();
    void physics_forces_apply();
    Vector2D getCoords();
    void getDimensionsBox(Vector2D *const v1, Vector2D *const v2);

};

#endif
