#ifndef CCONVEXPOLYGON_H
#define CCONVEXPOLYGON_H

#include "cbasepolygon.h"


/**
  @brief Выпуклый многоугольник

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/

class CConvexPolygon : public CBasePolygon
{
    /// Список треугольников триангуляции данного многоугольника
    TrianglesList* triangulation;

public:
    CConvexPolygon();
    CConvexPolygon( const CConvexPolygon& src );
    CConvexPolygon( ItersList* corners );
    ~CConvexPolygon();

    /// Триангулировать многоугольник
    TrianglesList* GetTriangulation();

    /// Отобразить в OpenGL треугольники многоугольника
    void display_triangles();

    /// Установить новый список углов
    void NewCorners( ItersList* corners );
};

#endif
