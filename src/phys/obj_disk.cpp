#include "obj_disk.h"

obj_Disk::obj_Disk(Vector2D* center, Scalar radius)
 : obj_Body(), EventMouseClick()
{

    set_defaults();

    setCoords( *center );
    obj_Disk::radius = radius;

}


obj_Disk::~obj_Disk()
{

}


/*!
    \fn obj_Disk::set_defaults()
 */
void obj_Disk::set_defaults()
{

    center_mass = Vector2D(0,0);
    obj_Disk::radius = 0.5;

    // ловля событий мышки, добавим себя в базовую группу обработчиков:
//    EventBase::base_group.AddItem(this);

}


void obj_Disk::Init()
{
    mass = CalculateMass();
    moment_inertia = CalculateMomentOfInertia();

    obj_Body::Init();
}


/*!
    \fn obj_Disk::display()
 */
void obj_Disk::display()
{

    glPushMatrix();

    glColor3f(1.0,1.0,0.0);

    glLineWidth ( 2.0f);
    glBegin( GL_LINES );

    // делаем такой шаг, чтобы при любом радиусе окружность была гладкой
    Angle step = fast_math::pi2 / ( (radius + 1) * 20);
    for ( Angle rot = 0; rot < fast_math::pi2; rot += step ) {
        glVertex2f( sin(rot) * radius, cos(rot) * radius );
        glVertex2f( sin(rot + step) * radius, cos(rot + step) * radius );
    }

    glEnd();

    // Нарисуем рисочку показывающую текущее положение диска
    glBegin( GL_LINES );

      glVertex2f( 0.0f, 0.0f );
      glVertex2f( sin(angle) * radius, cos(angle) * radius );

    glEnd();
    glLineWidth ( 1.0f);

    glPopMatrix();

}



void obj_Disk::physics_forces_calculate(){

    ApplyForce( g * mass ); // вес тела это сила тянущая его вниз

}


void obj_Disk::MouseClick(){

    if ( Filtered() ) return;
    ApplyForce( Vector2D ( -0.1f, 0.5f) );
    apply_moment( 0.01 * dt );

}



bool obj_Disk::Filtered(){

    // координаты клика в GL относительно центра экрана
    SDL_Event event = HumanInterface::getEvent();
    Vector2D click_coords = Screen::Pixel2GL ( event.button.x, event.button.y );

    // проверим - попали мы мышкой в радиус или нет
    if ( (click_coords - getCoord()).length() > radius ) return true ;

    return false;

}


void obj_Disk::getDimensionsBox(Vector2D *const v1, Vector2D *const v2){

    v1->val.x = -radius;
    v1->val.y = -radius;

    v2->val.x = radius;
    v2->val.y = radius;

}




Scalar obj_Disk::CalculateMomentOfInertia() const
{
    /*
    Момент инерции плоского круга радиуса R и массой M (равномерно распределенной по кругу) есть I = 0.5 * M * R^2.
    */

    return 0.5 * mass * pow(radius, 2.0);

}


Vector2D obj_Disk::NearestPointOfNutshell( Vector2D* point, bool* is_inside )
{

    // если точка дальше радиуса - она снаружи
    *is_inside = ( point->length() > radius ) ? false : true;

    return point->ort() * radius;

}


Vector2D obj_Disk::NearestPointFromVector( Vector2D* vector, Vector2D* body_coord, Vector2D* res_from_point )
{

    assert( res_from_point );

    // направление от вектора на центр диска и точка начала направления
    Vector2D to_center = vector->ShortestDirectionToPoint( body_coord, res_from_point );

    // точка лежит ровно на радиусе
    // если вектор пересекает окружность то вернётся направление на точку
    // на середине между пересечениями, что нас тоже устраивает
    return to_center - to_center.ort() * radius;

}


Scalar obj_Disk::CalculateMass() const
{
    return fast_math::pi * radius * radius * getDensity();
}
