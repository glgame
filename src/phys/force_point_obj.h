#ifndef FORCE_POINT_OBJ_H
#define FORCE_POINT_OBJ_H

#include <iostream>

#include "physics_obj.h"
#include "../math/vector2d.h"
#include "../debug.h"

/**
  @brief Точка приложения сил

  Внимание: Не путайте силу и импульс. Будьте внимательны!

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class force_point_obj : public physics_obj {

    /// Размер крестика, обозначающего точку при выводе в OpenGL
    static GLfloat size;

    /// Внутренний инит объекта
    void init_defaults();

protected:
    Vector2D impulse; ///< Текущие силы, передаваемые через точку

public:
    void Init(); ///< Внешний инит

    bool show_debug_vector; ///< Показывать вектор импульса (для дебага)
    bool show_debug_vector_axis; ///< Показывать оси вектора импульса (для дебага)
    bool show_debug_to_stdout; ///< Выводить в stdout информацию об импульсе

    force_point_obj();
    force_point_obj(Vector2D coords);
    force_point_obj(Vector2D* coords);
    ~force_point_obj();

    void physics_forces_reset();
    void physics_forces_calculate();
    void physics_forces_apply();

    virtual void display();

    void getDimensionsBox(Vector2D *const v1, Vector2D *const v2);

    /// Применить силу к точке
    inline void apply_force (Vector2D apply_force) { impulse += apply_force * dt; };

    /// Применить импульс к точке
    inline void ApplyImpulse (Vector2D apply_impulse) { impulse += apply_impulse; };

    /// Найти текущие силы применяемые к точке
    inline Vector2D getForce() const { return impulse; };
    void ShowDebugVector(); ///< Отобразить дебажный вектор в OpenGL

};

#endif
