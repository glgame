#include "obj_hypercube.h"

// Гиперкуб :)

GLuint obj_hypercube::hypercube_id = 0;

obj_hypercube::obj_hypercube(): displayed_obj()
{
  if (!hypercube_id) {
    hypercube_id = glGenLists(1);
    glNewList(hypercube_id,GL_COMPILE);

    const GLfloat size = 1.0f;
    const GLfloat step = 0.25f;

    for(GLfloat z = -size; z <= size; z+=step) {
    //   glTranslatef(0.0f, 0.0f, z);
        for(GLfloat x = -size; x <= size; x+=step) {
            glBegin( GL_LINES );
              glVertex3f( x, -size, z);
              glVertex3f( x, size, z);
    //        glEnd();

    //        glBegin( GL_LINES );
              glVertex3f( -size, x, z);
              glVertex3f( size, x, z);
    //        glEnd();

    //        glBegin( GL_LINES );
              glVertex3f( x, z, -size);
              glVertex3f( x, z, size);
            glEnd();
        }
    }
    glEndList();

  }
}

obj_hypercube::~obj_hypercube()
{
}




/*!
    \fn obj_hypercube::display()
 */
void obj_hypercube::display()
{
     glCallList(hypercube_id);
}
