#ifndef T_MY_LIST_H_
#define T_MY_LIST_H_

#include <list>
#include "tloop_iterator.h"

/**
@brief Класс списка с кольцевым итератором

@class den::list
*/
namespace den
{

  template <typename T>
  struct list : std::list<T>
  {
      /// Кольцевой итератор
      typedef loop_iterator<list> loop_iterator;
  };

}

#endif
