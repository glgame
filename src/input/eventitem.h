#ifndef EVENTITEM_H
#define EVENTITEM_H

#include <SDL/SDL.h>
#include <SDL/SDL_events.h>

#include "../debug.h"

/**
	@author Феклушкин Денис <edu2005-60@mail.ru>
*/


typedef SDL_Event event_t;
//typedef SDL_Events event_type_t;

class EventItem{

protected:
    bool RecvEnabled;
    static bool StopHandling;
    bool (EventItem::*filter_ptr) ();
    virtual bool Filtered() = 0; // внимание! проверять только то что надо, так как в одном сообщении может быть несколько событий!!
    EventItem* parent;
    inline void setFilter ( bool (EventItem::*filtered_func)() ) { filter_ptr = filtered_func; };

public:
    EventItem();
    virtual ~EventItem();

    virtual void RecvEvent() = 0;
    static event_t* event;

    inline static void StopHandlingReset() { StopHandling = false; };

};

#endif
