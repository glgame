#include "eventgroup.h"

EventGroup::EventGroup() : EventItem()
{

    RecvEnabled = true;
//    child_items = new EventGroupList;

}


EventGroup::~EventGroup()
{
}




/*!
    \fn EventGroup::AddItem (EventGroup* ptr)
 */
void EventGroup::AddItem (EventItem* ptr)
{

    child_items.push_back( ptr );

}


void EventGroup::RemoveItem (EventItem* ptr)
{

    child_items.remove( ptr ); // FIXME!!!

}


void EventGroup::RecvEvent()
{

    if ( Filtered() ) return;

    // перешлём сообщение дочерним элементам
    EventGroupList::iterator i = child_items.begin();

    while( i != child_items.end() ) {

/*      if ( event->type == SDL_MOUSEBUTTONDOWN && event->button.button == SDL_BUTTON_LEFT ) MSG("xxx");*/
      (*i)->RecvEvent();
      if( EventItem::StopHandling ) break; // событие кто-то обработал и отсановил обработку
      i++;

    }

}


bool EventGroup::Filtered()
{

  return EventItem::Filtered();

}
