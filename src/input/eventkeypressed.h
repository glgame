#ifndef EVENTKEYPRESSED_H
#define EVENTKEYPRESSED_H

#include "eventkeyboard.h"


/**
нажата одна клавиша

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class EventKeyPressed : public EventKeyboard
{

    SDL_KeyboardEvent event_prev;
    bool pressed;

protected:
    void ProcessEventType();
    void RecvEvent();
    virtual void AnyKeyPressed() = 0;

public:
    EventKeyPressed();
    ~EventKeyPressed();

};

#endif
