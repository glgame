#ifndef EVENTPROXY_H
#define EVENTPROXY_H


/**
шаблон-адаптер для приёма событий и передачи их в функцию-хэндлер любого класса

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/

class EventProxy
{

public:
    EventProxy();
    virtual ~EventProxy();

    virtual void RecvEvent() = 0;

};

#endif
