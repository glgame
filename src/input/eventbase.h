#ifndef EVENTBASE_H
#define EVENTBASE_H

#include <map>
#include <list>
#include <SDL/SDL.h>

#include "eventgroup.h"

/**
Базовый класс для событий

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class EventBase  {


protected:
    static event_t event;

public:
    EventBase();
    virtual ~EventBase();

//    static void ProcessEvents() = 0;

};

#endif
