#ifndef HUMANINTERFACE_H
#define HUMANINTERFACE_H

#include <list>
#include <SDL/SDL_events.h>
#include "../screen.h"
#include "../options.h"
#include "eventproxy.h"


/**
принимает события от системы и вызывает обработчики согласно списка

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/

typedef enum {
    CLICK,
    MOUSE_DOWN,
    MOUSE_UP,
    MOUSE_CLICK
} EventType;

class HumanInterface{

    static SDL_Event event;
    static EventType event_type;

    typedef std::list<SDLKey> PressedKeys;
    static PressedKeys pressed_keys;

    static bool mbut_pressed[6]; // положение кнопок мышки

    typedef struct {
      EventType Type;
      EventProxy* Obj;
    } Handler;

    // список принимающих событий
    typedef std::list<Handler> HandlersList;
    static HandlersList handlers_list;

public:
    HumanInterface();
    ~HumanInterface();

    static void AddHandler( EventType type, EventProxy* obj );
    static void HandleEvents();
    static bool KeyPressed(SDLKey key);
    static void CallHandlers( EventType type );

    static inline SDL_Event getEvent() { return event; }
    static inline EventType getEventType() { return event_type; }

private:
    static void HandleKeyboard();
    static void PlayingKeys();
    static void MouseEvents();
    static bool MouseButtionsPressed();
    static void MouseClickDetection();

};

#endif
