#include "humaninterface.h"

extern Options options;

SDL_Event HumanInterface::event;
EventType HumanInterface::event_type;
std::list<SDLKey> HumanInterface::pressed_keys;

// инициализация списка нажатых кнопок мыши
bool HumanInterface::mbut_pressed[] = { false, false, false, false, false, false };

// инициализация списка всех обработчиков
HumanInterface::HandlersList HumanInterface::handlers_list;

HumanInterface::HumanInterface()
{
}


HumanInterface::~HumanInterface()
{
}


void try_exit(int code) {

  // обход баги(?) когда при выходе из программы остаётся полноэкранное разрешение экрана
  if ( options.getBoolValue(Options::FULLSCREEN) ) {
          options.setValue(Options::FULLSCREEN, "false");
          Screen::Init();
  }

  exit(code);

}


/*!
    \fn HumanInterface::HandleEvents()
 */
void HumanInterface::HandleEvents()
{

  while ( SDL_PollEvent(&event) ) {

    // обработаем мышку
    MouseEvents();

    // кнопку нажали
    if ( event.type == SDL_KEYDOWN ) {

        // добавляем в список нажатых кнопок если её там ещё нет
        if ( !KeyPressed( event.key.keysym.sym ) ) {
                pressed_keys.push_back( event.key.keysym.sym );

                // и если первое нажатие чтобы исключить "дребезг" вызываем обработчик управляющих клавиш (выход, пауза и т.п.)
                HandleKeyboard();
        }

    }

    // кнопку отжали
    if ( event.type == SDL_KEYUP ) {

        // убираем из списка нажатых кнопок если она там
        if ( KeyPressed( event.key.keysym.sym) )
                pressed_keys.remove( event.key.keysym.sym );

    }


     // изменение размера окна
     if ( event.type == SDL_VIDEORESIZE ) {
          Screen::Resized ( event.resize.w, event.resize.h );
     }

     // требуется перерисовка окна
     if ( event.type == SDL_VIDEOEXPOSE ) {
          Screen::Refresh();
     }

     // запрос закрытия окна
     if ( event.type == SDL_QUIT ) try_exit(0);

  }

  // обрабатываем нажатия клавиш, которые могут нажиматься отжиматься произвольно (в том числе одновременно)
  PlayingKeys();

}


/*!
    \fn HumanInterface::HandleKeyboard()
 */
void HumanInterface::HandleKeyboard()
{

      // Toggle screen view
      if ( event.key.keysym.sym == SDLK_f ) {
            options.getBoolValue(Options::FULLSCREEN) ?
                  options.setValue(Options::FULLSCREEN, "false") :
                  options.setValue(Options::FULLSCREEN, "true");
            Screen::Init();
      };

      // пользователь запросил выход из игры
      if ( event.key.keysym.sym == SDLK_ESCAPE ||
              event.key.keysym.sym == SDLK_q ) try_exit(0);


      // нажата пауза
      if ( event.key.keysym.sym == SDLK_p ) {
            if ( options.getBoolValue(Options::PAUSED) )
                    options.setValue(Options::PAUSED, "false");
            else
                    options.setValue(Options::PAUSED, "true");
      }

}


/*!
    \fn HumanInterface::KeyPressed(SDLKey key)
 */
bool HumanInterface::KeyPressed(SDLKey key)
{

    PressedKeys::iterator i = pressed_keys.begin();
    PressedKeys::iterator end = pressed_keys.end();

    while ( i != end ) {

      if ( *i == key ) return true;
      i++;

    }

    return false;

}


/*!
    \fn HumanInterface::PlayingKeys()
 */
void HumanInterface::PlayingKeys()
{
     const Coord move_step = 0.08f;

      // курсорные кнопки
      if ( KeyPressed( SDLK_UP ) )
            Screen::SetScreenBoxOffset(0, move_step);
      if ( KeyPressed( SDLK_DOWN ) )
            Screen::SetScreenBoxOffset(0, -move_step);
      if ( KeyPressed( SDLK_LEFT ) )
            Screen::SetScreenBoxOffset(-move_step, 0);
      if ( KeyPressed( SDLK_RIGHT ) )
            Screen::SetScreenBoxOffset(move_step, 0);
}

bool HumanInterface::MouseButtionsPressed() {
  for ( Uint8 i = 1; i <= 3; i++ )
          if ( mbut_pressed[i] ) return true;
  return false;
}

/*!
    \fn HumanInterface::MouseEvents()
 */
void HumanInterface::MouseEvents()
{

    if ( event.type == SDL_MOUSEBUTTONDOWN ) {
          // обработка нажатия мышью
          if ( !mbut_pressed[event.button.button] ) {
                    CallHandlers( MOUSE_DOWN );
                    MouseClickDetection();
          }
          mbut_pressed[event.button.button] = true;
    }

    if ( event.type == SDL_MOUSEBUTTONUP ) {
          // обработка отпускания мыши
          if ( mbut_pressed[event.button.button] ) {
                    CallHandlers( MOUSE_UP );
//                    MouseClickDetection();
          }
          mbut_pressed[event.button.button] = false;
    }

    if ( event.type == SDL_MOUSEMOTION ) {
          if ( mbut_pressed[1] ); // обработка перемещения мышью с нажатой левой кнопкой
    }

    // зум колёсиком если другие кнопки не нажаты
    static GLfloat const depth_inc = 0.2f;
    static GLfloat const depth_max = 9.0f;
    static GLfloat const depth_min = 0.0f;

    if ( mbut_pressed[4] && !MouseButtionsPressed() &&
         (Screen::getDepth() + depth_inc) < depth_max ) {

            Screen::setDepth( Screen::getDepth() + depth_inc );
            PR(Screen::getDepth());

    }

    // отдаление колёсиком
    if ( mbut_pressed[5] && !MouseButtionsPressed() &&
         (Screen::getDepth() - depth_inc) > depth_min ) {

            Screen::setDepth( Screen::getDepth() - depth_inc );
            PR(Screen::getDepth());

    }

}


void HumanInterface::AddHandler( EventType type, EventProxy* obj )
{

    Handler tmp;
    tmp.Type = type;
    tmp.Obj = obj;

    handlers_list.push_back( tmp );

}
// FIXME DeleteHandler need


void HumanInterface::CallHandlers( EventType type )
{

  HandlersList::iterator i = handlers_list.begin();
  HandlersList::iterator end = handlers_list.end();

  event_type = type; // статическая переменная с типом на всякий случай

  while ( i != end ) {
      if ( (*i).Type == type ) (*i).Obj->RecvEvent();
      i++;
  }

}




/*!
    \fn HumanInterface::MouseClickDetection()
 */
void HumanInterface::MouseClickDetection()
{

    // проверим не нажаты ли кнопки кроме левой
    for ( Uint8 i = 2; i <= 5; i++ )
         if ( mbut_pressed[i] ) return;

    CallHandlers( MOUSE_CLICK );

/*
    static bool was_pressed = false;


    // TODO хорошо бы считать таймаут и сделать этот класс даблкликом

    if ( event.type == SDL_MOUSEBUTTONDOWN && !was_pressed ) {
            was_pressed = true;
            CallHandlers( MOUSE_CLICK );
    }

    if ( event.type == SDL_MOUSEBUTTONUP && was_pressed ) {
            was_pressed = false;
    }*/

}
