#ifndef EVENTGROUP_H
#define EVENTGROUP_H

#include <list>
#include <SDL/SDL.h>

#include "eventitem.h"


/**
объединим события в группу

  @author Феклушкин Денис <edu2005-60@mail.ru>
*/
class EventGroup : public EventItem
{

protected:
    typedef std::list<EventItem*> EventGroupList;

    bool Filtered();

public:
    EventGroup();
    virtual ~EventGroup();

    void AddItem (EventItem* ptr);
    void RemoveItem (EventItem* ptr);
    void RecvEvent();

private:
    EventGroupList child_items;

};

#endif
