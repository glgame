#ifndef EVENTMOUSEMOVE_H
#define EVENTMOUSEMOVE_H

//#include "eventmouse.h"

/**
обработчик передвижений мышкой

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class EventMouseMove //: public EventMouse
{

    virtual void MouseMove() = 0;

protected:
    bool Filtered();
    void RecvEvent();

public:
    EventMouseMove();

    virtual ~EventMouseMove();

};

#endif
