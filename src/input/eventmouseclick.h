#ifndef EVENTMOUSECLICK_H
#define EVENTMOUSECLICK_H

#include "humaninterface.h"

/**
мышкин даблклик

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class EventMouseClick : public EventProxy
{

    bool pressed;
    void RecvEvent();

 protected:
    virtual void MouseClick() = 0;

 public:

    EventMouseClick();
    virtual ~EventMouseClick();

};

#endif
