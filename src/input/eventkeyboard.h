#ifndef EVENTKEYBOARD_H
#define EVENTKEYBOARD_H

#include "eventitem.h"
#include "../linked_list_t.h"


/**
Обработка событий от клавиатуры

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class EventKeyboard : protected EventItem, public linked_list_t<EventKeyboard>
{

protected:
    bool Filtered();

public:
    EventKeyboard();
    ~EventKeyboard();

};

#endif
