#include "eventkeypressed.h"

EventKeyPressed::EventKeyPressed()
 : EventKeyboard()
{

    pressed = false;

}


EventKeyPressed::~EventKeyPressed()
{
}


void EventKeyPressed::RecvEvent()
{
    // наше ли это событие?
     if ( Filtered() ) return;

     if ( event->type == SDL_KEYDOWN && pressed == false) {
          pressed = true;
          AnyKeyPressed();
     }

     if ( event->type == SDL_KEYUP ) {
          pressed = false;
     }

}
