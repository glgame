#ifndef CGLTEXTURE_H
#define CGLTEXTURE_H

/**
This is standard texture creation with OpenGL. If you do not understand how this works, you should read about OpenGL Textures. SDL_LoadBMP returns a SDL_Surface that stores the pixels as RGB. This is why GL_RGB is used when creating the texture.

(короче, просто ведёт счётчик ссылок, загружает текстуры и отдаёт на них указатель)

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/

#include <string>
#include <SDL/SDL_opengl.h>
#include <stdexcept>
#include <SDL/SDL.h>


class CglTexture{
public:
    CglTexture(std::string name);

    ~CglTexture();

};

#endif
