#ifndef MENUMANAGER_H
#define MENUMANAGER_H

#include <string>
#include "menu/menubaseitem.h"

/**
менеджер для иерархии окон. иерархий может быть несколько - это удобно
одна для контекстного меню внутри игры, другая - главное меню сбоку экрана и т.п.

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class MenuManager : public MenuBaseItem
{

public:
    MenuManager(std::string name);
    ~MenuManager();
    void display_all();

private:
    void display();
};

#endif
