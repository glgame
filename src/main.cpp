

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include <SDL/SDL_opengl.h>
#include <GL/glu.h>

#include "options.h"
#include "debug.h"
#include "line.h"
#include "phys/obj_fromlist.h"
#include "math/fast_math.h"
#include "input.h"
#include "phys/obj_rope.h"
#include "hinge_obj.h"
#include "phys/obj_displayedvector.h"
#include "phys/obj_disk.h"
#include "math/vector2d.h"
#include "phys/obj_bodyhinge.h"
#include "phys/busiki.h"
#include "menu/morfwindow.h"
#include "input/eventbase.h"
#include "input/eventkeypressed.h"
#include "menu/menubutton.h"
#include "menu/menubuttontrigger.h"
#include "menumanager.h"
#include "rightmenu.h"
#include "phys/physicsworld.h"
#include "input/humaninterface.h"
#include "phys/obj_polygonebody.h"
#include "build_counter.h"
#include "phys/geometry.h"
#include "phys/ccurve.h"
#include "phys/obj_curvebody.h"
#include "tloop_iterator.h"


// поместим в бинарник информацию о сборке
// (эти макросы раньше устанавливались autotools):
#define PACKAGE "glgame"
#define VERSION "0.1"
static std::string const ProjectSummary ( "\n" PACKAGE " version: " VERSION " (build: " BUILD_COUNTER ", source: " SOURCE_ID ")\n" );


#ifndef __WIN32__
// linux and others:
std::string homedir = getenv("HOME");
std::string cfg_filename = homedir + "/.glgame.conf";
#else
// win32:
std::string cfg_filename = "glgame.conf";
#endif
  Options options;

using namespace std;

#ifdef __WIN32__

#include <windows.h>

int WINAPI WinMain (HINSTANCE hInstance,
                        HINSTANCE hPrevInstance,
                        PSTR szCmdLine,
                        int iCmdShow)
#else
int main(int argc, char *argv[])
#endif
{

std::string product_name( PACKAGE " version: " VERSION " (build: " BUILD_COUNTER ")" );
  options.loadFromFile( cfg_filename );


  try {
 cout <<"Initializing SDL." << endl;
  /* Initializes Audio and the CDROM, add SDL_INIT_VIDEO for Video */
  if(SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_OPENGL | SDL_RESIZABLE)< 0) {
    cout <<"Could not initialize SDL:" << SDL_GetError() << endl;
    //printf("Unable to set 640x480 video: %s\n", SDL_GetError());
       SDL_Quit();
  } else {
    SDL_WM_SetCaption(product_name.c_str(), 0);
    cout << "Audio & Video initialized correctly" << endl;
  }





//////////// PLAYGROUND
/*
 typedef den::list<int> ints;
 ints* crns666 = new ints;
 crns666->push_back ( 2 );
 crns666->push_back ( 3 );
 crns666->push_back ( 4 );

// ints::loop_iterator loop( crns666, crns666->begin() );
 ints::loop_iterator loop2( *crns666 );
 ints::iterator it = crns666->begin();

loop2 = it;

 PR( *(--loop2) );
 PR( *(++loop2) );
 PR( *(--loop2) );
 PR( *(++loop2) );
 PR( *(--loop2) );

exit(0);
*/
// loop_iterator<VectorsList> loop2 = loop;

/*
CBasePolygon::ItersList crns_iters;
CPolygon a3( crns666 );

CPolygon::ItersList::iterator i3;
i3 = a3.GetItersList()->begin();
i3 = a3.GetItersList()->GetNext( i3 );
i3 = a3.GetItersList()->GetNext( i3 );
PR( a3.IsCornerConvex( i3 ) );

delete crns666;
*/
////////////

static Screen scr;


// creating prism
GLuint triangle = glGenLists(1);
glNewList(triangle,GL_COMPILE);

     glBegin( GL_TRIANGLES );
        glColor3f(1.0,0.0,0.0);
         glVertex3f( 0.0, 1.0, 0.0);
         glVertex3f(-1.0,-1.0, 1.0);
         glVertex3f( 1.0,-1.0, 1.0);
        glColor3f(0.0,1.0,0.0);
         glVertex3f( 0.0, 1.0, 0.0);
         glVertex3f( 1.0,-1.0, 1.0);
         glVertex3f( 1.0,-1.0, -1.0);
        glColor3f(0.0,0.0,1.0);
         glVertex3f( 0.0, 1.0, 0.0);
         glVertex3f( 1.0,-1.0, -1.0);
         glVertex3f(-1.0,-1.0, -1.0);
        glColor3f(1.0,1.0,0.0);
         glVertex3f( 0.0, 1.0, 0.0);
         glVertex3f(-1.0,-1.0,-1.0);
         glVertex3f(-1.0,-1.0, 1.0);
     glEnd();
     glBegin( GL_QUADS );
        glColor3f(1.0,0.0,1.0);
         glVertex3f( 1.0,-1.0,-1.0);
         glVertex3f( 1.0,-1.0, 1.0);
         glVertex3f(-1.0,-1.0, 1.0);
         glVertex3f(-1.0,-1.0,-1.0);
     glEnd();
glEndList();


GLuint star = glGenLists(1);
glNewList(star,GL_COMPILE);

  glColor3f(1.0,1.0,1.0);
  glTranslatef(0.0f,0.0f,0.0f);
for(GLfloat j = 0; j < 1; j+=0.01){
      glRotatef(5, 0.0f, 0.0f, 1.0f);
      glBegin( GL_LINES );
        glVertex2f( 0, 0);
        glVertex3f( 0.0f, 1.0f, 0);
      glEnd();
}
glEndList();


// сотворим :) физический мир

PhysicsWorld* physics_world = new PhysicsWorld();

// нарисуем окно меню

 MenuManager* manager = new MenuManager( "main_menu" );

 RightMenu* rightmenu = new RightMenu();
 rightmenu->setParent( manager );

 force_point_obj *start = new force_point_obj( Vector2D (-2,2) );
 start->setParent( physics_world );
 start->Init();

 MenuObjButton* butto = new MenuObjButton();
 butto->setParent ( rightmenu );
 butto->setPos( Vector2D ( -8.7, -7.3 ) );
 butto->setSize( Vector2D ( 2.5, - 2.0) );
 butto->obj = start;


force_point_obj* up_fixed = new force_point_obj ( Vector2D(0.5,1.5) );


physics_obj::g = Vector2D (0.0f, -0.4f);
physics_obj::moment_resistance = 0.01;
physics_obj::air_resistance = 0.01;
physics_obj::dt = 0.0001; // приращение времени за одну итерацию движка


force_point_obj* up = new force_point_obj ( Vector2D(0.5,0.5) );
up->setParent( physics_world );

//for (int i = 0; i < 1; i++) new segment_obj(up_fixed, ss);

 force_point_obj *end   = new force_point_obj( Vector2D (2, 2) );
 end->setParent( start );
 end->Init();

 /*
	Тестируем моменты инерции
  */

 VectorsList crnsPol;
//crnsPol.push_back ( Vector2D (  4.0,  0.0) );
//crnsPol.push_back ( Vector2D (  3.0,  2.0) );
//crnsPol.push_back ( Vector2D (  2.0,  3.0) );
//crnsPol.push_back ( Vector2D (  0.0,  4.0) );
//crnsPol.push_back ( Vector2D ( -2.0,  3.0) );
//crnsPol.push_back ( Vector2D ( -3.0,  2.0) );
//crnsPol.push_back ( Vector2D ( -4.0,  0.0) );
//crnsPol.push_back ( Vector2D ( -3.0, -2.0) );
//crnsPol.push_back ( Vector2D ( -2.0, -3.0) );
//crnsPol.push_back ( Vector2D (  0.0, -4.0) );
//crnsPol.push_back ( Vector2D (  2.0, -3.0) );
//crnsPol.push_back ( Vector2D (  3.0, -2.0) );

crnsPol.push_back ( Vector2D (  1.0,  0.0) );
crnsPol.push_back ( Vector2D (  0.0,  1.732) );
crnsPol.push_back ( Vector2D ( -1.0,  0.0) );
//crnsPol.push_back ( Vector2D (  1.0, -1.0) );



 ItersList itListPoly;
 VectorsList::iterator i = crnsPol.begin();

 while ( i != crnsPol.end() ){
	itListPoly.push_back(i++);
 }

CPolygon dumbPoly( &itListPoly );

 dumbPoly.CalcMomentOfInertiaCapacity();

 Vector2D d(2, 0);
 Vector2D c(1, 2);

 VectorsList* curvePol;

 CCurve curv(d, c);
 curvePol = curv.ConvertToVectors(20);
 ItersList itListCurve;
 i = curvePol->end();

 while ( i != curvePol->begin() ){
	itListCurve.push_back(--i);
 }

CPolygon curvPoly( &itListCurve );

 curv.CalcMomentOfInertiaCapacity();
 curvPoly.CalcMomentOfInertiaCapacity();


  /*
	Протесировали моменты инерции
  */



 VectorsList crns;
 //crns.push_back ( Vector2D (  0.0, 1.0 ) );
 //crns.push_back ( Vector2D (  3.0, 0.0 ) );
 //crns.push_back ( Vector2D (  3.0, -3.0 ) );
 //crns.push_back ( Vector2D (  6.0, 0.0 ) );
 //crns.push_back ( Vector2D (  0.0, 3.0 ) );
 crns.push_back ( Vector2D (  1.0, 1.0 ) );
 crns.push_back ( Vector2D ( -1.0, 1.0 ) );
 crns.push_back ( Vector2D ( -1.0,-1.0 ) );
 crns.push_back ( Vector2D (  1.0,-1.0 ) );

 DoubleCorners( crns );
 DoubleCorners( crnsPol );

VectorsList* starr = GetStarPolygone( 5, Vector2D(-4,0), Vector2D(-2, 0.3 ) );

VectorsList corners;
VectorsList controls;
CGeom::SplitInto2Lists( starr, corners, controls );

VectorsList corn;
VectorsList cont;
CGeom::SplitInto2Lists( &crns, corn, cont );

obj_CurveBody* telo = new obj_CurveBody();
telo->setParent( start );
telo->NewPoints( &corn, &cont );
//telo->NewPoints( &corners, &controls );
telo->setCoords( Vector2D(0, 0) );
telo->DisplayCorners = true;
telo->setDensity( 1.00 );
telo->CalcMomentOfInertiaCapacity();
telo->getMomentOfInertia();



obj_CurveBody* telo2 = new obj_CurveBody();
telo2->setParent( start );
telo2->NewPoints( &corners, &controls );
telo2->setCoords( Vector2D(-2, 0) );
telo2->setDensity( 1.1 );
telo2->DisplayCorners = true;


//telo->ApplyForce( &Vector2D( 70, -70.8), &Vector2D(-0.5,-0.5) );
//telo2->ApplyForce( &Vector2D( -1, 0) );

force_point_obj* hh6 = telo->addBodyHingeFromCenter( Vector2D( 1,1) );

MenuObjButton* buttobj;


Vector2D* disk_center1 = new Vector2D(-2,0);
Vector2D* disk_center2 = new Vector2D(2,0);
Vector2D impact_force(2.0,0.0);
Vector2D coord(0.3,0.3);

    hinge_obj* tes = new hinge_obj( start->getCoord() );
    tes->setParent( start );
    tes->Init();

    segment_obj* rropp = new segment_obj( hh6, end );
    rropp->setParent ( telo );
    rropp->Init();

      buttobj = new MenuObjButton();
      buttobj->setParent ( rightmenu );
      buttobj->setPos( Vector2D ( 0.1, -2.6 ) );
      buttobj->setSize( Vector2D ( 0.8, - 0.8) );
      buttobj->obj = telo;

MSG("ssssssss");



  obj_Busiki* bbusiki = new obj_Busiki(start, end, 5, 1, 0.2, 0.5, 0.4 );
  bbusiki->setParent ( telo );
//
//      buttobj = new MenuObjButton();
//      buttobj->setParent ( rightmenu );
//      buttobj->setPos( Vector2D ( 0.1, -3.7 ) );
//      buttobj->setSize( Vector2D ( 1.2, -1.2) );
//      buttobj->obj = bbusiki;

     buttobj = new MenuObjButton();
     buttobj->setParent ( rightmenu );
     buttobj->setPos( Vector2D ( 0.1, -7.7 ) );
     buttobj->setSize( Vector2D ( 1.2, -1.2) );
     buttobj->obj = telo;



int timer;
const int max_fps = options.getIntValue(Options::MAX_FPS);
Uint32 time_per_frame = 1000/max_fps;

#ifndef NDEBUG
unsigned long frames = 0;
unsigned long est_time = 0;
#endif

physics_obj::all_forces_apply();

MSG("Main loop starting");

while(true) {
    for(GLfloat rot = 0.0; rot < 360.0; rot+=2.0) {
      timer = (int)SDL_GetTicks() + time_per_frame;

      // показываем сцену
      Screen::Refresh();

      // показываем физический мир
telo->HandlePenetrations( *telo2 );
//telo2->HandlePenetrations( *telo );
      physics_world->DisplayWithChilds();

      manager->display_all(); // рисуем меню

      SDL_GL_SwapBuffers(); // FIXME взял в конце Screen::Refresh() - вернуть!

      HumanInterface::HandleEvents(); // новая обработка ввода

      if ( !options.getBoolValue(Options::PAUSED) ) {

          // обсчёт физики несколько раз прежде чем 1 раз показать
          static const int speed_up = 1;
          for(int i = 0; i < speed_up; i++) {

            physics_obj::all_forces_calculate();  // подсчёт сил

            physics_obj::all_forces_apply();      // применение сил

            physics_obj::all_forces_reset();  // сборс сил

          }

      }

      timer -= SDL_GetTicks();
#ifndef NDEBUG
      frames++;
#endif
      if (timer > 0) {
#ifndef NDEBUG
          est_time+=timer;
#endif
          SDL_Delay(timer);
      }
    }
}

#ifndef NDEBUG
cout << "Average FPS: " << (float)frames / (float)est_time * 1000 <<
        " Frames showed: " << frames << " Time: " << est_time << "ms" << endl;
#endif

  SDL_Quit();
 } catch (std::runtime_error& catched) {
    std::cout << "catch(): " << catched.what() << std::endl;
  }

  return 0;

}
