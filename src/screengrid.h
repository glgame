#ifndef SCREENGRID_H
#define SCREENGRID_H

#include <SDL/SDL_opengl.h>
#include "gl_functions.h"
#include "options.h"
#include "math/vector2d.h"
//#include "screen.h"


/**
зелёная сеточка, сделал "на отъебись", но вроде работает

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class ScreenGrid{

    bool regenerate;

public:
    GLfloat grid_step;
    GLint   bold_step;
    GLfloat labels_size;
    GLfloat grid_size;

    GLfloat gl_depth;

     GLfloat grid_center_x;
     GLfloat grid_center_y;

    Vector2D GlScreenSize;
//    Vector2D view_coord;

    GLuint  grid_list;

    ScreenGrid();
    ~ScreenGrid();

    void display( Vector2D view_coord );
    void Init();
    void display_labels();
    void RegenerateGrid();

};

#endif
