#ifndef HINGE_OBJ_H
#define HINGE_OBJ_H

#include "debug.h"
#include "math/vector2d.h"
#include "phys/obj_body.h"


/**
  @brief Свободнолетающий шарнир

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class hinge_obj : public obj_Body
{
    // единственный шарнир на этом точечном теле
    force_point_obj* hinge;

    /// Отображаемые в OpenGL размеры точки
    static const Scalar display_size;

    /// Внутренний инит
    void Init_defaults();

public:
    bool fixed; ///< Объект прибит гвоздями к миру?

    hinge_obj(Vector2D coord);
    hinge_obj(point_t point);
    hinge_obj(Coord x, Coord y); ///< Конструктор с координатами появления шарнира
    ~hinge_obj();

    void display(); ///< Отображение средствами OpenGL

    void physics_forces_calculate();

    /// Возвращает ссылку на точку применения сил шарнира
    force_point_obj* GetHinge(){ assert(inited); return hinge; }

    // функции тела, которые для точки все равны простым значениям:
    void getDimensionsBox(Vector2D* v1, Vector2D* v2);
    Scalar getMomentOfInertia() const { return 1.0; }
    void SetMass( Scalar m ) { assert(!inited); mass = m; }
    Vector2D getCenterMass() const { return Vector2D(); }
    Scalar getMass() const { return mass; }

};

#endif
