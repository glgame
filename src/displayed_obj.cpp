#include "displayed_obj.h"


displayed_obj::ObjList displayed_obj::list_obj;

displayed_obj::displayed_obj()
{
  list_obj.push_back(this);
  this_obj = list_obj.end();

  visible = true;
}


displayed_obj::~displayed_obj()
{
  list_obj.remove(this); // FIXME fast deleting using iterator need
}

void displayed_obj::display_all() {
  ObjList::iterator i = list_obj.begin();
  while(i != list_obj.end() ) {

    if ( (*i)->visible ) (*i)->display();
    i++;

  }
}

