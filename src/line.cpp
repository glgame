#include "line.h"

// line::line(Coord start_x, Coord start_y, Coord end_x, Coord end_y)
//  : displayed_obj()
// {
//   s_x = start_x;
//   s_y = start_y;
//   e_x = end_x;
//   e_y = end_y;
// 
//   projection_x = fabs ( - end_x + start_x  );
//   projection_y = fabs ( - end_y + start_y  );
// 
//   length = sqrt ( pow ( projection_x, 2 ) +
//                   pow ( projection_y, 2 )
//            );
// 
//   rotation = atan(projection_y / projection_x) - fast_math::pi/2;
// 
// 
// PR(projection_x);
// PR(projection_y);
// PR(rotation);
// PR(length);
// PR(s_x);
// PR(s_y);
// 
// 
// }

line::line(Coord start_x, Coord start_y, Angle rot, Length len) : displayed_obj() {

  s_x = start_x;
  s_y = start_y;
  rotation = rot;
  length = len;

  projection_x = length * cos (rotation);
  projection_y = length * sin (rotation);

}

line::~line()
{
}


/*!
    \fn line::display()
 */
void line::display()
{
    static double toDegrees = fast_math::pi2 / 360;
//    static GLfloat depth = -5.5f;

    glPushMatrix();

    glColor3f(1.0,1.0,1.0);

    glTranslatef(s_x, s_y, 0);
    glRotated(rotation / toDegrees, 0.0f, 0.0f, 1.0f);
    glBegin( GL_LINES );
      glVertex2f( 0, 0);
      glVertex2f( 0, length);
    glEnd();

    glPopMatrix();

}


/*!
    \fn line::calculate()
 */
void line::calculate()
{
    /// @todo implement me
}
