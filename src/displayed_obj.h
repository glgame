#ifndef DISPLAYED_OBJ_H
#define DISPLAYED_OBJ_H

#include "screen.h"
#include "math/coord_t.h"
#include <list>
#include <SDL/SDL_opengl.h>
#include "math/vector2d.h"


/**
  @brief Отображаемый объект

  Виртуальный базовый класс для унаследования всеми отображаемыми объектами

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/

class displayed_obj {

    typedef std::list<displayed_obj*> ObjList;
    static ObjList list_obj; ///< Список всех отображаемых объектов
    ObjList::iterator this_obj; ///< Итератор на данный объект

public:
    virtual void display() = 0; ///< Отображение средствами OpenGL
    displayed_obj();
    virtual ~displayed_obj();

    static void display_all(); ///< Отобразить всех "детей", но не "внуков"

    bool visible; ///< Флаг видимости объекта

};

#endif
