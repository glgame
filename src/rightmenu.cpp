#include "rightmenu.h"

RightMenu::RightMenu()
 : Window()
{

    pos = size = Screen::getGlScreenSize();

    size.val.y *= -1; // вектор getGlScreenSize() направлен снизу вверх, а нам для окна нужно наоборот

    pos.val.x *= 0.85; // 15% экрана отдадим, 85% оставим
    pos *= 0.5; // половина экрана

    size.val.x *= 0.15; // 15% экрана займёт меню

    Vector2D buttsize = Vector2D ( 0.4, - 0.4);

    MenuObjButton* buttobj;
      buttobj = new MenuObjButton();
      buttobj->setParent ( this );
      buttobj->setPos( Vector2D ( 0.1, -0.05 ) );
      buttobj->setSize( buttsize );

    MenuButtonTrigger* tbutton;
    Scalar down = -0.5;
    for ( int i = 0; i < 3; i++ ) {

        tbutton = new MenuButtonTrigger();
        tbutton->setParent ( this );
        tbutton->setPos ( Vector2D ( 0.1, down ) );
        tbutton->setSize ( buttsize );

        down -= 0.5;

    }

}


RightMenu::~RightMenu()
{
}


