#include "options.h"

int Options::optCount = 0;
Options::OptionUnit* Options::availOptions = NULL;

Options::Options() {
	if (Options::availOptions == NULL){
		Init();
	}
}

void Options::Init() {
	OptionUnit defOptions[] = {
		{"debug",         DEBUG,         BOOLEAN_, "For debugging purposes",     "false"},
		{"debug2",        DEBUG,         BOOLEAN_, "For debugging purposes - 2", "false"},
		{"paused",        PAUSED,        BOOLEAN_, "Game paused",                "false"},
		{"fullscreen",    FULLSCREEN,    BOOLEAN_, "run in fullscreen mode",     "false"},
		{"window-size-x", WINDOW_SIZE_X, INT_,     "window size x",              "800"},
		{"window-size-y", WINDOW_SIZE_Y, INT_,     "window size y",              "600"},
		{"show-grid",     SHOW_GRID,     BOOLEAN_, "show grid",                  "true"},
		{"grid-step",     GRID_STEP,     FLOAT_,   "grid step",                  "0.05"},
		{"maximum-fps",   MAX_FPS,       INT_,     "maximum fps",                "40"},
		{"debug",         NOT_DEFINED,   BOOLEAN_, "For debugging purposes",     "false"},
	};
	optCount = sizeof(defOptions) / sizeof(OptionUnit);
	availOptions = new OptionUnit[ optCount ];
	for (int i = 0; i < optCount; i++){
		availOptions[i] = defOptions[i];
	}

}

void	Options::loadFromFile(std::string fileName)
{
	if (Options::availOptions == NULL){
		Init();
	}

	using namespace std;
    ifstream file;

    file.exceptions(ios_base::badbit);
    file.open(fileName.c_str(), ios::in);
    if (!file.good())
      throw ios_base::failure("Couldn't open config file " + fileName);

    // Заполняем массив опций из файла
    string name, value;

    while(!file.eof()){
      file >> name >> value;

	  for(int i = 0; i < optCount; i++) {

          if(Options::availOptions[i].nameString == name) {
                Options::availOptions[i].value = value;
                break;
          }
      }
    }

    file.close();
    file.clear();

}


std::string Options::getValue(std::string name)
{
	for(int i=0; i < optCount; i++)
		if(Options::availOptions[i].nameString == name)
			return Options::availOptions[i].value;
    throw std::runtime_error(std::string("Option " + name + " not found").c_str());
}

std::string Options::getValue(OPT_NAME nameEnum)
{
	for(int i=0; i < optCount; i++)
        if(Options::availOptions[i].nameEnum == nameEnum)
            return Options::availOptions[i].value;
    throw std::runtime_error("Option not found");
}


bool Options::getBoolValue(OPT_NAME nameEnum)
{
   return getValue(nameEnum) == "true" ? true : false;
}

int Options::getIntValue(OPT_NAME nameEnum)
{
  int i;
  std::stringstream s(getValue(nameEnum));
  s >> i;
  return i;
}

float Options::getFloatValue(OPT_NAME nameEnum)
{
  float f;
  std::stringstream s(getValue(nameEnum));
  s >> f;
  return f;
}


void Options::setValue(OPT_NAME nameEnum, std::string value)
{
    for(int i=0; i < optCount; i++)
        if(Options::availOptions[i].nameEnum == nameEnum) {
            Options::availOptions[i].value = value;
            return;
        }
    throw std::runtime_error("Option not found");
}

void Options::saveOptions(std::string fileName)
{
    using namespace std;
    std::ofstream file;

//    file.exceptions(ios_base::badbit|ios_base::failbit);
    file.open(fileName.c_str(), ios::out|ios::trunc);
    if (!file.good())
      throw ios_base::failure( string("Couldn't open config file for write: " + fileName).c_str() );

//    file << config_filename << endl;
	for(int i=0; i < optCount; i++) {

      file << Options::availOptions[i].nameString
           << " "
           << Options::availOptions[i].value
           << endl;

    }
//    throw std::runtime_error("Option not found");
    file.close();
}

