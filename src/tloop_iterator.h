#ifndef TLOOP_ITERATOR_H_
#define TLOOP_ITERATOR_H_

#include "debug.h"


/**
  @brief Круговой итератор.
  
  При инкременте автоматически перепрыгивает с конечного значения на начальное, при декременте - с начального на конечное. Возвращает end() только когда контейнер пуст. В остальных случаях ведёт себя как стандартный stl-итератор.

  @struct den::loop_iterator
  @author Феклушкин Денис <edu2005-60@mail.ru>
*/

namespace den {

template<typename T>
struct loop_iterator : public T::iterator
{
  /// Адрес контейнера для поиска его begin() и end()
  T* container_ptr;

  /// Конструктор
  /// @param container Ссылка на контейнер
  loop_iterator( T& container )
      : T::iterator()
  {
    container_ptr = &container;
  }

  /// Конструктор копирования
  loop_iterator( const loop_iterator& src )
      : T::iterator( src )
  {
    container_ptr = src.container_ptr;
  }

  /// "конструктор копирования" стандартного stl-итератора
  /// @param container Ссылка на контейнер, на элемент которого указывает итератор
  /// @param src Копируемый итератор
  loop_iterator(  T& container, const typename T::iterator& src )
      : T::iterator( src )
  {
    container_ptr = &container;
  }


  // операторы:

  /// копирование stl-итератора
  loop_iterator& operator=( const typename T::iterator& src )
  {
    assert( container_ptr );

    typename T::iterator* this_it = this;

    if( this_it == &src ) return *this; // присваивание самому себе
    *this_it = src;
    return *this;
  }

  /// копирование loop_iterator
  loop_iterator& operator=( const loop_iterator& src )
  {
    if( this == &src ) return *this; // присваивание самому себе
    *this = src;
    return *this;
  }

  /// ++It
  loop_iterator& operator++()
  {
    assert( container_ptr );

    typename T::iterator begin = container_ptr->begin();
    typename T::iterator end = container_ptr->end();
    typename T::iterator tmp;

    if( begin == end ) tmp = end; // список нулевого размера
    else {
      tmp = *this;
      assert( tmp != end );
      ++tmp;
      if( tmp == end ) tmp = begin;
    }

    *this = tmp;
    return *this;
  }

  /// --It
  loop_iterator& operator--()
  {
    assert( container_ptr );

    typename T::iterator begin = container_ptr->begin();
    typename T::iterator end = container_ptr->end();
    typename T::iterator tmp;

    if( begin == end ) tmp = end; // список нулевого размера
    else {
      tmp = *this;
      assert( tmp != end );
      if( tmp == begin ) tmp = end;
      --tmp;
    }

    *this = tmp;
    return *this;
  }

  /// It++
  loop_iterator operator++(int)
  {
    assert( container_ptr );

    loop_iterator res = *this;
    ++(*this);
    return res;
  }

  /// It--
  loop_iterator& operator--(int)
  {
    assert( container_ptr );

    loop_iterator res = *this;
    --(*this);
    return res;
  }

};

}

#endif
