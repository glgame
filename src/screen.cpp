#include "screen.h"

extern Options options;

Angle Screen::gl_fov = 45.0;
int   Screen::pix_width  = 0;
int   Screen::pix_height = 0;
Coord Screen::gl_width  = 10.0;
Coord Screen::gl_height = 10.0;
Scalar Screen::gl_depth = -25.5;

Coord Screen::view_coord_x = 0;
Coord Screen::view_coord_y = 0;

bool  Screen::recreate_grid = true;

Coord Screen::target_coord_x = 0;
Coord Screen::target_coord_y = 0;

ScreenGrid Screen::screen_grid;

Screen::Screen()
{

  pix_width  = options.getIntValue(Options::WINDOW_SIZE_X);
  pix_height = options.getIntValue(Options::WINDOW_SIZE_Y);

  Init();

  screen_grid.GlScreenSize = getGlScreenSize();
  screen_grid.gl_depth = gl_depth;
  screen_grid.Init();

}


Screen::~Screen()
{
}

void Screen::SetScreenBoxOffset(Coord x, Coord y)
{
// Сделаем "тормозную" камеру
// Это у нас будут координаты точки к которой камера будет постоянно стремиться
    target_coord_x += x;
    target_coord_y += y;

}

void Screen::Refresh()
{

    // коэффициент "тормознутости" камеры
    const static Coord lazy_factor = 3.0f;

    view_coord_x = target_coord_x;
    view_coord_y = target_coord_y;

    // Begin new frame
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -gl_depth);

   // Show grid
    screen_grid.GlScreenSize = getGlScreenSize();
    screen_grid.display( getCurrentViewCenter() );

//   SDL_GL_SwapBuffers();

}


/*!
    \fn Screen::Init()
 */
void Screen::Init()
{

// SDL screen init

  SDL_Surface *screen = 0;
  SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

  screen = SDL_SetVideoMode(
        options.getIntValue( Options::WINDOW_SIZE_X ),
        options.getIntValue( Options::WINDOW_SIZE_Y ),
        24, SDL_RESIZABLE | SDL_OPENGL );

  SDL_VideoInfo const *video_info = SDL_GetVideoInfo();

  if ( options.getBoolValue( Options::FULLSCREEN ) ) {

    screen = SDL_SetVideoMode(
            0, // FIXME кажется, для винды тут при FULLSCREEN надо проставлять размеры экрана
          0,
          0, SDL_OPENGL | SDL_FULLSCREEN );

  }

  if(!screen) std::cout << "Unable to set video: " << SDL_GetError() << std::endl; // TODO more error handling



// OpenGL init

  Resized( options.getIntValue( Options::WINDOW_SIZE_X ), options.getIntValue( Options::WINDOW_SIZE_Y ) );
  glClearColor( 0.0, 0.0, 0.0, 0.0 );


  // включим проверку глубины
  glClearDepth( 1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  // уберём всё что рисуется по часовой стрелке (исчезнут задние плоскости)
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

// End initialisation

}


/*!
    \fn Screen::getCurrentViewedBox()
 */
Box_t* Screen::getCurrentViewedBox(Box_t* viewed)
{
//    Box_t* v = (Box_t*) malloc( sizeof (Box_t) );
//    if(v == NULL) throw std::runtime_error("getCurrentViewedBox(): Out of memory");
    viewed->bottom_left.x = view_coord_x - gl_width/2;
    viewed->bottom_left.y = view_coord_y - gl_height/2;
    viewed->upper_right.x = view_coord_x + gl_width/2;
    viewed->upper_right.y = view_coord_y + gl_height/2;
    return viewed;
}


/*!
    \fn Screen::getCurrentViewCenter()
 */
point_t Screen::getCurrentViewCenter()
{

    point_t view;
    view.x = view_coord_x;
    view.y = view_coord_y;
    return view;

}


Vector2D Screen::getCurrentViewCenterVector()
{

    Vector2D view;
    view.val.x = view_coord_x;
    view.val.y = view_coord_y;
    return view;

}


/*!
    \fn Screen::getPixelScreenSize()
 */
Vector2D Screen::getPixelScreenSize()
{

    Vector2D res (  pix_width, pix_height );

    return res;

}


Vector2D Screen::getGlScreenSize()
{

    return getGlScreenSize( gl_depth );

}


Vector2D Screen::getGlScreenSize( Scalar depth ) // FIXME wrong algorhytm
{

    Vector2D res;

    res.val.y = res.val.x = tan( gl_fov * toDegrees ) * depth;

    return res;

}


Vector2D Screen::Pixel2GL(int x, int y)
{

  // x, y              - оконные координаты курсора мыши.

  GLint    viewport[4];    // параметры viewport-a.
  GLdouble mvmatrix[16];   // видовая матрица.
  GLdouble projmatrix[16]; // матрица проекции.

  GLdouble vx,vy,vz;       // координаты курсора мыши в системе координат viewport-a.
  GLdouble wx,wy,wz;       // возвращаемые мировые координаты.

  glGetIntegerv(GL_VIEWPORT,viewport);           // узнаём параметры viewport-a.
  glGetDoublev(GL_MODELVIEW_MATRIX,mvmatrix);    // узнаём видовую матрицу.
  glGetDoublev(GL_PROJECTION_MATRIX,projmatrix); // узнаём матрицу проекции.


  // переводим оконные координаты курсора в систему координат viewport-a.
  /*  note viewport[3] is height of window in pixels  */
  GLdouble realy = viewport[3] - (GLint) y - 1;

  GLuint err;

  err = gluUnProject ((GLdouble) x, (GLdouble) realy, -1,
               mvmatrix, projmatrix, viewport, &wx, &wy, &wz);

  if ( err == GL_FALSE ) throw std::runtime_error("gluUnProject() = GL_FALSE");

  Vector2D p1(wx,wy);

  return p1;

}


/*!
    \fn Screen::Resized()
 */
void Screen::Resized(int w, int h)
{

//FIXME надо знать реальное разрешение экрана, система считает что по Y первоначальный размер окна не меняется. возможно это бага в SDL. сейчас тут грязный хак.

  glViewport( 0, pix_height - h - 1, w, h );
  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();

  GLfloat aspect = 1;

  // узнаем глубину по углу обзора и ширине
  gl_depth = (gl_width/2) / ( tan (gl_fov/2));

  glOrtho( -gl_width/2, gl_width/2, -gl_height/2, gl_height/2, gl_depth, 0);
//  gluPerspective( gl_fov, aspect, 0.0, 1.0 );
  glMatrixMode( GL_MODELVIEW );
  glLoadIdentity();

}


/*!
    \fn Screen::ShowGrid()
 */
void Screen::ShowGrid()
{
    /// @todo implement me
}


/*!
    \fn Screen::CreateGrid()
 */
void Screen::CreateGrid()
{

}


/*!
    \fn Screen::getDepth()
 */
Scalar Screen::getDepth()
{
    return gl_depth;
}


/*!
    \fn Screen::setDepth(Scalar depth)
 */
void Screen::setDepth(Scalar depth)
{
    gl_depth = depth;
}


/*!
    \fn Screen::getZoom()
 */
Scalar Screen::getZoom()
{
	return 0;
    /// @todo implement me
}
