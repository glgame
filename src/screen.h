#ifndef SCREEN_H
#define SCREEN_H

#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <GL/glu.h>

#include "options.h"
#include "math/coord_t.h"
#include "displayed_obj.h"
#include "math/fast_math.h"
#include "menu/window.h"
#include "screengrid.h"
#include "math/vector2d.h"

/**
	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class Screen{
    static Coord  gl_width;
    static Coord  gl_height;
    static int    pix_width;
    static int    pix_height;
    static Angle  gl_fov;
    static Scalar gl_depth;

    static Coord view_coord_x;
    static Coord view_coord_y;
    static bool recreate_grid;

    static Coord target_coord_x;
    static Coord target_coord_y;

public:
    static ScreenGrid screen_grid;

    Screen();

    virtual ~Screen();
    static void SetScreenBoxOffset(Coord x, Coord y);
    static void Refresh();
    static void Init();
    static Box_t* getCurrentViewedBox(Box_t* viewed);
    static point_t getCurrentViewCenter();
    static Vector2D getCurrentViewCenterVector();
    static Vector2D getPixelScreenSize();
    static Vector2D getGlScreenSize( Scalar depth );
    static Vector2D getGlScreenSize();
    static Vector2D Pixel2GL(int x, int y);
    static void Resized(int w, int h);
    static void setDepth(Scalar depth);
    static Scalar getDepth();
    static Scalar getZoom();

private:
    static void CreateGrid();
private:
    static void ShowGrid();
};

#endif
