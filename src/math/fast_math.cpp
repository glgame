#include "fast_math.h"

#ifdef _MSC_VER
namespace std{
	int isnan(double x)
	{
		return (_isnan(x));
	}

	int isinf(double x)
	{
		return (!_finite(x));
	}
}
#endif //_MSC_VER

//const double fast_math::pi = atan2( 1.0, 1.0) * 4.0; // TODO от точности числа пи сильно зависит вывод изображения - странно! пока оставим как есть но надо разобраться
const double fast_math::pi = 3.141592653;
const double fast_math::pi2 = fast_math::pi * 2.0;

fast_math::fast_math(double precision)
{
  fast_math::precision = precision;

  array_count = (int) ceil ( pi / precision ) + 1;
  array = (double*) malloc( (size_t) ( array_count * sizeof( double ) ) );

  if ( !array ) throw std::runtime_error("fast_math(): Out of memory");

//   TRACE(std::cout << "Created math array size: " << array_count * sizeof( double ) << " bytes" << std::endl; )

  // заполяем массив значениями sin(x), step precision
  // хранить будем значения до угла 90 градусов
  double* tmp = array;
  double i;
  for(i = 0; i <= pi; i += precision) {
//          PR(i);
          *tmp = std::sin(i);
//          PR(*tmp);
          tmp++;
  }
}


fast_math::~fast_math()
{
  free( (void*)array );
  array = NULL;
}


inline double fast_math::sinInside(double angle)
{

   return *(array + (int) ( angle / precision + 0.5 ));
}

/*!
    \fn fast_math::sin(double angle)
 */
inline double fast_math::sin(double angle)
{

   // убираем лишние "обороты" вокруг "окружности"
   if( angle >= pi )
         angle -= pi * static_cast<double>( static_cast<int>( angle / pi ) );

   if( angle <= 0 - pi )
         angle += pi * static_cast<double>( static_cast<int>( - (angle / pi) ) );

   assert(angle >=0);
   assert(angle <= pi);

   double* tmp = array; //    precision
   tmp+= (int) ( angle/precision );
   return *tmp;
}

double fast_math::cos(double angle)
{
  return fast_math::sin(angle + pi/2);
}


Length projection_on_axis ( Coord x1, Coord x2 ) {
  return - ( x1 - x2  );
}

Length length_between_points (point_t p1, point_t p2) {
   Length projection_x = fabs( projection_on_axis ( p1.x, p2.x ) );
   Length projection_y = fabs( projection_on_axis ( p1.y, p2.y ) );

   Length length = sqrt ( pow ( projection_x, 2 ) +
                   pow ( projection_y, 2 )
            );
  return length;
}

Angle  angle_x_between_points (point_t p1, point_t p2) {
  Length projection_x = projection_on_axis (p1.x, p2.x);
  Length projection_y = projection_on_axis (p1.y, p2.y);
  return atan2( projection_y, projection_x );
}
