#ifndef COORD_T_H
#define COORD_T_H

#include <SDL/SDL_opengl.h>


/**
@file
@brief В этом файле задаются разные структуры. Совсем разные!

TODO: Название файла уже слабо отражает суть содержимого.
*/

typedef double Coord;
typedef double Angle;
typedef double Length;
typedef double Mass;
typedef double Strength;
typedef double Speed;
typedef double Velocity;

/// Точка в 2D пространстве
typedef struct point_t {
    Coord x;
    Coord y;
} point_t;

/// Ящик, заданный нижней_левой и верхней_правой точками (TODO: избавиться от него, не нужен)
typedef struct Box_t {
  point_t bottom_left;
  point_t upper_right;
} Box_t;

/// Цвет для удобства работы с OpenGL
typedef struct Color {
  GLubyte  red;
  GLubyte  green;
  GLubyte  blue;
} Color;

/// Цвет с альфаканалом для удобства работы с OpenGL
typedef struct ColorA : Color {
  GLubyte  alpha;
} ColorA;

#endif
