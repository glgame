#ifndef VECTOR2D_H
#define VECTOR2D_H

#include <iostream>
#include <cmath>
#include <SDL/SDL_opengl.h>
#include <string>
#include <iostream>
#include <sstream>

#include "coord_t.h"
#include "fast_math.h"


/**
  @brief Вектор для 2D векторных вычислений

  @class Vector2D
  @author Феклушкин Денис <edu2005-60@mail.ru>
*/

typedef double Scalar; ///< переменная с плавающей запятой, скалярное значение

/// для перевода радианов в градусы умножте их на эту переменную
const double toDegrees = fast_math::pi2 / 360;

class Vector2D{
public:
    /// Положение точки относительно вектора
    enum point_on {
      NAN_VAL,  ///< Попытка работать с несуществующими точкой или вектором
      LEFT,     ///< Слева по направлению вектора
      RIGHT,    ///< Справа по направлению вектора
      BACK,     ///< Сзади по направлению вектора на прямой
      FORWARD,  ///< Впереди по направлению вектора на прямой
      BETWEEN,  ///< Между началом и концом вектора
      START,    ///< Совпадает с точкой начала вектора
      END,      ///< Совпадает с точкой конца вектора
      STRAIGHT, ///< На прямой, совпадающей с вектором
      PARALLEL  ///< Параллельны (для векторов)
    };

    point_t val; ///< координаты вектора
    Vector2D ();
    Vector2D (const Vector2D& src);
    Vector2D ( Coord x, Coord y );
    Vector2D ( point_t coords );
    ~Vector2D();

    /// Скалярное произведение векторов
    static Scalar   ProductScalar(Vector2D first, Vector2D second);

    /// Векторное произведение векторов
    static Scalar   ProductVector(Vector2D first, Vector2D second);

    /// Разделить вектор скалярным значением
    static Scalar   DivScalar (Vector2D first, Vector2D second);

    /// Найти проекцию first на second
    static Vector2D Projection(Vector2D first, Vector2D second);

    /// Найти нормаль (перпендикуляр) к точке
    static Vector2D Normal(Vector2D* vect, Vector2D* point_coord);

    void SetCoords(point_t coords); ///< Приравнивает вектор к координатам точки
    Length length() const; ///< Найти длину вектора
    Vector2D ort(); ///< Возвращает вектор того же направления, длиной 1
    void Reset(); ///< Сброс вектора на ноль
    Vector2D NormalToPoint(Vector2D* point_coords); ///< Найти нормаль (перпендикуляр) к точке
    bool IsParallel(Vector2D* vector); ///< Вектор аргумента параллелен данному?
    bool IsCollinear(Vector2D* vector); ///< Вектор параллелен и направлен в ту же сторону, что и данный?

    /// Вектор равен нолю?
    bool IsNull() const;
    /// Вектор равен нолю?
    bool IsZero() const { return IsNull(); }

    /// Ищет точку пересечения векторов (внимание: работает и на обратных углах)
    Vector2D CrossingPoint(Vector2D* other_vector, Vector2D* his_coords);

    Vector2D& operator+();
    Vector2D operator-();

    Vector2D operator*(const Scalar& scalar);
    Vector2D operator/(const Scalar& scalar);
    Vector2D operator+(const Vector2D& right);
    Vector2D operator-(const Vector2D& right);
    Angle    operator^(const Vector2D& right); ///< Угол между векторами
    bool     operator==(const Vector2D& right);
    bool     operator!=(const Vector2D& right);

    void StdoutValues(); ///< Выводит на консоль значение вектора в читаемом виде
    std::string getStringValues() const; ///< Вернуть значение вектора в читаемом виде

    /// Вернуть вектор с координатами данного, взятыми положительными
    Vector2D getPositive();

    /// Дополняет габариты вектора к переданным габаритам
    static void MergeVector(Vector2D& vec, Vector2D* l_d, Vector2D* r_u);

    /// В один ли сектор тригонометрической окружности смотрят векторы?
    bool SectorCompare( Vector2D* vec );

    /// Имеют ли векторы точку пересечения?
    bool VectorsIsCrossing( Vector2D* vec, Vector2D* coord, Vector2D* result_point );

    /// Пересекаются ли векторы?
    bool VectorsIsCrossing( Vector2D* vec, Vector2D* coord );

    /// Кратчайшее направление от данного вектора к точке
    Vector2D ShortestDirectionToPoint( Vector2D* point );

    /// Кратчайшее направление к точке с сохранением начальной точки направления, принадлежащей вектору
    Vector2D ShortestDirectionToPoint( Vector2D* point, Vector2D* vector_nearest_point );

    /// Показывает средствами OpenGL текущий вектор
    void DisplayDebug( Vector2D& coord, bool show_x_y );

    /// Положение точки относительно вектора
    point_on Classify( Vector2D& point );

    /// Положение точки относительно вектора упрощённо - лево, право и на прямой
    point_on ClassifySimple( Vector2D& point );

    /// Принадлежит ли точка вектору?
    bool ClassifyBelong( Vector2D& point );

    /// Повернуть данный вектор на угол
    void Rotate( Scalar angle );

    /// Классификация точки пересечения векторов
    point_on ClassifyCrossing( Vector2D& vec, Vector2D& coord, Vector2D& res_point );

    inline Scalar GetK(); ///< Вернуть коэффициент k из формулы прямой y = k*x + b;
    bool IsNaN(); ///< Вектор неопределён?
    void SetNaN(); ///< Установить в состояние неопределённости
    Scalar AngleOX() const; ///< угол между вектором и осью OX
    Vector2D Rotated( Scalar angle ); ///< получить вектор, повёрнутый на угол

};

Vector2D operator*(const Scalar& scalar, const Vector2D& right);
Scalar   operator*(const Vector2D& left, const Vector2D& right);
Scalar   operator/(const Vector2D& left, const Vector2D& right);
Vector2D& operator+=(Vector2D& left, const Vector2D& right);
Vector2D& operator-=(Vector2D& left, const Vector2D& right);
Vector2D& operator*=(Vector2D& left, const Scalar& right);
Vector2D& operator/=(Vector2D& left, const Scalar& right);
Scalar operator+( const Scalar& left, const Vector2D& right );
Scalar operator-( const Scalar& left, const Vector2D& right );
std::ostream& operator<<( std::ostream& ostr, const Vector2D& vec );

#endif
