#ifndef FAST_MATH_H
#define FAST_MATH_H

#include <cmath>
#include <float.h>
#include <stdexcept>
#include "coord_t.h"
#include "../debug.h"


/**
@file
@brief Математическая библиотека

Тут мы считаем sin() и cos() согласно таблице Брадиса.

Задумка сделать таблицу синусов не оправдалась: на самом деле
получается неточно и/или медленно, MMX работает быстрее.

Лишь некоторые функции файла используются в данный момент.

@author Феклушкин Денис <edu2005-60@mail.ru>
*/

#ifdef _MSC_VER
namespace std{
	int isnan(double x);
	int isinf(double x);
}
#endif //_MSC_VER

class fast_math{
    double* array;
    double precision;
    int array_count;
    double sinInside(double angle);
public:
    static const double pi; /// Число Пи
    static const double pi2; /// 2 * Пи
    fast_math(double precision);
    ~fast_math();
    double sin(double angle);
    double cos(double angle);
};

Length projection_on_axis ( Coord x1, Coord x2 );
Length length_between_points (point_t p1, point_t p2);
Angle  angle_x_between_points (point_t p1, point_t p2);

#endif
