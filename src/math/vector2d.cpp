#include "vector2d.h"

Vector2D::Vector2D()
{
  Reset();
}


Vector2D::Vector2D(Coord x, Coord y){
  val.x = x;
  val.y = y;
}


Vector2D::Vector2D(point_t coords)
{
  SetCoords(coords);
}


Vector2D::~Vector2D()
{
}


Vector2D::Vector2D (const Vector2D& src)
{
  val = src.val;
}


Scalar Vector2D::ProductVector(Vector2D first, Vector2D second)
{
    return first.length() * second.length() * sin ( first ^ second );
}


Scalar Vector2D::DivScalar (Vector2D first, Vector2D second)
{

    Scalar second_length = second.length();
    assert( second_length != 0 );

    return ( first.length() / second_length ) * cos ( first ^ second );

}


Scalar Vector2D::ProductScalar(Vector2D first, Vector2D second)
{
    return first.val.x * second.val.x + first.val.y * second.val.y;
}


/*!
    \fn Vector2D::SetCoords(point_t coords)
 */
void Vector2D::SetCoords(point_t coords)
{
  val = coords;
}


Length Vector2D::length() const
{
     return sqrt( pow(val.x, 2) + pow(val.y, 2) );
}


void Vector2D::Reset()
{
  val.x = 0;
  val.y = 0;
}


Vector2D Vector2D::ort()
{
     Vector2D res = *this;

     Scalar l = length();

     if( !l ) return res;
     return res /= l;
}


Vector2D Vector2D::NormalToPoint(Vector2D* point_coords)
{

    return Vector2D::Normal( this, point_coords );

}

Vector2D& Vector2D::operator+() {
  return *this;
}

Vector2D Vector2D::operator-() {
  Vector2D res = *this * -1;
  return res;
}


Vector2D Vector2D::operator*(const double& scalar) {
  Vector2D res = *this;
  res.val.x *= scalar;
  res.val.y *= scalar;
  return res;
}

Vector2D Vector2D::operator/(const double& scalar) {
  Vector2D res = *this;
  res.val.x /= scalar;
  res.val.y /= scalar;
  return res;
}

Vector2D Vector2D::operator+(const Vector2D& right) {
  Vector2D res = *this;
  res += right;
  return res;
}

Vector2D Vector2D::operator-(const Vector2D& right) {
  Vector2D res = *this;
  res -= right;
  return res;
}

// Angle between vectors is ^:
Angle    Vector2D::operator^(const Vector2D& right) {
  return AngleOX() - right.AngleOX();
//   Vector2D::AngleBetweenVectors(*this, right);
}

bool     Vector2D::operator==(const Vector2D& right) {
  return ( val.x == right.val.x ) && ( val.y == right.val.y );
}

bool     Vector2D::operator!=(const Vector2D& right) {
  return !( *this == right );
}

// binary operators:
Vector2D operator*(const Scalar& scalar, const Vector2D& right)
{
  Vector2D res = right;
  res *= scalar;
  return res;
}

Scalar   operator*(const Vector2D& left, const Vector2D& right)
{
  return Vector2D::ProductScalar(left, right);
}

Scalar   operator/(const Vector2D& left, const Vector2D& right)
{
  return Vector2D::DivScalar(left, right);
}

Vector2D& operator+=(Vector2D& left, const Vector2D& right) {
  left.val.x += right.val.x;
  left.val.y += right.val.y;
  return left;
}

Vector2D& operator-=(Vector2D& left, const Vector2D& right) {
  left.val.x -= right.val.x;
  left.val.y -= right.val.y;
  return left;
}

Vector2D& operator*=(Vector2D& left, const Scalar& right) {
  left.val.x *= right;
  left.val.y *= right;
  return left;
}

Vector2D& operator/=(Vector2D& left, const Scalar& right) {
  left.val.x /= right;
  left.val.y /= right;
  return left;
}

Scalar operator+(const Scalar& left, const Vector2D& right) {
   Vector2D vect = right;
   return left + vect.length();
}

Scalar operator-(const Scalar& left, const Vector2D& right) {
   Vector2D vect = right;
   return left - vect.length();
}

Vector2D Vector2D::Projection(Vector2D first, Vector2D second)
{
  Scalar second_len = second.length();
  if ( second_len == 0 ) return second;

  Scalar length = ProductScalar( first, second ) / second_len;
  Vector2D res = ( second / second_len ) * length ;
  return res;
}


Vector2D Vector2D::Normal(Vector2D* vect, Vector2D* point_coord)
{

  return *point_coord - Vector2D::Projection( *point_coord, *vect );

}


bool Vector2D::IsParallel(Vector2D* vector) {

  Scalar k1 = GetK();
  Scalar k2 = vector->GetK();

  // C99 isinf() не умеет говорить минус-бесконечность или плюс ей дали,
  // поэтому на всякий случай берём модуль:
  if( std::isinf( k1 ) ) k1 = fabs( k1 );
  if( std::isinf( k2 ) ) k1 = fabs( k2 );

  return ( k1 == k2 ) ? true : false;

}


bool Vector2D::IsCollinear(Vector2D* vector) {

  return ( GetK() == vector->GetK() ) ? true : false;

}


bool Vector2D::IsNull() const {
  return ( val.x || val.y ) ? false : true;
}


Vector2D Vector2D::CrossingPoint(Vector2D* vector, Vector2D* coords){

  Vector2D r_norm = -NormalToPoint( coords );
  Scalar angle = r_norm ^ *vector;
  Scalar gipotenuza = r_norm.length() / cos(angle);

  return *coords + vector->ort() * gipotenuza;

}


void Vector2D::StdoutValues()
{
#ifndef NDEBUG
    std::cout << "Vector " << *this << std::endl;
#endif
}


std::string Vector2D::getStringValues() const
{
    std::stringstream res;
    res << "x=" << val.x << " y=" << val.y;
    return res.str();
}


Vector2D Vector2D::getPositive()
{
    Vector2D res( val.x, val.y );
    if (res.val.x < 0 ) res.val.x *= -1;
    if (res.val.y < 0 ) res.val.y *= -1;
    return res;
}


/**
    "Раздвигает" границы воображаемого многоугольника, координаты двух противоположных углов которого заданы векторами l_d и r_u (left down и right up), чтобы в него поместился вектор vec. Таким образом собирается габаритный "ящик" сложных объектов.

    Результат остаётся в l_d и r_u.
    @param vec добавляемый в многоугольник вектор
    @param l_d левая нижняя точка габаритного "ящика"
    @param r_u правая верхняя точка габаритного "ящика"
*/
void Vector2D::MergeVector(Vector2D& vec, Vector2D* l_d, Vector2D* r_u)
{
    assert( l_d );
    assert( r_u );

    if( vec.val.x < l_d->val.x ) l_d->val.x = vec.val.x;
    if( vec.val.y < l_d->val.y ) l_d->val.y = vec.val.y;

    if( vec.val.x > r_u->val.x ) r_u->val.x = vec.val.x;
    if( vec.val.y > r_u->val.y ) r_u->val.y = vec.val.y;

}


/// сравнивает знак a и b
/// @return если знак совпадает true, иначе false
bool SignCompare( Scalar a, Scalar b ) {

    bool s_a = ( a < 0 ) ? false : true;
    bool s_b = ( b < 0 ) ? false : true;

    return ( s_a == s_b ) ? true : false;

}


bool Vector2D::SectorCompare( Vector2D* vec )
{

  if( SignCompare( vec->val.x, val.x ) &&
      SignCompare( vec->val.y, val.y ) ) return true;
  else  return false;

}


bool Vector2D::VectorsIsCrossing( Vector2D* vec, Vector2D* coord, Vector2D* result_point )
{

  *result_point = CrossingPoint( vec, coord );

  // проверим, в один ли сектор "смотрят" вектор и точка пересечения на нём
  // или может точка упала на начало вектора?
  if( result_point->IsNull() || SectorCompare( result_point ) ) {

      // точка пересечения попадает в длину вектора?
      // если да то она точно принадлежит вектору
      if ( length() > result_point->length() ) {

          // и ещё проверим чтобы концы vect лежали по разные
          // стороны от нашего вектора. для этого сравним углы
          // между концами vec и нашим вектором - они должны быть
          // с разным знаком
          Scalar a1 = *coord ^ *this;
          Scalar a2 = (*coord + *vec) ^ *this;
          if( !SignCompare( a1, a2 ) ) return true;
      }
  }

  return false;
}


bool Vector2D::VectorsIsCrossing( Vector2D* vec, Vector2D* coord )
{

    Vector2D unused;
    return VectorsIsCrossing( vec, coord );

}

Vector2D Vector2D::ShortestDirectionToPoint( Vector2D* point )
{
    Vector2D unused;
    return ShortestDirectionToPoint( point, &unused );
}


Vector2D Vector2D::ShortestDirectionToPoint( Vector2D* point, Vector2D* vector_nearest_point )
{

  assert( vector_nearest_point );

  // проекция из точки на вектор
  Vector2D proj = Vector2D::Projection(*point, *this);

  // смотрит ли проекция в ту же сторону?
  if( !SectorCompare( &proj ) ) {

      // точкой начала направления является начало вектора
      vector_nearest_point->Reset();

      // тогда возвращаем расстояние от начала вектора до точки
      return  *point;

  }

  // ушла ли проекция дальше вектора?
  if( proj.length() >= length() ) {

      // точкой отсчёта начала направления является конец вектора
      *vector_nearest_point = *this;

      // возвращаем расстояние от конца вектора до точки
      return *point - *this;

  } else {

      // значит точка начала отсчёта направления это проекция на вектор
      *vector_nearest_point = proj;

      // остаётся вернуть нормаль к точке
      return  *point - proj;
  }

}



void Vector2D::DisplayDebug( Vector2D& coord, bool show_x_y )
{
#ifndef NDEBUG

    glPushMatrix();
    glLoadIdentity();
    glTranslatef ( coord.val.x, coord.val.y, 0.0f);

    if( show_x_y ) {

      // x axis:
      glBegin( GL_LINES );

        glColor3f(0.9f, 0.2f, 0.2f);
        glVertex2f( 0.0f, 0.0f );

        glColor3f(0.3f, 0.0f, 0.0f);
        glVertex2f( val.x, 0.0f );

      glEnd();

      // y axis:
      glBegin( GL_LINES );

        glColor3f(0.2f, 0.9f, 0.2f);
        glVertex2f( 0.0f, 0.0f );

        glColor3f(0.0f, 0.3f, 0.0f);
        glVertex2f( 0.0f, val.y );

      glEnd();

    }

    // value:
    glBegin( GL_LINES );

      glColor3f(0.7f, 0.7f, 0.7f);
      glVertex2f( 0.0f, 0.0f );

      glColor3f(0.3f, 0.3f, 0.3f);
      glVertex2f( val.x, val.y );

    glEnd();

    glPopMatrix();

#endif
}


Vector2D::point_on Vector2D::Classify( Vector2D& point )
{
    // вектор или точка не определены
    if( IsNaN() ) return NAN_VAL;
    if( point.IsNaN() ) return NAN_VAL;

    // слева-справа
    Scalar sa = val.x * point.val.y - point.val.x * val.y;
    if( sa > 0.0 ) return LEFT;
    if( sa < 0.0 ) return RIGHT;

    // позади и впереди
    if ((val.x * point.val.x < 0.0) || (val.y * point.val.y < 0.0)) return BACK;
    if ( length() < point.length() ) return FORWARD;

    // совпадение с началом и концом
    if( point.IsNull() ) return START;
    if( *this == point ) return END;

    // принадлежит вектору
    return BETWEEN;

}


Vector2D::point_on Vector2D::ClassifySimple( Vector2D& point )
{
    point_on res = Classify( point );

    // просто уберём лишние варианты в вариант "на прямой"
    switch ( res ) {
      case START:
      case END:
      case FORWARD:
      case BACK:
      case BETWEEN:
        res = STRAIGHT;

      default:;
    }

    return res;
}


bool Vector2D::ClassifyBelong( Vector2D& point )
{
    point_on res = Classify( point );

    switch ( res ) {
      case START:
      case END:
      case BETWEEN:
        return true;

      default:
        return false;
    }
}


void Vector2D::Rotate( Scalar angle )
{

  Scalar len = length();
  Scalar new_angle = atan2( val.x, val.y ) + angle;

  val.x = sin(new_angle) * len;
  val.y = cos(new_angle) * len;

}


Vector2D::point_on Vector2D::ClassifyCrossing( Vector2D& vec, Vector2D& coord, Vector2D& res_point )
{
    // проверка на параллельность
    if( IsParallel( &vec ) ) {
        // оставим точку пересечения несуществующей
        res_point.SetNaN();
        return PARALLEL;
    }

    res_point = CrossingPoint( &vec, &coord );

    if( res_point.IsNull() ) return START;
    if( res_point == *this ) return END;

    // позади и впереди
    if ((val.x * res_point.val.x < 0.0) || (val.y * res_point.val.y < 0.0)) return BACK;
    if ( length() < res_point.length() ) return FORWARD;

    return BETWEEN;
}


Scalar Vector2D::GetK()
{
    Scalar k = val.y / val.x;

    // если вектор нулевой то получится NaN, а нам он не нужен
    assert( !std::isnan( k ) );

    return k;
}


bool Vector2D::IsNaN()
{
    return ( std::isnan( val.x ) || std::isnan( val.y ) ) ? true : false;
}


void Vector2D::SetNaN()
{
        Scalar unused = 0;
        val.x = unused / unused;
        val.y = unused / unused;
}


Vector2D Vector2D::Rotated( Scalar angle )
{
  Scalar len = length();
  Scalar new_angle = AngleOX() + angle;

  return Vector2D( sin(new_angle) * len, cos(new_angle) * len );
}


Scalar Vector2D::AngleOX() const
{
    return atan2( val.x, val.y );
}


std::ostream& operator<<( std::ostream& ostr, const Vector2D& vec )
{
    ostr << &vec << ": " << vec.getStringValues();
    return ostr;
}
