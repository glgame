#ifndef MENUOBJBUTTON_H
#define MENUOBJBUTTON_H

#include "menu/menubuttontrigger.h"
#include "phys/physics_obj.h"

/**
кнопка с нарисованным на ней объектом

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class MenuObjButton : public MenuButtonTrigger
{
public:
    MenuObjButton();
    ~MenuObjButton();

    void display();

    physics_obj* obj;

};

#endif
