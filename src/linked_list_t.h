#ifndef LINKED_LIST_T_H
#define LINKED_LIST_T_H

#include "debug.h"

/**
@brief Шаблон связного списка произвольных объектов

При добавлении этого шаблона к объекту при создании (конструировании) объекта он автоматически будет добавлен в связный список объектов этого типа. При уничтожении объекта он автоматически удаляется из списка.

Синтаксис включения в произвольный класс:
class A :
  public linked_list_t<A>
{
...
}

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/

template <class T>
class linked_list_t{

protected:
    static T *first; ///< Указывает первый элемент списка
    T *prev; ///< Предыдущий элемент списка
    T *next; ///< Следующий элемент списка

    /**
    Выполнить метод во всех элементах списка

    @param (T::*func_ptr)() Указатель на метод объекта.
    Метод должен возвращать void и не иметь параметров.
    
    Синтаксис использования:
    exec4all( &T::some_method );
    */
    static void exec4all( void (T::*func_ptr)() );

public:
    linked_list_t();
    ~linked_list_t();

};


template <class T>
T* linked_list_t<T>::first = 0;


template <class T>
linked_list_t<T>::linked_list_t()
{
     // связываем все объекты в однонаправленный список

      if(!first) {

        // добавляем самый первый элемент
        first = prev = next = static_cast<T*> (this);

      } else {

          // добавляем другие элементы в конец списка
          prev = first->prev;
          next = first;

          first->prev->next = static_cast<T*> (this);
          first->prev = static_cast<T*> (this);

      }

}


template <class T>
linked_list_t<T>::~linked_list_t() {

      // удаляем себя из списка объектов
      if(next == this) {

        first = 0; // этот объект был последний в списке

      } else {

        prev->next = next;
        next->prev = prev;
        if( this == first) first = next;

      }

}


template <class T>
void linked_list_t<T>::exec4all( void (T::*func_ptr)() )
{

          T* curr = T::first;

          if ( curr ) do {

             T* this_obj = static_cast<T*> (curr);
             ( this_obj->*func_ptr )();
              curr = curr->next;

          } while ( curr != T::first );

}


#endif
