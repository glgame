#include "menumanager.h"

MenuManager::MenuManager(std::string name)
 : MenuBaseItem()
{
}


MenuManager::~MenuManager()
{
}




/*!
    \fn MenuManager::display()
 */
void MenuManager::display_all()
{

    glPushMatrix();
    glLoadIdentity();

    // Отключим на время тест глубины и задних плоскостей
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    // покажем всю систему меню
    DisplayWithChilds();

    // Вернём тест глубины и задних плоскостей
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    glPopMatrix();

}


void MenuManager::display(){}
