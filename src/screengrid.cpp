#include "screengrid.h"

ScreenGrid::ScreenGrid()
{

    regenerate = true;

}


ScreenGrid::~ScreenGrid()
{
}



/*!
    \fn ScreenGrid::Init()
 */
void ScreenGrid::Init()
{
extern Options options;
    grid_step = options.getFloatValue( Options::GRID_STEP );
    bold_step   = 10;
    labels_size = 0.05f;

    grid_size = GlScreenSize.val.x / 2 + 2 * (grid_step * bold_step);
}


void ScreenGrid::RegenerateGrid()
{

      // Creating grid

      grid_list = glGenLists(1);
      glNewList(grid_list,GL_COMPILE);

      GLint   bold_step_counter = 0;

      for ( GLfloat x = - grid_size; x <= grid_size; x+= grid_step ) {

        if(bold_step_counter) {

          glColor3f(0.0f, 0.10f, 0.0f);

          glBegin( GL_LINES );
            glVertex2f( x, - grid_size);
            glVertex2f( x,   grid_size);
          glEnd();

          glBegin( GL_LINES );
            glVertex2f( - grid_size, x);
            glVertex2f(   grid_size, x);
          glEnd();

        }

        bold_step_counter++;
        if (bold_step_counter >= bold_step) bold_step_counter = 0;

      }

      for ( GLfloat x = - grid_size;  x <= grid_size; x+= grid_step * bold_step ) {

        glColor3f(0.0,0.3,0.0);

        glBegin( GL_LINES );
          glVertex2f( x, - grid_size);
          glVertex2f( x,   grid_size);
        glEnd();

        glBegin( GL_LINES );
          glVertex2f( - grid_size, x);
          glVertex2f(   grid_size, x);
        glEnd();
      }

      glEndList();
   // End creating grid

}



/*!
    \fn ScreenGrid::display_labels()
 */
void ScreenGrid::display_labels()
{

   GLfloat gl_width  =   GlScreenSize.val.x / 2;
   GLfloat gl_height = - GlScreenSize.val.y / 2;

   GLfloat otstup = 1;

   // Show labels - horisontal
   glColor3f(1.0,0.6,0.0);

   glPushMatrix();
   glLoadIdentity();

   glTranslatef(grid_center_x, -gl_height* otstup, -gl_depth);

   for ( GLfloat x = - grid_size;  x <= grid_size; x+= grid_step * bold_step ) {

        glBegin( GL_TRIANGLES );
          glVertex2f( x,               0.0f);
          glVertex2f( x + labels_size, labels_size);
          glVertex2f( x - labels_size, labels_size);
        glEnd();

   }

   glPopMatrix();

   // Show labels - vertical
   glPushMatrix();
   glLoadIdentity();

   glTranslatef( - gl_width * otstup, grid_center_y, -gl_depth);

   for ( GLfloat y = - grid_size;  y <= grid_size; y+= grid_step * bold_step ) {

        glBegin( GL_TRIANGLES );
          glVertex2f( 0.0f, 0.0f + y);
          glVertex2f( -labels_size, labels_size + y);
          glVertex2f( -labels_size, -labels_size +y);
        glEnd();

   }

    glPopMatrix();

}


void ScreenGrid::display( Vector2D view_coord )
{

    if ( regenerate ) { regenerate = false; RegenerateGrid(); }

    grid_center_x = - fmodf ( (float)view_coord.val.x, fabs(grid_step * bold_step) );
    grid_center_y = - fmodf ( (float)view_coord.val.y, fabs(grid_step * bold_step) );

    glPushMatrix();
//    glLoadIdentity();
    glTranslatef(grid_center_x, grid_center_y, 0.0f);

    // Отключим на время рисования сетки тест глубины и задних плоскостей
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    glCallList(grid_list); // сетка
    display_labels(); // сверху рисуем метки

    // Вернём тест глубины и задних плоскостей
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    glPopMatrix();

}
