#ifndef LINE_H
#define LINE_H

#include "displayed_obj.h"
#include "phys/physics_obj.h"
#include <cmath>
#include "math/fast_math.h"


/**
Объект линия. Для тестирования всего этого барахла  возможно пригодится

	@author Феклушкин Денис <edu2005-60@mail.ru>
*/
class line : public displayed_obj , physics_obj
{
    Coord s_x, s_y, e_x, e_y;
    Angle rotation;
    Length length;
    Length projection_x, projection_y;
    void display();
    void calculate();
public:
//    line(Coord start_x, Coord start_y, Coord end_x, Coord end_y);
    line(Coord start_x, Coord start_y, Angle rot, Length len);

    ~line();

};

#endif
