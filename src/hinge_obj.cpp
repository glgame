#include "hinge_obj.h"

const Scalar hinge_obj::display_size = 0.06f;

void hinge_obj::Init_defaults()
{

  mass = 0.01;
  moment_inertia = 1.0;
  fixed = false;

  // удалять при деструкции не нужно - obj_Body следит за шарнирами тела
  hinge = addBodyHingeFromCenter( Vector2D(0, 0) );
  hinge->visible = false; // скроем шарнир, чтобы крестики не накладывались

}


hinge_obj::hinge_obj(Vector2D coord)
{

  setCoords( &coord );
  Init_defaults();

}


hinge_obj::hinge_obj(point_t point)
{

  Vector2D coords;
  coords.val.x = point.x;
  coords.val.y = point.y;
  setCoords( &coords );
  Init_defaults();

}

hinge_obj::hinge_obj(Coord x, Coord y)
{

  Vector2D coords;
  coords.val.x = x;
  coords.val.y = y;
  setCoords( &coords );
  Init_defaults();

}


hinge_obj::~hinge_obj()
{
}

void hinge_obj::display()
{

//    force_point_obj::ShowDebugVector();

    glColor3f(1.0,1.0,1.0);

    glBegin( GL_LINES );
      glVertex2f( -display_size, -display_size);
      glVertex2f(  display_size,  display_size);

      glVertex2f( -display_size,  display_size);
      glVertex2f(  display_size, -display_size);
    glEnd();

}

void hinge_obj::physics_forces_calculate()
{

    // обычный незафиксированный движущийся шарнир
    if(!fixed) {

      // других внутренних сил на точку вроде не действует:
      ApplyForce( g * mass ); // вес тела это сила тянущая его вниз
    }

}


void hinge_obj::getDimensionsBox(Vector2D* v1, Vector2D* v2)
{
    Vector2D g1(-display_size, -display_size);
    Vector2D g2( display_size,  display_size);
    Vector2D::MergeVector( g1, v1, v2 );
    Vector2D::MergeVector( g2, v1, v2 );
}
