#ifndef HEADER_FILE_H_
#define HEADER_FILE_H_

#include <SDL/SDL_opengl.h>

#include "colors.h"
#include "math/vector2d.h"

void gl_glass_enable();  // прозрачность "черное стекло"
void gl_glass_disable();


void gl_white_glass_enable();  // прозрачность "белое стекло"
void gl_white_glass_disable();

void glColor ( Color c );
void glVertex( Vector2D& v );
void glTranslate( Vector2D& coord );

void DisplayCross( Scalar size );
void DisplayCross( Vector2D coord, Scalar size );

#endif
